#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "iec61131_cc.h"
#include "iec61131_token.h"

#include "iec61131.h"



static void _iec61131_destroyTokenCollection( void *pObject );

/* static void _iec61131_destroyTokenInstruction( void *pObject ); */

static void _iec61131_destroyTokenLiteral( void *pObject );

static void _iec61131_destroyTokenPOU( void *pObject );

static void _iec61131_destroyTokenRaw( void *pObject );

static void _iec61131_destroyTokenType( void *pObject );

static void _iec61131_destroyTokenVar( void *pObject );


static void
_iec61131_destroyTokenCollection( void *pObject )
{
    IEC61131_TOKEN_COLLECTION *pToken;


    pToken = ( IEC61131_TOKEN_COLLECTION * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Members )
        {
            BASE_DESTROY( pToken->Members );
        }

        free( pToken );
        pToken = NULL;
    }
}


static void 
_iec61131_destroyTokenLiteral( void *pObject )
{
    IEC61131_TOKEN_LITERAL *pToken;


    pToken = ( IEC61131_TOKEN_LITERAL * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Name != NULL )
        {
            free( pToken->Name );
            pToken->Name = NULL;
        }

        free( pToken );
        pToken = NULL;
    }
}


static void
_iec61131_destroyTokenPOU( void *pObject )
{
    IEC61131_TOKEN_POU *pToken;


    pToken = ( IEC61131_TOKEN_POU * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Body != NULL )
        {
            BASE_DESTROY( pToken->Body );
        }
        if( pToken->Variables != NULL )
        {
            BASE_DESTROY( pToken->Variables );
        }

        if( pToken->Name != NULL )
        {
            free( pToken->Name );
            pToken->Name = NULL;
        }

        free( pToken );
        pToken = NULL;
    }
}


static void
_iec61131_destroyTokenRaw( void *pObject )
{
    IEC61131_TOKEN_RAW *pToken;


    pToken = ( IEC61131_TOKEN_RAW * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Length > 0 )
        {
            free( pToken->Data );
            pToken->Data = NULL;
        }

        free( pToken );
        pToken = NULL;
    }
}


static void
_iec61131_destroyTokenType( void *pObject )
{
    IEC61131_TOKEN_TYPE *pToken;


    pToken = ( IEC61131_TOKEN_TYPE * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Elements != NULL )
        {
            BASE_DESTROY( pToken->Elements );
        }

        if( pToken->Name != NULL )
        {
            free( pToken->Name );
            pToken->Name = NULL;
        }

        free( pToken );
        pToken = NULL;
    }
}


static void
_iec61131_destroyTokenVar( void *pObject )
{
    IEC61131_TOKEN_VARIABLE *pToken;


    pToken = ( IEC61131_TOKEN_VARIABLE * ) pObject;

    if( pToken != NULL )
    {
        if( pToken->Name != NULL )
        {
            free( pToken->Name );
            pToken->Name = NULL;
        }

        free( pToken );
        pToken = NULL;
    }
}


IEC61131_TOKEN *
iec61131_allocateToken( unsigned int uId )
{
    IEC61131_TOKEN *pToken;


    if( ( pToken = ( IEC61131_TOKEN * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, free );

    IEC61131_TOKEN_ID( pToken, uId );

    return pToken;
}


IEC61131_TOKEN_COLLECTION *
iec61131_allocateTokenCollection( unsigned int uId )
{
    IEC61131_TOKEN_COLLECTION *pToken;


    if( ( pToken = ( IEC61131_TOKEN_COLLECTION * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenCollection );

    IEC61131_TOKEN_ID( pToken, uId );

    pToken->Members = NULL;

    return pToken;
}


IEC61131_TOKEN_INSTRUCTION *
iec61131_allocateTokenInstruction( unsigned int uId )
{
    IEC61131_TOKEN_INSTRUCTION *pToken;


    if( ( pToken = ( IEC61131_TOKEN_INSTRUCTION * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, free );

    IEC61131_TOKEN_ID( pToken, uId );

    pToken->Label = NULL;
    pToken->Operand = NULL;
    pToken->Branch = NULL;
    pToken->List = NULL;
    pToken->PC = 0;
    pToken->Target = 0;

    return pToken;
}


void
iec61131_allocateTokenLabel( IEC61131_TOKEN_INSTRUCTION *pToken, const char *pLabel )
{
    if( pLabel == NULL )
    {
        return;
    }

    if( ( pToken->Label = ( char * ) calloc( 1, strlen( pLabel ) + 1 ) ) == NULL )
    {
        return;
    }
    ( void ) memcpy( pToken->Label, pLabel, strlen( pLabel ) );
}


IEC61131_TOKEN_LITERAL *
iec61131_allocateTokenLiteral( unsigned int uId, char *pName )
{
    IEC61131_TOKEN_LITERAL *pToken;


    if( ( pToken = ( IEC61131_TOKEN_LITERAL * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenLiteral );

    IEC61131_TOKEN_ID( pToken, uId );
    pToken->Fixed_Type = BOOL_FALSE;
    pToken->Assigned = BOOL_FALSE;
    pToken->Name = NULL;

    if( pName != NULL )
    {
        if( ( pToken->Name = ( char * ) calloc( 1, strlen( pName ) + 1 ) ) == NULL ) 
        {
            BASE_DESTROY( pToken );
            return NULL;
        }
        ( void ) memcpy( pToken->Name, pName, strlen( pName ) );
    }

    return pToken;
}


IEC61131_TOKEN_POU *
iec61131_allocateTokenPOU( unsigned int uId, char *pName )
{
    IEC61131_TOKEN_POU *pToken;


    if( pName == NULL ) 
    {
        return NULL;
    }

    if( ( pToken = ( IEC61131_TOKEN_POU * ) calloc( 1, sizeof( *pToken ) ) ) == NULL ) 
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenPOU );

    IEC61131_TOKEN_ID( pToken, uId );

    if( ( pToken->Name = ( char * ) calloc( 1, strlen( pName ) + 1 ) ) == NULL )
    {
        BASE_DESTROY( pToken );
        return NULL;
    }
    ( void ) memcpy( pToken->Name, pName, strlen( pName ) );

    pToken->Body = NULL;
    pToken->Variables = NULL;
    pToken->Size = 0;

    return pToken;
}


IEC61131_TOKEN_RAW *
iec61131_allocateTokenRaw( unsigned int uId, char *pData, size_t uLength )
{
    IEC61131_TOKEN_RAW *pToken;


    if( ( pToken = ( IEC61131_TOKEN_RAW * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenRaw );

    IEC61131_TOKEN_ID( pToken, uId );

    if( uLength > 0 )
    {
        if( ( pToken->Data = ( char * ) calloc( 1, uLength ) ) == NULL )
        {
            BASE_DESTROY( pToken );
            return NULL;
        }
        ( void ) memcpy( pToken->Data, pData, uLength );
    }

    pToken->Length = uLength;

    return pToken;
}


IEC61131_TOKEN_TYPE *
iec61131_allocateTokenType( unsigned int uId, char *pName )
{
    IEC61131_TOKEN_TYPE *pToken;


    if( pName == NULL )
    {
        return NULL;
    }

    if( ( pToken = ( IEC61131_TOKEN_TYPE * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenType );

    IEC61131_TOKEN_ID( pToken, uId );

    if( ( pToken->Name = ( char * ) calloc( 1, strlen( pName ) + 1 ) ) == NULL )
    {
        BASE_DESTROY( pToken );
        return NULL;
    }
    ( void ) memcpy( pToken->Name, pName, strlen( pName ) );

    pToken->Elements = NULL;

    return pToken;
}


IEC61131_TOKEN_VARIABLE *
iec61131_allocateTokenVar( unsigned int uId, char *pName )
{
    IEC61131_TOKEN_VARIABLE *pToken;


    if( pName == NULL ) 
    {
        return NULL;
    }

    if( ( pToken = ( IEC61131_TOKEN_VARIABLE * ) calloc( 1, sizeof( *pToken ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pToken, TYPE_TOKEN );
    BASE_SET_DESTROY( pToken, _iec61131_destroyTokenVar );

    IEC61131_TOKEN_ID( pToken, uId );

    if( ( pToken->Name = ( char * ) calloc( 1, strlen( pName ) + 1 ) ) == NULL )
    {
        BASE_DESTROY( pToken );
        return NULL;
    }
    ( void ) memcpy( pToken->Name, pName, strlen( pName ) );

    pToken->Type = NULL;
    pToken->Location = NULL;

    return pToken;
}


