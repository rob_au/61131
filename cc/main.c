#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <limits.h>
#include <errno.h>

#include "hex.h"
#include "iec61131_bytecode.h"
#include "iec61131_cc.h"
#include "iec61131_list.h"
#include "stage.h"
#include "symboltable.h"


#define IS_DIR_SEPARATOR( c )   ( ( (c) == '/' ) || ( (c) == '\\' ) )


/*
    The purpose of this symbol is to provide a unified start of cycle time 
    stamp for time dependent operations of the IEC 61131-3 virtual machine.  
    This symbol is however unused within the IEC 61131-3 compiler application 
    but is required for successful linking of the compiler application with the 
    external C library that is shared between compiler and virtual machine 
    binaries for external C library function and function block implementation.
*/

uint32_t _CYCLETIME = 0;

extern char * optarg;


int
main( int nArgc, char **pArgv )
{
    IEC61131_CC stContext;
    char sFilename[NAME_MAX];
    char *pChar, *pName, *pOutput;
    int nOption, nResult;
    bool bSymbolTable;


    ( void ) memset( &stContext, 0, sizeof( stContext ) );
    ( void ) memblock_initialise( &stContext.ByteCode );

    pOutput = NULL;

    pName = pArgv[0] + strlen( pArgv[0] );
    while( ( pName != pArgv[0] ) &&
            ( ! IS_DIR_SEPARATOR( pName[-1] ) ) )
    {
        --pName;
    }
    stContext.Name = pName;
    stContext.Align = 4;
    stContext.Debug = 0;
    bSymbolTable = false;

    opterr = 0;
    while( ( nOption = getopt( nArgc, pArgv, "a:do:s:t" ) ) != -1 )
    {
        switch( nOption )
        {
            case 'a':

                stContext.Align = strtoul( optarg, ( char ** ) NULL, 10 );
                if( ( stContext.Align < 1 ) ||
                        ( stContext.Align > 8 ) )
                {
                    printf( "%s: error: invalid alignment size '%lu'\n",
                            stContext.Name,
                            stContext.Align );
                    return -EINVAL;
                }
                break;

            case 'd':

                stContext.Debug = 1;
                break;

            case 'o':

                pOutput = optarg;
                break;

            case 's':

                stContext.Size = strtoul( optarg, ( char ** ) NULL, 10 );
                break;

            case 't':

                bSymbolTable = true;
                break;

            default:

                printf( "%s: error: bad command line option '-%c'\n",
                        stContext.Name,
                        optopt );
                break;
        }
    }
    if( optind >= nArgc )
    {
        printf( "%s: error: no input files\n",
                stContext.Name );
        return -EINVAL;
    }

    if( ( ( nResult = stage_token( &stContext, nArgc - optind, &pArgv[ optind ] ) ) < 0 ) ||
            ( ( nResult = stage_functionblock( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_tables( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_type( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_format( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_symboltable( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_bytecode( &stContext ) ) < 0 ) ||
            ( ( nResult = stage_link( &stContext ) ) < 0 ) )
    {
        return nResult;
    }

    if( stContext.ByteCode.End /* == 0 */ < sizeof( IEC61131_FORMAT ) )
    {
//        printf( "%s: error: cannot compile source '%s'\n",
//                stContext.Name,
//                stContext.Source );
        return -EINVAL;
    }

    if( pOutput == NULL )
    {
        ( void ) snprintf( sFilename, sizeof( sFilename ), "%s", stContext.Source );

        if( ( pChar = strrchr( sFilename, '.' ) ) != NULL )
        {
            *pChar = '\0';
        }
        ( void ) snprintf( pChar, sizeof( sFilename ) - ( pChar - sFilename ), ".hex" );

        pChar = sFilename + strlen( sFilename );
        while( ( pChar != sFilename ) &&
                ( ! IS_DIR_SEPARATOR( pChar[-1] ) ) )
        {
            --pChar;
        }

        pOutput = pChar;
    }

    if( ( nResult = hex_writeFile( pOutput, stContext.ByteCode.Data, stContext.ByteCode.End ) ) != 0 )
    {
        ( void ) unlink( pOutput );

        return nResult;
    }

    if( bSymbolTable )
    {
        symboltable_writeOutput( &stContext );
    }

    return 0;
}
