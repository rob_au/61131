#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <assert.h>

#include "iec61131_cc.h"
#include "iec61131_list.h"
#include "iec61131_pou.h"
#include "iec61131_symbol.h"
#include "iec61131_token.h"
#include "stage.h"
#include "symbol.h"

#include "iec61131.h"
#include "iec61131_bytecode.h"
#include "iec61131_external.h"


static unsigned int _stage_getInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static const char * _stage_getInstructionName( int nId );

static IEC61131_TYPE _stage_getOperandType( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static IEC61131_TYPE _stage_getType( IEC61131_CC *pContext, IEC61131_TOKEN *pToken );

static int _stage_insertInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static void _stage_processInstructionList( IEC61131_CC *pContext, IEC61131_LIST *pList );

static void _stage_processParameterList( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_writeAlignment( IEC61131_CC *pContext, unsigned int uBytes );

static int _stage_writeInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_writeLiteral( IEC61131_CC *pContext, IEC61131_TOKEN_LITERAL *pLiteral );

static void _stage_writeToken( IEC61131_CC *pContext, IEC61131_TOKEN *pToken );


/*
    This function provides the mapping between token identifiers and the
    corresponding IEC 61131-3 virtual machine byte codes.  This function is
    significantly important as it allows for dissociation of compiler parsing
    and tokenisation operations from virtual machine byte code definitions.
*/

static unsigned int
_stage_getInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    unsigned int uInstr, uOperand;


    uOperand = 0;

    if( ( pInstr->List != NULL ) &&
            ( IEC61131_LIST_SIZE( pInstr->List ) > 0 ) )
    {
        switch( pInstr->Token.Id )
        {
            case CAL:
            case CALC:
            case CALCN:

                /*
                    The List member of the IEC61131_TOKEN_INSTRUCTION is employed for both the 
                    storage or deferred actions and lists of program organisation unit (POU) 
                    arguments.  For the latter case, this code implements an exception such 
                    that these parameters can be written to bytecode in a different manner to 
                    that for deferred instruction execution.
                */

                break;

            default:

                uOperand = S_INSTRUCTION( OP_NONE, MODIFIER_PARENTHESIS_LEFT );
                break;
        }
    }

    uOperand |= _stage_getOperandType( pContext, pInstr );

    switch( pInstr->Token.Id )
    {
        case NOP:       uInstr = S_INSTRUCTION( OP_NOP, MODIFIER_NONE ); break;
        case LD:        uInstr = S_INSTRUCTION( OP_LD, MODIFIER_NONE ); break;
        case LDN:       uInstr = S_INSTRUCTION( OP_LD, MODIFIER_N ); break;
        case ST:        uInstr = S_INSTRUCTION( OP_ST, MODIFIER_NONE ); break;
        case STN:       uInstr = S_INSTRUCTION( OP_ST, MODIFIER_N ); break;
        case S:         uInstr = S_INSTRUCTION( OP_S, MODIFIER_NONE ); break;
        case R:         uInstr = S_INSTRUCTION( OP_R, MODIFIER_NONE ); break;
        case AND:       uInstr = S_INSTRUCTION( OP_AND, MODIFIER_NONE ); break;
        case ANDN:      uInstr = S_INSTRUCTION( OP_AND, MODIFIER_N ); break;
        case OR:        uInstr = S_INSTRUCTION( OP_OR, MODIFIER_NONE ); break;
        case ORN:       uInstr = S_INSTRUCTION( OP_OR, MODIFIER_N ); break;
        case XOR:       uInstr = S_INSTRUCTION( OP_XOR, MODIFIER_NONE ); break;
        case XORN:      uInstr = S_INSTRUCTION( OP_XOR, MODIFIER_N ); break;
        case NOT:       uInstr = S_INSTRUCTION( OP_NOT, MODIFIER_NONE ); break;
        case ADD:       uInstr = S_INSTRUCTION( OP_ADD, MODIFIER_NONE ); break;
        case SUB:       uInstr = S_INSTRUCTION( OP_SUB, MODIFIER_NONE ); break;
        case MUL:       uInstr = S_INSTRUCTION( OP_MUL, MODIFIER_NONE ); break;
        case DIV:       uInstr = S_INSTRUCTION( OP_DIV, MODIFIER_NONE ); break;
        case MOD:       uInstr = S_INSTRUCTION( OP_MOD, MODIFIER_NONE ); break;
        case GT:        uInstr = S_INSTRUCTION( OP_GT, MODIFIER_NONE ); break;
        case GE:        uInstr = S_INSTRUCTION( OP_GE, MODIFIER_NONE ); break;
        case EQ:        uInstr = S_INSTRUCTION( OP_EQ, MODIFIER_NONE ); break;
        case NE:        uInstr = S_INSTRUCTION( OP_NE, MODIFIER_NONE ); break;
        case LE:        uInstr = S_INSTRUCTION( OP_LE, MODIFIER_NONE ); break;
        case LT:        uInstr = S_INSTRUCTION( OP_LT, MODIFIER_NONE ); break;
        case JMP:       uInstr = S_INSTRUCTION( OP_JMP, MODIFIER_NONE ); break;
        case JMPC:      uInstr = S_INSTRUCTION( OP_JMP, MODIFIER_C ); break;
        case JMPCN:     uInstr = S_INSTRUCTION( OP_JMP, MODIFIER_C | MODIFIER_N ); break;
        case CAL:       uInstr = S_INSTRUCTION( OP_CAL, MODIFIER_NONE ); break;
        case CALC:      uInstr = S_INSTRUCTION( OP_CAL, MODIFIER_C ); break;
        case CALCN:     uInstr = S_INSTRUCTION( OP_CAL, MODIFIER_C | MODIFIER_N ); break;
        case RET:       uInstr = S_INSTRUCTION( OP_RET, MODIFIER_NONE ); break;
        case RETC:      uInstr = S_INSTRUCTION( OP_RET, MODIFIER_C ); break;
        case RETCN:     uInstr = S_INSTRUCTION( OP_RET, MODIFIER_C | MODIFIER_N ); break;
        default:

            return OP_NONE;
    }

    uInstr |= uOperand;

    return uInstr;
}


static const char * 
_stage_getInstructionName( int nId )
{
    switch( nId )
    {
        case NOP:       return "NOP";
        case LD:        return "LD";
        case LDN:       return "LDN";
        case ST:        return "ST";
        case STN:       return "STN";
        case S:         return "S";
        case R:         return "R";
        case AND:       return "AND";
        case ANDN:      return "ANDN";
        case OR:        return "OR";
        case ORN:       return "ORN";
        case XOR:       return "XOR";
        case XORN:      return "XORN";
        case NOT:       return "NOT";
        case ADD:       return "ADD";
        case SUB:       return "SUB";
        case MUL:       return "MUL";
        case DIV:       return "DIV";
        case MOD:       return "MOD";
        case GT:        return "GT";
        case GE:        return "GE";
        case EQ:        return "EQ"; 
        case NE:        return "NE";
        case LE:        return "LE";
        case LT:        return "LT";
        case JMP:       return "JMP";
        case JMPC:      return "JMPC";
        case JMPCN:     return "JMPCN";
        case CAL:       return "CAL";
        case CALC:      return "CALC";
        case CALCN:     return "CALCN";
        case RET:       return "RET";
        case RETC:      return "RETC";
        case RETCN:     return "RETCN";
        default:

            return "???";
    }
}


static IEC61131_TYPE
_stage_getOperandType( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_TOKEN *pToken;


    if( ( pToken = ( IEC61131_TOKEN * ) pInstr->Operand ) == NULL )
    {
        /*
            Instead of simply returning TYPE_NONE at this point, this code will return
            any operand meta-information which may have been derived during the type-
            checking pass of IEC 61131-3 compiler operations.
        */

        return pInstr->Meta.Operand;
    }
    pToken->Scope = ( ( IEC61131_TOKEN * ) pInstr )->Scope;

    return _stage_getType( pContext, pToken );
}


static IEC61131_TYPE 
_stage_getType( IEC61131_CC *pContext, IEC61131_TOKEN *pToken )
{
    IEC61131_SYMBOL *pSymbol;
    unsigned int uType;


    switch( pToken->Id )
    {
        case identifier:

            if( ( pSymbol = symbol_getSymbol( pContext,
                    ( ( IEC61131_TOKEN_RAW * ) pToken )->Data,
                    pToken->Scope,
                    true ) ) == NULL )
            {
                return TYPE_NONE;
            }

            if( ( uType = pSymbol->Data.Type ) == TYPE_NONE )
            {
                uType = symbol_getSymbolType( pContext, 
                        ( ( IEC61131_TOKEN_RAW * ) pToken )->Data,
                        pToken->Scope );
            }

            return uType |
                    pSymbol->Direct |
                    OPERAND_SYMBOL;
            /* break; */

        case bit_string:
        case boolean:
        case duration:
        case integer:
        case real:

            return ( ( IEC61131_TOKEN_LITERAL * ) pToken )->Value.Type;

        default:

            return TYPE_NONE;
    }
}


static int
_stage_insertInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    /*
        Dynamically reallocate memory for the execution jump or POU call
        instruction storage and perform an insertion sort for the new entry based
        on instruction scope and target name.
    */

    if( ( pContext->InstructionTable.Entries = ( IEC61131_TOKEN_INSTRUCTION ** ) realloc(
            pContext->InstructionTable.Entries,
            ( sizeof( IEC61131_TOKEN_INSTRUCTION ) * ( pContext->InstructionTable.Count + 1 ) ) ) ) == NULL )
    {
        return -errno;
    }
    pContext->InstructionTable.Entries[ pContext->InstructionTable.Count++ ] = pInstr;

    return 0;
}


static void
_stage_processInstructionList( IEC61131_CC *pContext, IEC61131_LIST *pList )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_INSTRUCTION *pInstr;


    if( pList == NULL )
    {
        return;
    }

    for( pElement = IEC61131_LIST_HEAD( pList );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
        _stage_writeInstruction( pContext, pInstr );
    }
}


static void
_stage_processParameterList( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN *pToken;
    unsigned int uByteCode, uModifier, uOperand, uValue;


    /*
        It should be noted that the parameters associated with a function program 
        organisation unit (POU) execution call are pushed onto the parameter stack 
        in reverse order such that they can be popped off in a last-in, first-out 
        (LIFO) manner within the POU in parameter order.  Additionally, the 
        modifier associated with the execution call instruction (CAL) is 
        additionally applied to these push instructions such that the parameter 
        stack is not unnecessarily populated with POU parameters.
    */

    if( ( ! pInstr->List ) ||
            ( ( uByteCode = _stage_getInstruction( pContext, pInstr ) ) == OP_NONE ) )
    {
        return;
    }

    uModifier = G_MODIFIER( uByteCode );

    for( pElement = IEC61131_LIST_HEAD( pInstr->List );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pToken = ( IEC61131_TOKEN * ) pElement->Data;
        pToken->Scope = ( ( IEC61131_TOKEN * ) pInstr )->Scope;

        uOperand = _stage_getType( pContext, pToken );
        uByteCode = S_BYTECODE( OP_PUSH, uModifier, uOperand );

        uValue = htobe32( uByteCode );
        memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
        memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
        memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
        memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

        _stage_writeToken( pContext, pToken );
    }
}


static int
_stage_writeAlignment( IEC61131_CC *pContext, unsigned int uBytes )
{
    unsigned int uIndex, uPadding;


    uPadding = ( ( uBytes % pContext->Align ) > 0 ) ?
            ( pContext->Align - ( uBytes % pContext->Align ) ) :
            0;

    for( uIndex = 0; uIndex < uPadding; ++uIndex )
    {
    	memblock_append( &pContext->ByteCode, 0 );
    }
    return 0;
}


static int
_stage_writeInstruction( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN *pToken;
    IEC61131_TOKEN_POU *pPOU;
    unsigned int uByteCode, uId, uModifier, uOpcode, uValue;
    char *pName;


    if( ( uOpcode = _stage_getInstruction( pContext, pInstr ) ) == OP_NONE )
    {
        return -ENOENT;
    }


    /*
        If the instruction written includes reference to either a POU or POU label,
        the current reference position is recorded for later linking operations and
        a placeholder for the subsequent jump or call operation inserted into the
        byte code stream.  This placeholder is populated during the later linking
        operation.

        This action is performed in the event that the target location has not yet
        been written to byte code and therefore not yet known at this point of
        operations.
    */

    switch( G_OP( uOpcode ) )
    {
        case OP_JMP:
        case OP_CAL:

            pInstr->Target = pContext->ByteCode.End;

            pToken = ( IEC61131_TOKEN * ) pInstr;
            pPOU = ( IEC61131_TOKEN_POU * ) pToken->Scope;
            pName = ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data;

            uId = iec61131_getPOUType( pContext, pName, pPOU ); 

            if( ( pSymbol = symbol_getSymbol( pContext, pName, pPOU, true ) ) == NULL )
            {
                return -ENOENT;
            }

            switch( uId )
            {
                case FUNCTION:

                    if( pInstr->List != NULL )
                    {
                        _stage_processParameterList( pContext, pInstr );
                    }
                    break;

                case FUNCTION_BLOCK:

                    uModifier = G_MODIFIER( uOpcode );
                    uByteCode = S_INSTRUCTION( OP_PUSH, uModifier );
                    uByteCode |= OPERAND_FUNCTIONBLOCK;

                    uValue = htobe32( uByteCode );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                    memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

                    uValue = htobe32( pSymbol->Offset );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                    memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                    memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

printf( "%8s (%u)\n", 
        "PUSH",
        G_OPERAND( uByteCode ) );

printf( "[%08x]  ", pInstr->Target );
memblock_dump( &pContext->ByteCode, pInstr->Target, 0 );
                    break;

                case PROGRAM:
                default:

                    break;
            }

            pInstr->PC = pContext->ByteCode.End;

            uValue = htobe32( uOpcode );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

            _stage_insertInstruction( pContext, pInstr );

            memblock_append( &pContext->ByteCode, 0 );
            memblock_append( &pContext->ByteCode, 0 );
            memblock_append( &pContext->ByteCode, 0 );
            memblock_append( &pContext->ByteCode, 0 );

printf( "%8s (%u)\n", 
        _stage_getInstructionName( pInstr->Token.Id ),
        G_OPERAND( uOpcode ) );

printf( "[%08x]  ", pInstr->PC );
memblock_dump( &pContext->ByteCode, pInstr->PC, 0 );

            break;

        case OP_RET:

            /* 
                If a return instruction is encountered within a program organisation unit 
                (POU) that has an explicit return type, the return value for the POU is 
                specified as a operand to the return instruction.  This approach has been 
                adopted to provide some efficiency with respect to the management of such 
                return values within the virtual machine run-time.
            */

            pInstr->Target = pContext->ByteCode.End;
            pInstr->PC = pContext->ByteCode.End;

            for( ;; )
            {
                pToken = ( IEC61131_TOKEN * ) pInstr;
                pPOU = ( IEC61131_TOKEN_POU * ) pToken->Scope;
                /* if( ( ( IEC61131_TOKEN * ) pPOU )->Id == FUNCTION ) */
                if( pPOU->Type != NULL )
                {
                    if( ( pSymbol = symbol_getSymbol( pContext, pPOU->Name, pPOU, true ) ) != NULL )
                    {
                        uOpcode |= ( pSymbol->Data.Type | OPERAND_SYMBOL );

                        uValue = htobe32( uOpcode );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                        memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

                        uValue = htobe32( pSymbol->Offset );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                        memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

                        break;
                    }
                }

                uValue = htobe32( uOpcode );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

printf( "%8s (%u)\n", 
        _stage_getInstructionName( pInstr->Token.Id ),
        G_OPERAND( uOpcode ) );

printf( "[%08x]  ", pInstr->Target );
memblock_dump( &pContext->ByteCode, pInstr->Target, 0 );

                break;
            }

            break;

        case OP_AND:
        case OP_OR:
        case OP_XOR:
        case OP_ADD:
        case OP_SUB:
        case OP_MUL:
        case OP_DIV:
        case OP_MOD:
        case OP_GT:
        case OP_GE:
        case OP_EQ:
        case OP_NE:
        case OP_LE:
        case OP_LT:

            /*
                If there is any deferred execution list associated with the current
                instruction, write these operations to the byte code, terminating these
                actions with a OP_PARENTHESIS_NONE (right hand parenthesis, execute
                deferred operations) instruction.
            */

            pInstr->Target = pContext->ByteCode.End;
            pInstr->PC = pContext->ByteCode.End;

            uValue = htobe32( uOpcode );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

            if( pInstr->List )
            {
                _stage_processInstructionList( pContext, pInstr->List );

                uOpcode = S_INSTRUCTION( OP_PARENTHESIS_RIGHT, MODIFIER_NONE );
                uOpcode |= TYPE_NONE;

                uValue = htobe32( uOpcode );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );
            }
            else
            {
                _stage_writeToken( pContext, pInstr->Operand );
            
printf( "%8s (%u)\n", 
        _stage_getInstructionName( pInstr->Token.Id ),
        G_OPERAND( uOpcode ) );

printf( "[%08x]  ", pInstr->Target );
memblock_dump( &pContext->ByteCode, pInstr->Target, 0 );
            }

            break;

        default:

            pInstr->Target = pContext->ByteCode.End;
            pInstr->PC = pContext->ByteCode.End;

            uValue = htobe32( uOpcode );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
            memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

            if( pInstr->Operand )
            {
                _stage_writeToken( pContext, pInstr->Operand );

printf( "%8s (%u)\n", 
        _stage_getInstructionName( pInstr->Token.Id ),
        G_OPERAND( uOpcode ) );

printf( "[%08x]  ", pInstr->Target );
memblock_dump( &pContext->ByteCode, pInstr->Target, 0 );
            }
            break;
    }

    return 0;
}


static int
_stage_writeLiteral( IEC61131_CC *pContext, IEC61131_TOKEN_LITERAL *pLiteral )
{
    uint16_t uVal16;
    uint32_t uVal32;
    uint64_t uVal64;


    switch( pLiteral->Value.Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

        	memblock_append( &pContext->ByteCode, pLiteral->Value.Value.B8 );

        	_stage_writeAlignment( pContext, sizeof( pLiteral->Value.Value.B8 ) );

            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uVal16 = htobe16( pLiteral->Value.Value.B16 );
            memblock_append( &pContext->ByteCode, ( ( uVal16 & 0x0000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uVal16 & 0x000000ff ) );

            _stage_writeAlignment( pContext, sizeof( pLiteral->Value.Value.B16 ) );

            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:
        case TYPE_REAL:
        case TYPE_TIME:

            uVal32 = htobe32( pLiteral->Value.Value.B32 );
            memblock_append( &pContext->ByteCode, ( ( uVal32 & 0xff000000 ) >> 24 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal32 & 0x00ff0000 ) >> 16 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal32 & 0x0000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uVal32 & 0x000000ff ) );

            _stage_writeAlignment( pContext, sizeof( pLiteral->Value.Value.B32 ) );

            break;

        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:
        case TYPE_LREAL:

            uVal64 = htobe64( pLiteral->Value.Value.B64 );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0xff00000000000000 ) >> 56 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x00ff000000000000 ) >> 48 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x0000ff0000000000 ) >> 40 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x000000ff00000000 ) >> 32 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x00000000ff000000 ) >> 24 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x0000000000ff0000 ) >> 16 ) );
            memblock_append( &pContext->ByteCode, ( ( uVal64 & 0x000000000000ff00 ) >> 8 ) );
            memblock_append( &pContext->ByteCode, ( uVal64 & 0x00000000000000ff ) );

            _stage_writeAlignment( pContext, sizeof( pLiteral->Value.Value.B64 ) );

            break;

        default:

assert( 0 );
            return -1;
    }

    return 0;
}


static void
_stage_writeToken( IEC61131_CC *pContext, IEC61131_TOKEN *pToken )
{
    unsigned int uOffset, uValue;


    switch( pToken->Id )
    {
        case identifier:

            uOffset = symbol_getSymbolOffset( pContext, ( ( IEC61131_TOKEN_RAW * ) pToken )->Data, pToken->Scope );

            uValue = htobe32( uOffset );
			memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
			memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
			memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
			memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );
            break;

        case bit_string:
        case boolean:
        case duration:
        case integer:
        case real:

            _stage_writeLiteral( pContext, ( IEC61131_TOKEN_LITERAL * ) pToken );
            break;

        default:

            break;
    }
}


/*
    This stage of the IEC 61131-3 compilation process involves the writing of
    the parsed code into a byte code format compatible with the IEC 61131-3
    virtual machine.  For this operation, common header files are employed for
    byte code definitions.
*/

int
stage_bytecode( IEC61131_CC *pContext )
{
    IEC61131_FORMAT stFormat;
    IEC61131_LIST_ELEMENT *pElement1, *pElement2;
    IEC61131_TOKEN *pToken;
    IEC61131_TOKEN_POU *pPOU;
    IEC61131_TOKEN_VARIABLE *pVariable;
    unsigned int uByteCode, uId, uOffset, uStart, uType, uValue;


    ( void ) memset( &stFormat, 0, sizeof( stFormat ) );
    stFormat.ByteCode = pContext->ByteCode.End;
    memblock_write( &pContext->ByteCode, offsetof( IEC61131_FORMAT, ByteCode ), ( char * ) &stFormat.ByteCode, sizeof( stFormat.ByteCode ) );


    /*
        Step through each of the program organisation units (POUs) and generate 
        corresponding IEC 61131-3 virtual machine byte code.
    */

    for( pElement1 = IEC61131_LIST_HEAD( &pContext->Library );
            pElement1;
            pElement1 = IEC61131_LIST_NEXT( pElement1 ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement1->Data;
        pPOU->PC = pContext->ByteCode.End;


        uId = ( ( IEC61131_TOKEN * ) pPOU )->Id;
        switch( uId )
        {
            case FUNCTION:

                /*
                    The following code is intended to insert stack-pop byte code instructions 
                    as required in order to retrieve parameters passed to this POU from the 
                    virtual machine stack, copying them into the appropriate input parameter 
                    location.  Note that this operation must be performed in the opposite 
                    order to which values are pushed onto the stack in the calling code due
                    to the last-in, first-out (LIFO) nature of stack access.
                */

                if( pPOU->Variables != NULL )
                {
                    for( pElement2 = IEC61131_LIST_TAIL( pPOU->Variables );
                            pElement2;
                            pElement2 = IEC61131_LIST_PREVIOUS( pElement2 ) )
                    {
                        pVariable = ( IEC61131_TOKEN_VARIABLE * ) pElement2->Data;
                        if( ( pVariable->Flags & FLAGS_INPUT ) == 0 )
                        {
                            continue;
                        }

                        pToken = ( IEC61131_TOKEN * ) pVariable;

                        uByteCode = S_INSTRUCTION( OP_POP, MODIFIER_NONE );
                        uType = symbol_getSymbolType( pContext, pVariable->Name, pToken->Scope );
                        uByteCode |= ( uType | OPERAND_SYMBOL );
                        if( pVariable->Location != NULL )
                        {
                            /* uByteCode |= 0; */
                        }

uStart = pContext->ByteCode.End;

                        uValue = htobe32( uByteCode );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                        memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

                        uOffset = symbol_getSymbolOffset( pContext, pVariable->Name, pToken->Scope );        

                        uValue = htobe32( uOffset );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                        memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                        memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

printf( "%8s (%u)\n", 
        "POP",
        G_OPERAND( uByteCode ) );

printf( "[%08x]  ", uStart );
memblock_dump( &pContext->ByteCode, uStart, 0 );
                    }
                }
                break;

            case FUNCTION_BLOCK:

                /*
                    The following code is intended to pop the symbol table address of the 
                    function block instance for which function block operations should be 
                    branched and executed.
                */

                uByteCode = S_INSTRUCTION( OP_POP, MODIFIER_NONE );
                uByteCode |= OPERAND_FUNCTIONBLOCK;

uStart = pContext->ByteCode.End;

                uValue = htobe32( uByteCode );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0xff000000 ) >> 24 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x00ff0000 ) >> 16 ) );
                memblock_append( &pContext->ByteCode, ( ( uValue & 0x0000ff00 ) >> 8 ) );
                memblock_append( &pContext->ByteCode, ( uValue & 0x000000ff ) );

                memblock_append( &pContext->ByteCode, 0 );
                memblock_append( &pContext->ByteCode, 0 );
                memblock_append( &pContext->ByteCode, 0 );
                memblock_append( &pContext->ByteCode, 0 );

printf( "%8s (%u)\n", 
        "POP",
        G_OPERAND( uByteCode ) );

printf( "[%08x]  ", uStart );
memblock_dump( &pContext->ByteCode, uStart, 0 );
                break;

            case PROGRAM:
            default:

                break;
        }

        _stage_processInstructionList( pContext, pPOU->Body );
    }

    return 0;
}

