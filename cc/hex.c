#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <sys/types.h>

#include "hex.h"


int
hex_writeFile( const char * Filename, char * Pointer, unsigned int Length )
{
    unsigned int uOffset;
    unsigned char uByte, uChecksum, uLength;
    int nFd, nLength, nResult;
    char szLine[LINE_MAX];


    if( ( nFd = open( Filename, O_RDWR | O_CREAT | O_TRUNC, 0644 ) ) < 0 )
    {
        return -errno;
    }

    for( uOffset = 0,
            nResult = 0;;)
    {
        uLength = ( ( Length - uOffset ) > 16 ) ? 16 : ( Length - uOffset );
        if( uLength == 0 )
        {
            break;
        }

        ( void ) snprintf( szLine, sizeof( szLine ), ":%02x%02x%02x%02x",
                uLength,
                ( ( uOffset & 0xff00 ) >> 8 ),
                ( uOffset & 0x00ff ),
                HEX_RECTYP_DATA );

        nLength = strlen( szLine );
        if( write( nFd, szLine, nLength ) != nLength )
        {
            nResult = -errno;
            break;
        }

        uChecksum = uLength +
                ( ( uOffset & 0xff00 ) >> 8 ) +
                ( uOffset & 0x00ff ) +
                HEX_RECTYP_DATA;

        uOffset += uLength;

        for( ; uLength > 0; --uLength )
        {
            uByte = ( *Pointer++ & 0xff );

            ( void ) snprintf( szLine, sizeof( szLine ), "%02x", uByte );

            nLength = strlen( szLine );
            if( write( nFd, szLine, nLength ) != nLength )
            {
                nResult = -errno;
                break;
            }
            uChecksum += uByte;
        }

        uChecksum = 0x100 - ( uChecksum & 0xff );
        ( void ) snprintf( szLine, sizeof( szLine ), "%02x\n",
                ( uChecksum & 0xff ) );

        nLength = strlen( szLine );
        if( write( nFd, szLine, nLength ) != nLength )
        {
            nResult = -errno;
            break;
        }
    }

    if( nResult == 0 )
    {
        ( void ) snprintf( szLine, sizeof( szLine ), ":000000%02x%02x\n",
                HEX_RECTYP_EOFR,
                0x100 - HEX_RECTYP_EOFR );

        nLength = strlen( szLine );
        if( write( nFd, szLine, nLength ) != nLength )
        {
            nResult = -errno;
        }
    }

    close( nFd );

    return nResult;
}
