#include <stdio.h>

#include "iec61131_cc.h"

#include "iec61131.h"
#include "iec61131_bytecode.h"
#include "iec61131_external.h"


/*
    This stage of the IEC 61131-3 compilation process involves the generation 
    of the header stub of the byte code output file.  At this stage, this is 
    all performed within a dynamic memory block.
*/

int
stage_format( IEC61131_CC *pContext )
{
    IEC61131_FORMAT stFormat;


    /* assert( pContext->ByteCode.End == 0 ); */
    ( void ) memset( &stFormat, 0, sizeof( stFormat ) );
    stFormat.Header = IEC61131_FORMAT_MAGIC;
    stFormat.Symbols = 0;
    /* stFormat.Retain = 0; */
    stFormat.ByteCode = 0;
    IEC61131_DIGEST( ( char * ) &stFormat.Functions );
    memblock_write( &pContext->ByteCode, pContext->ByteCode.End, ( char * ) &stFormat, sizeof( stFormat ) );

    return 0;
}
