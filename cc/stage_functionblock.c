#include <stdio.h>

#include "iec61131_cc.h"

#include "iec61131.h"
#include "iec61131_bytecode.h"
#include "iec61131_symbol.h"


/*
    This stage of the compilation process is intended to determine how much 
    space should allocated within the symbol table for each function block 
    instance.  This is determined by iterating through each of the input, 
    internal and output variables associated with the function block and 
    accumulating how much memory-aligned space should be allocated for each.
*/

int
stage_functionblock( IEC61131_CC *pContext )
{
    IEC61131_LIST_ELEMENT *pElement1, *pElement2, *pElement3;
    IEC61131_TOKEN *pType;
    IEC61131_TOKEN_POU *pPOU, *pPOU1, *pPOU2;
    IEC61131_TOKEN_VARIABLE *pVariable;
    unsigned int uId, uSize, uType;


    for( pElement1 = IEC61131_LIST_HEAD( &pContext->Library );
            pElement1;
            pElement1 = IEC61131_LIST_NEXT( pElement1 ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement1->Data;

        uId = ( ( IEC61131_TOKEN * ) pPOU )->Id;
        if( uId != FUNCTION_BLOCK )
        {
            continue;
        }

        pPOU->Size = 0;

        for( pElement2 = IEC61131_LIST_HEAD( pPOU->Variables );
                pElement2;
                pElement2 = IEC61131_LIST_NEXT( pElement2 ) )
        {
            pVariable = ( IEC61131_TOKEN_VARIABLE * ) pElement2->Data;
            pType = ( IEC61131_TOKEN * ) pVariable->Type;

            uType = iec61131_symbolTypeMapping( pType->Id );
            uSize = iec61131_symbolTypeSize( uType );
            if( ( uSize % pContext->Align ) != 0 )
            {
                uSize += ( pContext->Align - ( uSize % pContext->Align ) );
            }
            pPOU->Size += uSize;
        }
    }


    /*
        The following code performs a second pass through the list of variables 
        associated with function blocks - This is in order to *attempt* to insert 
        symbol table space requirements associated with function block instances 
        that may be contained _within_ other function blocks (which is permitted 
        and allowed).  It should be noted that this operation is currently fraught 
        with danger as there is no check for circular references between function 
        block instances and function block definitions (which is _not_ permitted).
    */

    for( pElement1 = IEC61131_LIST_HEAD( &pContext->Library );
            pElement1;
            pElement1 = IEC61131_LIST_NEXT( pElement1 ) )
    {
        pPOU1 = ( IEC61131_TOKEN_POU * ) pElement1->Data;

        uId = ( ( IEC61131_TOKEN * ) pPOU1 )->Id;
        if( uId != FUNCTION_BLOCK )
        {
            continue;
        }

        for( pElement2 = IEC61131_LIST_HEAD( pPOU1->Variables );
                pElement2;
                pElement2 = IEC61131_LIST_NEXT( pElement2 ) )
        {
            pVariable = ( IEC61131_TOKEN_VARIABLE * ) pElement2->Data;
            pType = ( IEC61131_TOKEN * ) pVariable->Type;

            uType = iec61131_symbolTypeMapping( pType->Id );
            if( uType != TYPE_NONE )
            {
                continue;
            }

            for( pElement3 = IEC61131_LIST_HEAD( &pContext->Library );
                    pElement3;
                    pElement3 = IEC61131_LIST_NEXT( pElement3 ) )
            {
                pPOU2 = ( IEC61131_TOKEN_POU * ) pElement3->Data;
                if( strcasecmp( pVariable->Type->Name, pPOU2->Name ) == 0 )
                {
                    pPOU1->Size += pPOU2->Size;
                    break;
                }
            }
        }
    }

    return 0;
}
