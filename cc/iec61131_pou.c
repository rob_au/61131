#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>

#include "iec61131_cc.h"
#include "iec61131_external.h"
#include "iec61131_pou.h"
#include "iec61131_token.h"
#include "iec61131.h"


static int _iec61131_compareSymbols( const void *pLeft, const void *pRight );


static int
_iec61131_compareSymbols( const void *pLeft, const void *pRight )
{
    IEC61131_SYMBOL *pSymbol1, *pSymbol2;

    pSymbol1 = ( IEC61131_SYMBOL * ) pLeft;
    pSymbol2 = ( IEC61131_SYMBOL * ) pRight;

    if( pSymbol1->Scope == pSymbol2->Scope )
    {
        return strcmp( pSymbol1->Name, pSymbol2->Name );
    }

    return ( pSymbol1->Scope < pSymbol2->Scope ) ? -1 : 1;
}


int
iec61131_getPOUType( IEC61131_CC *pContext, char *pName, void *pScope )
{
    EXTERNAL_FUNCTION_BLOCK_DECLARATION *pDeclaration;
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_SYMBOL *pSymbol, stSymbol;
    IEC61131_TOKEN_POU *pPOU;
    IEC61131_TOKEN_LITERAL *pLiteral;
    char *pField;
    unsigned int uIndex;


    /*
        When attempting to determine the POU of a JMP/CAL target, the first action 
        undertaken is to check the supplied target name against the list of POUs 
        parsed from source files.  It is only where this preliminary check has 
        failed that this function shifts to check function block instances within 
        the current POU scope to determine whether this code is calling a program, 
        function or function block.  This latter action is a two-step process 
        involving the checking of function block instances both against the IEC 
        61131-3 language function blocks parsed from source files and against the 
        list of C-language library function blocks.
    */

    for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;
        if( strcmp( pPOU->Name, pName ) == 0 )
        {
            return ( ( IEC61131_TOKEN * ) pPOU )->Id;
        }
    }

    /* if( pScope == NULL ) return -ENOENT; */

    //  IEC 61131-3 language function blocks

    ( void ) memset( &stSymbol, 0, sizeof( stSymbol ) );
    ( void ) strncpy( stSymbol.Name, pName, sizeof( stSymbol.Name ) );
    if( ( pField = strchr( stSymbol.Name, '.' ) ) != NULL )
    {
        *pField++ = '\0';
    }
    stSymbol.Scope = pScope;

    if( ( pSymbol = bsearch( &stSymbol,
            pContext->SymbolTable.Entries,
            pContext->SymbolTable.Count,
            sizeof( IEC61131_SYMBOL ),
            _iec61131_compareSymbols ) ) != NULL )
    {
        pLiteral = ( IEC61131_TOKEN_LITERAL * ) pSymbol->Type;

        return iec61131_getPOUType( pContext, pLiteral->Name, NULL );
    }

    //  C library function blocks

    for( uIndex = 0; uIndex < IEC61131_FUNCTION_BLOCK_COUNT; ++uIndex )
    {
        pDeclaration = ( EXTERNAL_FUNCTION_BLOCK_DECLARATION * ) &IEC61131_FUNCTION_BLOCK[ uIndex ];
        if( ( pDeclaration->Name == NULL ) ||
                ( pDeclaration->Size == 0 ) ||
                ( pDeclaration->Parameter == NULL ) ||
                ( pDeclaration->Pointer == NULL ) )
        {
            continue;
        }

        if( strcasecmp( pDeclaration->Name, pName ) == 0 )
        {
            return FUNCTION_BLOCK;
        }
    }

    return -ENOENT;
}
