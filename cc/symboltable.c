#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>

#include "iec61131_bytecode.h"
#include "iec61131_cc.h"
#include "iec61131_symbol.h"
#include "iec61131.h"

#include "symboltable.h"


#define _MIN( a, b )    ( (a) < (b) ? (a) : (b) )


static int _symboltable_writeLine( IEC61131_CC *pContext, FILE *pFile, char *pFormat, ... );


static int
_symboltable_writeLine( IEC61131_CC *pContext, FILE *pFile, char *pFormat, ... )
{
    va_list stAp;
    char szLine[LINE_MAX];


    va_start( stAp, pFormat );
    vsnprintf( szLine, sizeof( szLine ), pFormat, stAp );
    va_end( stAp );

    if( fwrite( szLine, 1, strlen( szLine ), pFile ) != strlen( szLine ) )
    {
        printf( "%s: %s: %s\n",
                pContext->Name,
                pContext->Source,
                strerror( errno ) );
        return -errno;
    }
    return 0;
}


int
symboltable_writeOutput( IEC61131_CC *pContext )
{
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN_POU *pPOU;
    FILE *pFile;
    char szFilename[PATH_MAX];
    unsigned int uByte, uSymbol, uType;
    char *pChar, *pData;
    int nResult;


    /*
        The symbol table for a compiled IEC 61131-3 application is output as a 
        comma-separated values (CSV) file.
    */

    ( void ) snprintf( szFilename, sizeof( szFilename ), "%s", pContext->Source );
    if( ( pChar = strrchr( szFilename, '.' ) ) != NULL )
    {
        *pChar = '\0';
    }
    ( void ) strncat( szFilename, ".csv", sizeof( szFilename ) - strlen( szFilename ) );

    if( ( pFile = fopen( szFilename, "w" ) ) == NULL )
    {
        printf( "%s: %s: %s\n",
                pContext->Name,
                pContext->Source,
                strerror( errno ) );
        return -errno;
    }

    for( uSymbol = 0; uSymbol < pContext->SymbolTable.Count; ++uSymbol )
    {
        pSymbol = ( IEC61131_SYMBOL * ) &pContext->SymbolTable.Entries[ uSymbol ];
        pPOU = ( IEC61131_TOKEN_POU * ) pSymbol->Scope;
        uType = ( ( IEC61131_TOKEN * ) pPOU )->Id;
        if( uType == FUNCTION_BLOCK )
        {
            continue;
        }

        if( ( nResult = _symboltable_writeLine( pContext, pFile, "\"%s\",%u,%u,%u,%u,\"",
                pSymbol->Name,
                pSymbol->Offset,
                pSymbol->Size,
                pSymbol->Data.Type,
                pPOU->Id ) ) != 0 )
        {
            break;
        }

        pData = NULL;
        switch( pSymbol->Data.Type )
        {
            case TYPE_BOOL:
            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                pData = ( char * ) &pSymbol->Data.Value.B1;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                pData = ( char * ) &pSymbol->Data.Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                pData = ( char * ) &pSymbol->Data.Value.B32;
                break;

            case TYPE_LINT:
            case TYPE_ULINT:

                pData = ( char * ) &pSymbol->Data.Value.B64;
                break;

            case TYPE_REAL:

                pData = ( char * ) &pSymbol->Data.Value.Float;
                break;

            case TYPE_LREAL:

                pData = ( char * ) &pSymbol->Data.Value.Double;
                break;

            default:

                break;
        }

        if( pData != NULL )
        {
            for( uByte = 0; uByte < pSymbol->Size; ++uByte )
            {
                if( ( nResult = _symboltable_writeLine( pContext, pFile, "%02x%s",
                            ( pData[ uByte ] & 0xff ),
                            ( uByte < ( pSymbol->Size - 1 ) ) ? " " : "" ) ) != 0 )
                {
                    fclose( pFile );
                    unlink( szFilename );

                    return nResult;
                }
            }
        }

        if( ( nResult = _symboltable_writeLine( pContext, pFile, "\",\"%s\"\n",
                ( pSymbol->Direct != OPERAND_DIRECT_NONE ) ?
                        pSymbol->Location :
                        "" ) ) != 0 )
        {
            break;
        }
    }

    fclose( pFile );
    if( nResult != 0 )
    {
        unlink( szFilename );
    }

    return 0;
}

