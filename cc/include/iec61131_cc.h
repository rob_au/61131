#ifndef __IEC61131_CC_H
#define __IEC61131_CC_H


#include <stdarg.h>

#include "iec61131_list.h"
#include "iec61131_symbol.h"
#include "iec61131_token.h"
#include "memblock.h"


typedef struct _IEC61131_CC_TABLE
{
    IEC61131_TOKEN_INSTRUCTION **Entries;

    size_t Count;
}
IEC61131_CC_TABLE;


/*!
    \struct IEC61131_CC_SYMBOL_TABLE
*/

typedef struct _IEC61131_CC_SYMBOL_TABLE
{
    /*!
        \param Symbols

        This data structure member is intended to hold a dynamically reallocated
        array of IEC61131_SYMBOL data structures containing information about IEC
        61131-3 symbols parsed from a source file.
    */

    IEC61131_SYMBOL *Entries;

    /*!
        \param Data

        This data structure member is intended to hold an aligned block of memory
        holding values associated with IEC 61131-3 symbols parsed from a source file.
        This memory block is aligned to the memory alignment size specified by the
        Align member of the IEC61131_CC data structure and in turn is intended to
        allow for ready access to symbol values following linking _without_
        necessitating reference or iteration through symbol table definitions.
    */

    unsigned char *Data;

    /*!
        \param Count

        This data structure member is intended to hold the number of IEC 61131-3
        symbols parsed and represented within the Symbols and Data pointers within
        this data structure.
    */

    size_t Count;

    /*!
        \param Offset

        This data structure member is intended to hold an offset pointer to the next
        byte for allocation within the aligned block of memory pointed to by the
        Data data structure member.  In turn, this data structure member can be
        stated to hold the total number of data bytes allocated in association with
        the Data data structure member.
    */

    size_t Offset;
}
IEC61131_CC_SYMBOL_TABLE;


/*!
    \struct IEC61131_CC

    This data structure serves as the primary contextual data structure for
    IEC61131 compiler operations.
*/

typedef struct _IEC61131_CC
{
    /*!
        \param Name

        This data structure member is intended to hold the name of the
        IEC61131-3 compiler binary for use within diagnostic output and error
        messages.
    */

    const char *Name;

    /*!
        \param Source

        This data structure member is intended to hold the name of the
        IEC61131-3 source file for use within diagnostic output and error
        messages.
    */

    const char *Source;

    /*!
        \param Align

        This data structure member is intended to hold memory size alignment
        required for a given target platform.
    */

    unsigned long Align;

    /*!
        \param Size;

        This data structure member is intended to hold the maximum byte size for 
        the compiled IEC 61131-3 byte code.  This parameter is intended to be used
        where there are limits in the target platform for IEC 61131-3 virtual 
        machine application storage.

        If the value of zero is employed for this data structure member, no limit 
        is employed for compiled IEC 61131-3 byte code generation.
    */

    unsigned long Size;

    /*!
        \param Library

        This data structure member is intended to hold a list of tokens associated
        with a IEC 61131-3 library.
    */

    IEC61131_LIST Library;


    /*!
        \param ByteCode

        This data structure member is a pointer to a block of dynamically
        allocated memory where IEC 61131-3 byte code is written prior to output to
        an object file.  This memory block is particularly important given the 
        multi-pass nature of this IEC 61131-3 compiler with respect to linking 
        operations.
    */

    MEMBLOCK ByteCode;


    /*!
        \param LabelTable

        This data structure member is a complex data structure intended to hold a
        list of unresolved execution jump and function call IEC 61131-3 
        instructions parsed from a source file.  This information is used for the 
        subsequent linking and population of target program counter offsets 
        following the generation of IEC 61131-3 byte code.
    */

    IEC61131_CC_TABLE InstructionTable;

    IEC61131_CC_TABLE LabelTable;


    /*!
        \param SymbolTable

        This data structure member is a complex data structure intended to hold a
        list of IEC 61131-3 symbols and associated with the list of IEC 61131-3
        compiler tokens parsed from a source file.  This complex structure
        encapsulates a number of members holding information associated with the
        symbol table logical data structure.
    */

    IEC61131_CC_SYMBOL_TABLE SymbolTable;

    unsigned char Debug;
}
IEC61131_CC;


#endif
