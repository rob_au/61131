#ifndef __SYMBOL_H
#define __SYMBOL_H


#include <stdbool.h>

#include "iec61131_cc.h"
#include "iec61131_symbol.h"
#include "iec61131_types.h"


IEC61131_SYMBOL * symbol_getSymbol( IEC61131_CC *pContext, char *pName, void *pScope, bool bResolve );

unsigned int symbol_getSymbolOffset( IEC61131_CC *pContext, char *pName, void *pScope );

IEC61131_TYPE symbol_getSymbolType( IEC61131_CC *pContext, char *pChar, void *pScope );


#endif
