#ifndef __BASE_H
#define __BASE_H


#include <stdint.h>


typedef enum _BASE_TYPE
{
    TYPE_UNKNOWN = 0,
    TYPE_LIST,
    TYPE_LIST_ELEMENT,
    TYPE_TOKEN,
    TYPE_SYMBOL,
    TYPE_MEMBLOCK,
}
BASE_TYPE;


/*!
    \struct BASE
    \brief Common base data structure

    This data structure is intended to serve as a common base for all complex
    data structures employed within this code base and provides elements for
    structure identification and destruction.  When employed in this manner, it
    is assumed that this common base data structure will be the first member of
    the derived data structure.  In this way, members of this common base data
    structure may be accessed by casting a pointer to the derived data
    structure as a pointer to this type definition.

    It should be noted that all complex data structures employed within this
    code base are assumed to be derived from this common base data structure.
*/

typedef struct _BASE
{
    /*!
        \param Type

        This data structure member is intended to hold a unique identifier for the
        data structure type.  This in turn is intended to allow for differential
        processing of data structures when passed by reference between subroutines.
    */

    uint32_t Type;

    /*!
        \param Destroy

        This data structure member is intended to store a pointer to the function
        deemed responsible for the destruction of instances of this data structure.
    */

    void ( *Destroy )( void *Self );
}
BASE;


/*!
    The following macro definitions have been declared for ease-of-use in the
    population and accessing of BASE-derived data structures.
*/

#define BASE_DESTROY( __Object )                        ( base_destroy( __Object ) )

#define BASE_SET_DESTROY( __Object, __Callback )        ( ( ( BASE * ) __Object )->Destroy = ( __Callback ) )

#define BASE_SET_TYPE( __Object, __Type )               ( ( ( BASE * ) __Object )->Type = ( __Type ) )

#define BASE_TYPE( __Object )                           ( ( ( BASE * ) __Object )->Type )

#define BASE_UNUSED( __Object )                         ( ( void ) ( ( __Object ) == ( __Object ) ) )


void base_destroy( void *Object );


#endif
