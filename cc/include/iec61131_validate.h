#ifndef __IEC61131_VALIDATE_H
#define __IEC61131_VALIDATE_H


#include "iec61131_cc.h"
#include "iec61131_token.h"


int iec61131_validatePOU( IEC61131_CC *Context, IEC61131_TOKEN_POU *POU );


#endif
