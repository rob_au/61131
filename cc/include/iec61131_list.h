#ifndef __IEC61131_LIST_H
#define __IEC61131_LIST_H


#include <stdlib.h>

#include "base.h"


/*!
    \struct IEC61131_LIST_ELEMENT
    \brief List element data structure
*/

typedef struct _IEC61131_LIST_ELEMENT
{
    BASE Base;

    struct _IEC61131_LIST_ELEMENT *Next;

    struct _IEC61131_LIST_ELEMENT *Previous;

    void *Data;
}
IEC61131_LIST_ELEMENT;


/*!
    \struct IEC61131_LIST
    \brief List data structure

    This data structure implements a linked-list structure which is employed
    within this code base.
*/

typedef struct _IEC61131_LIST
{
    /*!
        \param Base

        The first member of this data structure is assumed to be the IEC61131_BASE
        data structure from which all complex data structures ad assumed to be
        derived.  This data structure member provides means for common
        identification and destruction accessors within all complex data structure
        types.
    */

    BASE Base;

    /*!
        \param Head
        \param Tail

        The following members of this data structure are intended to hold pointers
        to the head and tail elements of the IEC61131_LIST linked-list data
        structure.
    */

    IEC61131_LIST_ELEMENT *Head;

    IEC61131_LIST_ELEMENT *Tail;

    /*!
        \param Size

        This data structure member is intended to hold the number of elements
        stored within the IEC61131_LIST linked-list data structure.
    */

    size_t Size;
}
IEC61131_LIST;


/*!
    The following macro definitions have been declared for ease-of-use in the
    population and manipulation of IEC61131_LIST data structures.
*/

#define IEC61131_NEW_LIST()                             ( iec61131_allocateList() )

#define IEC61131_LIST_CONSOLIDATE( __List, __List2 )    ( iec61131_listConsolidate( __List, __List2 ) )

#define IEC61131_LIST_INIT( __List )                    ( iec61131_listInitialise( __List ) )

#define IEC61131_LIST_INSERT_HEAD( __List, __Data )     ( iec61131_listInsertHead( __List, __Data ) )

#define IEC61131_LIST_INSERT_TAIL( __List, __Data )     ( iec61131_listInsertTail( __List, __Data ) )

#define IEC61131_LIST_HEAD( __List )                    ( ( __List )->Head )

#define IEC61131_LIST_TAIL( __List )                    ( ( __List )->Tail )

#define IEC61131_LIST_SIZE( __List )                    ( ( __List )->Size )

#define IEC61131_LIST_NEXT( __Element )                 ( ( __Element )->Next )

#define IEC61131_LIST_PREVIOUS( __Element )             ( ( __Element )->Previous )

#define IEC61131_LIST_FOREACH( __List, __Code )                                 \
    {                                                                           \
        IEC61131_LIST_ELEMENT *Element;                                         \
        for( Element = ( __List )->Head; Element; Element = Element->Next )     \
        {                                                                       \
            ( __Code );                                                         \
        }                                                                       \
    }


IEC61131_LIST * iec61131_allocateList( void );

void iec61131_listConsolidate( IEC61131_LIST *List, IEC61131_LIST *List2 );

void iec61131_listInitialise( IEC61131_LIST *List );

int iec61131_listInsertHead( IEC61131_LIST *List, void *Data );

int iec61131_listInsertTail( IEC61131_LIST *List, void *Data );

int iec61131_listRemove( IEC61131_LIST *List, IEC61131_LIST_ELEMENT *Element, void **Data );


#endif
