#ifndef __IEC61131_LINK_H
#define __IEC61131_LINK_H


#include "iec61131_token.h"


/*!
    \struct IEC61131_LINK_ENTRY

    This data structure is intended to be employed in the storage of execution 
    jump and POU call instructions as these are encountered in IEC 61131-3 byte 
    code during compilation operations.  This is so that these locations can be 
    subsequently populated during the linking compilation phase once all target 
    label and POU byte code locations are defined.
*/

typedef struct _IEC61131_LINK_ENTRY
{
    /*!
        \param PC

        This data structure is intended to store the indexed address within the 
        application byte code (program counter) where the corresponding execution 
        jump or POU call instruction is located.
    */

    unsigned int PC;

    /*!
        \param POU
    */

    unsigned int POU;

    /*!
        \param Instr
    */

    IEC61131_TOKEN_INSTRUCTION *Instr;
}
IEC61131_LINK_ENTRY;


/*!
    \struct IEC61131_LINK_TABLE
*/

typedef struct _IEC61131_LINK_TABLE
{
    /*!
        \param Entries

        This data structure member is intended to hold a dynamically reallocated
        array of IEC61131_LINK_ENTRY data structures containing information about
        unresolved execution jump and POU call instructions encountered in
        IEC 61131-3 byte code during compilation operations.
    */

    IEC61131_LINK_ENTRY *Entries;

    /*!
        \param Count

        This data structure is intended to hold the number of unresolved execution
        jump and POU call instructions encountered in IEC 61131-3 byte code during
        compilation operations.
    */

    size_t Count;
}
IEC61131_LINK_TABLE;


#endif
