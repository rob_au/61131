#ifndef __MEMBLOCK_H
#define __MEMBLOCK_H


#include <stdint.h>


#define MEMBLOCK_ALLOCATION_SIZE        (1024)


/*!
    \struct MEMBLOCK

    This data structure is intended to encapsulate - and provide members to
    manage - a continguous memory block that may be dynamically expanded as
    data bytes are appended to the block.
*/

typedef struct _MEMBLOCK
{
    /*!
        \param Data

        This data structure member holds a pointer to the memory that is
        dynamically allocated and managed in association with this data structure.
    */

    char *Data;

    /*!
        \param End
    */

    unsigned int End;

    /*!
        \param Size
    */

    unsigned int Size;
}
MEMBLOCK;


void memblock_initialise( MEMBLOCK *Block );

void memblock_destroy( void *pObject );

void memblock_dump( MEMBLOCK *pBlock, unsigned int uStart, unsigned int uFinish );

int memblock_read( MEMBLOCK *pBlock, unsigned int uIndex, char *pData, unsigned int uLength );

int memblock_write( MEMBLOCK *pBlock, unsigned int uIndex, char *pData, unsigned int uLength );

int memblock_append( MEMBLOCK *pBlock, char sData );


#endif
