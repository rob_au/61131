#ifndef __IEC61131_POU_H
#define __IEC61131_POU_H


#include "iec61131_cc.h"


int iec61131_getPOUType( IEC61131_CC *pContext, char *pName, void *pScope );


#endif
