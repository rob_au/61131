#ifndef __IEC61131_ERROR_H
#define __IEC61131_ERROR_H


#include <stdio.h>


#ifndef YYERROR
#define YYERROR
#endif

#define IEC61131_ERROR( __Source, __Line, __Column, __Msg ) {	\
	printf( "%s:%u:%u: error: %s\n",			\
		__Source,					\
		__Line,						\
		__Column,					\
		__Msg );					\
	YYERROR;						\
	}

#endif
