#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "memblock.h"


int
memblock_append( MEMBLOCK *pBlock, char sData )
{
    return memblock_write( pBlock, pBlock->End, &sData, 1 );
}


void
memblock_destroy( void *pObject )
{
    MEMBLOCK *pBlock;


    pBlock = ( MEMBLOCK * ) pObject;
    if( pBlock->Data != NULL )
    {
        free( pBlock->Data );
        pBlock->Data = NULL;
    }
    pBlock->End = 0;
    pBlock->Size = 0;
}


void
memblock_dump( MEMBLOCK *pBlock, unsigned int Start, unsigned int Finish )
{
    unsigned int uIndex;


    if( ( Finish == 0 ) ||
            ( Finish < Start ) )
    {
        Finish = pBlock->End;
    }

    for( uIndex = Start; uIndex < Finish; ++uIndex )
    {
        printf( "%02x ", pBlock->Data[ uIndex ] & 0xff );
    }
    printf( "\n" );
}


void
memblock_initialise( MEMBLOCK *pBlock )
{
    pBlock->Data = NULL;
    pBlock->End = 0;
    pBlock->Size = 0;
}


int
memblock_read( MEMBLOCK *pBlock, unsigned int uIndex, char *pData, unsigned int uLength )
{
    if( ( uIndex + uLength ) >= pBlock->End )
    {
        return -EINVAL;
    }

    ( void ) memcpy( pBlock->Data + uIndex, pData, uLength );

    return uLength;
}


int
memblock_write( MEMBLOCK *pBlock, unsigned int uIndex, char *pData, unsigned int uLength )
{
    unsigned int uBytes, uSize;


    uBytes = ( uIndex + uLength );
    if( uBytes > pBlock->Size )
    {
        uSize = ( ( uBytes / MEMBLOCK_ALLOCATION_SIZE ) * MEMBLOCK_ALLOCATION_SIZE ) +
                ( ( ( uBytes % MEMBLOCK_ALLOCATION_SIZE ) > 0 ) ?
                        MEMBLOCK_ALLOCATION_SIZE :
                        0 );

        if( ( pBlock->Data = ( char * ) realloc( pBlock->Data, uSize ) ) == NULL )
        {
            return -errno;
        }

        pBlock->Size = uSize;
    }

    ( void ) memcpy( pBlock->Data + uIndex, pData, uLength );
    pBlock->End = ( uBytes > pBlock->End ) ? uBytes : pBlock->End;

    return uLength;
}


