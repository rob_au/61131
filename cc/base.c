#include <stdlib.h>

#include "base.h"


/*!
    \fn void base_destroy( void *pObject )
    \brief Function for the destruction of data structures derived from common base
    \param pObject Data structure pointer
    \returns No return value

    This function will call any destruction function pointers declared for data
    structures derived from the BASE common base data structure.
*/

void
base_destroy( void *pObject )
{
    BASE *pBase;


    pBase = ( BASE * ) pObject;
    if( pBase != NULL )
    {
        if( pBase->Destroy != NULL )
        {
            pBase->Destroy( pBase );
        }
    }
}
