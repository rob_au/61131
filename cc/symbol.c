#include <string.h>
#include <limits.h>

#include "iec61131_external.h"
#include "symbol.h"

#include "iec61131.h"


static int _symbol_compareSymbolNames( const void *pLeft, const void *pRight );

static unsigned int _symbol_getFieldOffset( IEC61131_CC *pContext, char *pName, char *pField );


static int
_symbol_compareSymbolNames( const void *pLeft, const void *pRight )
{
    IEC61131_SYMBOL *pSymbol1, *pSymbol2;


    pSymbol1 = ( IEC61131_SYMBOL * ) pLeft;
    pSymbol2 = ( IEC61131_SYMBOL * ) pRight;

    if( pSymbol1->Scope == pSymbol2->Scope )
    {
        return strcmp( pSymbol1->Name, pSymbol2->Name );
    }

    return ( pSymbol1->Scope < pSymbol2->Scope ) ? -1 : 1;
}


/*
    The following function will return the offset within the symbol table where 
    a field associated with a function block instance is stored.  This 
    operation is performed by first searching through those functions 
    implemented in IEC 61131-3 languages and then through those implemented 
    through the C-based library interface.  This arrangement is deliberate in 
    that it allows for functions implemented in IEC 61131-3 languages to 
    replace or supplant those implemented in the C-based library interface.
*/

static unsigned int
_symbol_getFieldOffset( IEC61131_CC *pContext, char *pName, char *pField )
{
    EXTERNAL_FUNCTION_BLOCK_DECLARATION *pDeclaration;
    EXTERNAL_PARAMETER eParameter;
    IEC61131_LIST_ELEMENT *pElement1, *pElement2;
    IEC61131_TOKEN *pType;
    IEC61131_TOKEN_POU *pPOU;
    IEC61131_TOKEN_VARIABLE *pVariable;
    IEC61131_TYPE eType;
    unsigned int uId, uIndex, uOffset, uSize, uType;


    //  IEC 61131-3 language function blocks

    for( pElement1 = IEC61131_LIST_HEAD( &pContext->Library );
            pElement1;
            pElement1 = IEC61131_LIST_NEXT( pElement1 ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement1->Data;
        uId = ( ( IEC61131_TOKEN * ) pPOU )->Id;

        if( ( uId != FUNCTION_BLOCK ) ||
                ( strcmp( pPOU->Name, pName ) != 0 ) )
        {
            continue;
        }


        /*
            Iterate through each of the members of the function block instance, adding 
            up the cumulative offset until the member field requested has been located.
        */

        uOffset = 0;
        for( pElement2 = IEC61131_LIST_HEAD( pPOU->Variables );
                pElement2;
                pElement2 = IEC61131_LIST_NEXT( pElement2 ) )
        {
            pVariable = ( IEC61131_TOKEN_VARIABLE * ) pElement2->Data;
            pType = ( IEC61131_TOKEN * ) pVariable->Type;

            if( strcmp( pVariable->Name, pField ) == 0 )
            {
                return uOffset;
            }

            uType = iec61131_symbolTypeMapping( pType->Id );
            uSize = iec61131_symbolTypeSize( uType );
            if( ( uSize % pContext->Align ) != 0 )
            {
                uSize += ( pContext->Align - ( uSize % pContext->Align ) );
            }
            uOffset += uSize;
        }
    }

    //  C-based library function blocks

    for( uIndex = 0; uIndex < IEC61131_FUNCTION_BLOCK_COUNT; ++uIndex )
    {
        pDeclaration = ( EXTERNAL_FUNCTION_BLOCK_DECLARATION * ) &IEC61131_FUNCTION_BLOCK[ uIndex ];
        if( ( pDeclaration->Name == NULL ) ||
                ( pDeclaration->Size == 0 ) ||
                ( pDeclaration->Parameter == NULL ) ||
                ( pDeclaration->Pointer == NULL ) )
        {
            continue;
        }
        if( strcasecmp( pDeclaration->Name, pName ) != 0 )
        {
            continue;
        }

        if( pDeclaration->Parameter( pField, &eParameter, &uOffset, &eType ) != 0 )
        {
            return 0;
        }

        return uOffset;
    }

    return 0;
}


/*
    The following function is intended to resolve a symbol name and scope to a 
    pointer to the corresponding IEC61131_SYMBOL data structure employed by the 
    IEC 61131-3 compiler.  This involves a two-step process where an attempt is 
    first made to attempt to resolve the symbol name directly and secondly to a 
    symbol within a function block scope (which may be referenced by way of 
    <instance>.<parameter> syntax).
*/

IEC61131_SYMBOL * 
symbol_getSymbol( IEC61131_CC *pContext, char *pName, void *pScope, bool bResolve )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_SYMBOL *pSymbol, stSymbol;
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_POU *pPOU;
    char *pField;
    unsigned int uId;


    ( void ) memset( &stSymbol, 0, sizeof( stSymbol ) );
    ( void ) strncpy( stSymbol.Name, pName, sizeof( stSymbol.Name ) );
    if( ( pField = strchr( stSymbol.Name, '.' ) ) != NULL )
    {
        *pField++ = '\0';
    }
    stSymbol.Scope = pScope;

    if( ( pSymbol = bsearch( &stSymbol,
            pContext->SymbolTable.Entries,
            pContext->SymbolTable.Count,
            sizeof( IEC61131_SYMBOL ),
            _symbol_compareSymbolNames ) ) == NULL )
    {
        return NULL;
    }

    if( bResolve == false )
    {
        return pSymbol;
    }


    /*
        The following code searches for a symbol within the scope of an IEC 61131-3 
        language function block based upon the employ of <instance>.<parameter> 
        syntax.

        TODO: Is there a requirement for handling of the <instance>.<parameter> 
        syntax for C language function blocks?!
    */

    if( pField != NULL )
    {
        pLiteral = ( IEC61131_TOKEN_LITERAL * ) pSymbol->Type;

        for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
                pElement;
                pElement = IEC61131_LIST_NEXT( pElement ) )
        {
            pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;
            uId = ( ( IEC61131_TOKEN * ) pPOU )->Id;
            if( ( uId != FUNCTION_BLOCK ) ||
                    ( strcmp( pPOU->Name, pLiteral->Name ) != 0 ) )
            {
                continue;
            }

            return symbol_getSymbol( pContext, pField, pPOU, true );
        }
    }

    return pSymbol;
}


/*!
    \fn unsigned int symbol_getSymbolOffset( IEC61131_CC *pContext, char *pName, void *pScope )
    \brief Return symbol table storage offset for supplied symbol and scope
    \param pContext Pointer to IEC 61131-3 contextual data structure
*/

unsigned int 
symbol_getSymbolOffset( IEC61131_CC *pContext, char *pName, void *pScope )
{
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_POU *pPOU;
    char *pField, szName[LINE_MAX];
    unsigned int uId, uOffset;


    pPOU = ( IEC61131_TOKEN_POU * ) pScope;
    uId = ( ( IEC61131_TOKEN * ) pPOU )->Id;

    switch( uId )
    {
        case FUNCTION_BLOCK:

            return _symbol_getFieldOffset( pContext, pPOU->Name, pName );
            /* break; */

        case FUNCTION:
        case PROGRAM:

            ( void ) memset( szName, 0, sizeof( szName ) );
            ( void ) strncpy( szName, pName, sizeof( szName ) );
            if( ( pField = strchr( szName, '.' ) ) != NULL )
            {
                *pField++ = '\0';
            }

            if( ( pSymbol = symbol_getSymbol( pContext, szName, pScope, false ) ) == NULL )
            {
                return 0;
            }

            uOffset = pSymbol->Offset;
            if( pField != NULL )
            {
                pLiteral = ( IEC61131_TOKEN_LITERAL * ) pSymbol->Type;
                uOffset += _symbol_getFieldOffset( pContext, pLiteral->Name, pField );
            }

            return uOffset;
            /* break; */
            
        default:

            break;
    }

    return 0;
}


/*!
    \fn IEC61131_TYPE symbol_getSymbolType( IEC61131_CC *pContext, char *pName, void *pScope )

    The following function is intended to return the IEC 61131-3 type 
    associated with a symbol.  While it would be easy to assume that this could 
    be performed with reference to the symbol_getSymbol function above, there 
    is additional complexity which needs to be considered due to the deferral 
    of parameter definition and processing for C-based function blocks.  That 
    is, because parameters associated with C-based function blocks are defined 
    and validated through execution of a C-based function, rather than by way 
    of reference to a local array of symbols, further considerations must be 
    taken when resolving symbol offset and type.
*/

IEC61131_TYPE 
symbol_getSymbolType( IEC61131_CC *pContext, char *pName, void *pScope )
{
    EXTERNAL_FUNCTION_BLOCK_DECLARATION *pDeclaration;
    EXTERNAL_PARAMETER eParameter;
    IEC61131_SYMBOL *pSymbol, stSymbol;
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TYPE eType;
    unsigned int uIndex, uOffset;
    char *pField;


    if( ( ( pSymbol = symbol_getSymbol( pContext, pName, pScope, true ) ) != NULL ) &&
            ( pSymbol->Data.Type != TYPE_NONE ) )
    {
        return pSymbol->Data.Type;
    }

    //  C-based library function blocks

    ( void ) memset( &stSymbol, 0, sizeof( stSymbol ) );
    ( void ) strncpy( stSymbol.Name, pName, sizeof( stSymbol.Name ) );
    if( ( pField = strchr( stSymbol.Name, '.' ) ) == NULL )
    {
        return TYPE_NONE;
    }
    *pField++ = '\0';

    if( ( pSymbol = symbol_getSymbol( pContext, stSymbol.Name, pScope, false ) ) == NULL )
    {
        return TYPE_NONE;
    }

    pLiteral = ( IEC61131_TOKEN_LITERAL * ) pSymbol->Type;

    for( uIndex = 0; uIndex < IEC61131_FUNCTION_BLOCK_COUNT; ++uIndex )
    {
        pDeclaration = ( EXTERNAL_FUNCTION_BLOCK_DECLARATION * ) &IEC61131_FUNCTION_BLOCK[ uIndex ];
        if( ( pDeclaration->Name == NULL ) ||
                ( pDeclaration->Size == 0 ) ||
                ( pDeclaration->Parameter == NULL ) ||
                ( pDeclaration->Pointer == NULL ) )
        {
            continue;
        }
        if( strcasecmp( pDeclaration->Name, pLiteral->Name ) != 0 )
        {
            continue;
        }

        if( pDeclaration->Parameter( pField, &eParameter, &uOffset, &eType ) == 0 )
        {
            return eType;
        }

        break;
    }

    return TYPE_NONE;
}

