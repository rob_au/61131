#include <string.h>
#include <errno.h>
#include <float.h>
#include <strings.h>

#include "iec61131_cc.h"

#include "iec61131.h"

#include "iec61131_parse.h"


int
iec61131_castBitstring( IEC61131_TOKEN *Value, IEC61131_TYPE Type )
{
    return iec61131_castInteger( Value, Type );
}


int 
iec61131_castInteger( IEC61131_TOKEN *Value, IEC61131_TYPE Type )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    uint64_t uVal64, uVal64_2;


    pLiteral = ( IEC61131_TOKEN_LITERAL * ) Value;
    uVal64 = ( uint64_t ) pLiteral->Value.Value.U64;

    if( pLiteral->Value.Type == Type )
    {
        pLiteral->Fixed_Type = BOOL_TRUE;
        return 0;
    }
    if( pLiteral->Fixed_Type != BOOL_FALSE )
    {
        return -1;
    }

    switch( Type )
    {
        case TYPE_BOOL:

            pLiteral->Value.Value.B1 = ( uint8_t ) ( uVal64 > 0 ) ? BOOL_TRUE : BOOL_FALSE;
            pLiteral->Value.Type = TYPE_BOOL;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.B1;

            break;

        case TYPE_SINT:

            pLiteral->Value.Value.S8 = ( int8_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.S8;

            break;

        case TYPE_INT:

            pLiteral->Value.Value.S16 = ( int16_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.S16;

            break;

        case TYPE_DINT:

            pLiteral->Value.Value.S32 = ( int32_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.S32;

            break;

        case TYPE_LINT:

            pLiteral->Value.Type = Type;
            uVal64_2 = uVal64;
            break;

        case TYPE_BYTE:
        case TYPE_USINT:

            pLiteral->Value.Value.U8 = ( uint8_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.U8;

            break;

        case TYPE_WORD:
        case TYPE_UINT:

            pLiteral->Value.Value.U16 = ( uint16_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.U16;

            break;

        case TYPE_DWORD:
        case TYPE_UDINT:

            pLiteral->Value.Value.U32 = ( uint32_t ) uVal64;
            pLiteral->Value.Type = Type;

            uVal64_2 = ( uint64_t ) pLiteral->Value.Value.U32;

            break;

        case TYPE_LWORD:
        case TYPE_ULINT:

            pLiteral->Value.Type = Type;
            uVal64_2 = uVal64;
            break;

        case TYPE_REAL:

            pLiteral->Value.Value.Float = ( float ) uVal64;
            pLiteral->Value.Type = Type;

            return 0;

        default:

            return -1;
    }

    if( uVal64 == uVal64_2 )
    {
        pLiteral->Fixed_Type = BOOL_TRUE;
        return 0;
    }

    return -1;
}


int
iec61131_castReal( IEC61131_TOKEN *Value, IEC61131_TYPE Type ) 
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    double dVal, dVal_2;


    pLiteral = ( IEC61131_TOKEN_LITERAL * ) Value;
    dVal = ( double ) pLiteral->Value.Value.Double;

    if( pLiteral->Value.Type == Type )
    {
        pLiteral->Fixed_Type = BOOL_TRUE;
        return 0;
    }
    if( pLiteral->Fixed_Type != BOOL_FALSE )
    {
        return -1;
    }

    switch( Type )
    {
        case TYPE_REAL:

            pLiteral->Value.Value.Float = ( float ) dVal;
            pLiteral->Value.Type = TYPE_REAL;

            dVal_2 = ( double ) pLiteral->Value.Value.Float;

            break;

        case TYPE_LREAL:

            pLiteral->Value.Type = TYPE_LREAL;
            dVal_2 = dVal;
            break;

        default:

            return -1;
    }

    if( abs( dVal - dVal_2 ) <= DBL_EPSILON )
    {
        pLiteral->Fixed_Type = BOOL_TRUE;
        return 0;
    }

    return -1;
}



IEC61131_TOKEN *
iec61131_parseBitstring( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column, unsigned int Base )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_RAW *pRaw;


    if( ( pLiteral = IEC61131_NEW_LITERAL( bit_string, NULL ) ) == NULL )
    {
        return NULL;
    }
    IEC61131_TOKEN_LINE( pLiteral, Line );
    IEC61131_TOKEN_COLUMN( pLiteral, Column );

    pRaw = ( IEC61131_TOKEN_RAW * ) Token;
    if( pRaw->Data != NULL )
    {
        errno = 0;
        pLiteral->Value.Value.U64 = strtoull( pRaw->Data, ( char ** ) NULL, Base );
        pLiteral->Value.Type = TYPE_LWORD;

        if( errno != 0 )
        {
            BASE_DESTROY( pLiteral );
            BASE_DESTROY( pRaw );

            return NULL;
        }

        pLiteral->Assigned = BOOL_TRUE;
    }

    BASE_DESTROY( pRaw );

    return ( IEC61131_TOKEN * ) pLiteral;
}


IEC61131_TOKEN * 
iec61131_parseBoolean( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_RAW *pRaw;


    if( ( pLiteral = IEC61131_NEW_LITERAL( boolean, NULL ) ) == NULL )
    {
        return NULL;
    }
    IEC61131_TOKEN_LINE( pLiteral, Line );
    IEC61131_TOKEN_COLUMN( pLiteral, Column );

    pRaw = ( IEC61131_TOKEN_RAW * ) Token;
    if( pRaw->Data != NULL )
    {
        /*
            While it would be preferable to use the strcasestr case-insensitive
            function in the following string matching code, this function is considered
            a non-standard extension to C89 and C99.  As a result, this function is
            only available within the string.h header file where _GNU_SOURCE is defined.
        */

        if( ( /* strcasestr */ strstr( pRaw->Data, "FALSE" ) != NULL ) ||
                ( /* strcasestr */ strstr( pRaw->Data, "BOOL#0" ) != NULL ) )
        {
            pLiteral->Value.Value.B1 = BOOL_FALSE;
        }
        else if( ( /* strcasestr */ strstr( pRaw->Data, "TRUE" ) != NULL ) ||
                ( /* strcasestr */ strstr( pRaw->Data, "BOOL#1" ) != NULL ) )
        {
            pLiteral->Value.Value.B1 = BOOL_TRUE;
        }
        else
        {
            BASE_DESTROY( pLiteral );
            BASE_DESTROY( pRaw );

            return NULL;
        }

        pLiteral->Value.Type = TYPE_BOOL;
        pLiteral->Assigned = BOOL_TRUE;
    }

    BASE_DESTROY( pRaw );

    return ( IEC61131_TOKEN * ) pLiteral;
}


IEC61131_TOKEN * 
iec61131_parseDuration( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_RAW *pRaw;
    int32_t nValue;
    char *pChar;


    if( ( pLiteral = IEC61131_NEW_LITERAL( duration, NULL ) ) == NULL )
    {
        return NULL;
    }
    IEC61131_TOKEN_LINE( pLiteral, Line );
    IEC61131_TOKEN_COLUMN( pLiteral, Column );

    pRaw = ( IEC61131_TOKEN_RAW * ) Token;
    if( pRaw->Data != NULL )
    {
        pLiteral->Value.Value.Time = 0;
        pChar = pRaw->Data;

        if( strncasecmp( pChar, "T#", 2 ) == 0 )
        {
            pChar += 2;
        }
        else if( strncasecmp( pChar, "TIME#", 5 ) == 0 )
        {
            pChar += 5;
        }

        for( ; strlen( pChar ) > 0; )
        {
            errno = 0;
            nValue = strtol( pChar, &pChar, 10 );

            if( errno != 0 )
            {
                BASE_DESTROY( pLiteral );
                BASE_DESTROY( pRaw );

                return NULL;
            }

            if( pChar == NULL )
            {
                pLiteral->Value.Value.Time += nValue;
            }
            else if( strncasecmp( pChar, "ms", 2 ) == 0 )
            {
                pLiteral->Value.Value.Time += nValue;
                pChar += 2;
            }
            else if( strncasecmp( pChar, "s", 1 ) == 0 )
            {
                pLiteral->Value.Value.Time += ( nValue * 1000 );
                ++pChar;
            }
            else if( strncasecmp( pChar, "m", 1 ) == 0 )
            {
                pLiteral->Value.Value.Time += ( nValue * 60000 );
                ++pChar;
            }
            else if( strncasecmp( pChar, "h", 1 ) == 0 )
            {
                pLiteral->Value.Value.Time += ( nValue * 3600000 );
                ++pChar;
            }
            else if( strncasecmp( pChar, "d", 1 ) == 0 )
            {
                pLiteral->Value.Value.Time += ( nValue * 86400000 );
                ++pChar;
            }
            else
            {
                BASE_DESTROY( pLiteral );
                BASE_DESTROY( pRaw );

                return NULL;
            }
        }

        pLiteral->Value.Type = TYPE_TIME;
        pLiteral->Assigned = BOOL_TRUE;
    }

    BASE_DESTROY( pRaw );

    return ( IEC61131_TOKEN * ) pLiteral;
}


IEC61131_TOKEN * 
iec61131_parseInteger( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column, unsigned int Base )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_RAW *pRaw;


    if( ( pLiteral = IEC61131_NEW_LITERAL( integer, NULL ) ) == NULL )
    {
        return NULL;
    }
    IEC61131_TOKEN_LINE( pLiteral, Line );
    IEC61131_TOKEN_COLUMN( pLiteral, Column );


    /*
        When attempting to parse a raw integer token, the integer bit width is
        assumed to be as wide as possible.  Further grammar parsing actions may
        subsequently be employed to limit or curtail integer value to meet parsed
        integer type.
    */

    pRaw = ( IEC61131_TOKEN_RAW * ) Token;
    if( pRaw->Data != NULL )
    {
        errno = 0;
        pLiteral->Value.Value.S64 = strtoll( pRaw->Data, ( char ** ) NULL, Base );
        pLiteral->Value.Type = TYPE_LINT;

        if( errno != 0 /* == ERANGE */ )
        {
            errno = 0;
            pLiteral->Value.Value.U64 = strtoull( pRaw->Data, ( char ** ) NULL, Base );
            pLiteral->Value.Type = TYPE_ULINT;

            if( errno != 0 )
            {
                BASE_DESTROY( pLiteral );
                BASE_DESTROY( pRaw );

                return NULL;
            }
        }

        pLiteral->Assigned = BOOL_TRUE;
    }

    BASE_DESTROY( pRaw );

    return ( IEC61131_TOKEN * ) pLiteral;
}


IEC61131_TOKEN * 
iec61131_parseReal( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column )
{
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_RAW *pRaw;


    if( ( pLiteral = IEC61131_NEW_LITERAL( real, NULL ) ) == NULL )
    {
        return NULL;
    }
    IEC61131_TOKEN_LINE( pLiteral, Line );
    IEC61131_TOKEN_COLUMN( pLiteral, Column );

    pRaw = ( IEC61131_TOKEN_RAW * ) Token;
    if( pRaw->Data != NULL )
    {
        errno = 0;
        pLiteral->Value.Value.Double = strtod( pRaw->Data, ( char ** ) NULL );
        pLiteral->Value.Type = TYPE_LREAL;

        if( errno != 0 )
        {
            BASE_DESTROY( pLiteral );
            BASE_DESTROY( pRaw );

            return NULL;
        }

        pLiteral->Assigned = BOOL_TRUE;
    }

    BASE_DESTROY( pRaw );

    return ( IEC61131_TOKEN * ) pLiteral;
}


