%{
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#include "iec61131_cc.h"
#include "iec61131_list.h"
#include "iec61131_parse.h"
#include "iec61131_token.h"
#include "iec61131_validate.h"


#define IEC61131_ERROR( line, column, msg )     \
    fprintf( stderr, "%s:%u:%u: error: %s\n",   \
            Context->Source,                    \
            (line),                             \
            (column),                           \
            (msg) );                            \
    YYERROR;


void _iec61131_setTokenScope( IEC61131_LIST *List, void *Scope );

IEC61131_TYPE _iec61131_tokenToType( int Id );

void yyerror( IEC61131_CC *Context, const char *Msg );

int yylex( void );

%}

%defines

%parse-param { IEC61131_CC *Context }

%union {
	struct _IEC61131_LIST *list;

	struct _IEC61131_TOKEN *token;
}

%token END_OF_INPUT 0

/* B.1.3.1 Elementary data types */

%token	<token>     BOOL
%token	<token>     BYTE
%token	<token>     WORD
%token	<token>     DWORD
%token	<token>     LWORD
%token	<token>     DATE
%token	<token>     TIME_OF_DAY
%token	<token>     TOD
%token	<token>     DATE_AND_TIME
%token	<token>     DT
%token	<token>     REAL
%token	<token>     LREAL
%token	<token>     USINT
%token	<token>     UINT
%token	<token>     UDINT
%token	<token>     ULINT
%token	<token>     SINT
%token	<token>     INT
%token	<token>     DINT
%token	<token>     LINT
%token	<token>     STRING
%token	<token>     WSTRING
%token	<token>     TIME

%token	F_EDGE
%token	R_EDGE

/* B.1.3.2 Generic data types */

%token	ANY
%token	ANY_DERIVED
%token	ANY_ELEMENTARY
%token	ANY_MAGNITUDE
%token	ANY_NUM
%token	ANY_REAL
%token	ANY_INT
%token	ANY_BIT
%token	ANY_STRING
%token	ANY_DATE

/* B.2.2 Operators */

%token  <token>     NOP
%token	<token>		LD
%token	<token>		LDN
%token	<token>		ST
%token	<token>		STN
%token	<token>		NOT
%token	<token>		S
%token	<token>		R
%token	<token>		S1
%token	<token>		R1
%token	<token>		CLK
%token	<token>		CU
%token	<token>		CD
%token	<token>		PV
%token	<token>		IN
%token	<token>		PT
%token	<token>		AND
%token	<token>		AND2
%token	<token>		OR
%token	<token>		XOR
%token	<token>		ANDN
%token	<token>		ANDN2
%token	<token>		ORN
%token	<token>		XORN
%token	<token>		ADD
%token	<token>		SUB
%token	<token>		MUL
%token	<token>		DIV
%token	<token>		MOD
%token	<token>		GT
%token	<token>		GT2
%token	<token>		GE
%token	<token>		GE2
%token	<token>		EQ
%token	<token>		LT
%token	<token>		LE
%token	<token>		LE2
%token	<token>		NE
%token	<token>		NE2
%token	<token>		PWR
%token	<token>		CAL
%token	<token>		CALC
%token	<token>		CALCN
%token	<token>		RET
%token	<token>		RETC
%token	<token>		RETCN
%token	<token>		JMP
%token	<token>		JMPC
%token	<token>		JMPCN

/* B.2.5.1.5 Standard Functions */

/* B.2.5.1.5.1 Type Conversion Functions */

%token	<token>		BOOL_TO_SINT
%token	<token>		BOOL_TO_INT
%token	<token>		BOOL_TO_DINT
%token	<token>		BOOL_TO_LINT
%token	<token>		BOOL_TO_USINT
%token	<token>		BOOL_TO_UINT
%token	<token>		BOOL_TO_UDINT
%token	<token>		BOOL_TO_ULINT
%token	<token>		BOOL_TO_REAL
%token	<token>		BOOL_TO_LREAL
%token	<token>		BOOL_TO_TIME
%token  <token>     BOOL_TO_DATE
%token	<token>		BOOL_TO_TOD
%token	<token>		BOOL_TO_DT
%token	<token>		BOOL_TO_STRING
%token	<token>		BOOL_TO_BYTE
%token	<token>		BOOL_TO_WORD
%token	<token>		BOOL_TO_DWORD
%token	<token>		BOOL_TO_LWORD
%token	<token>		BOOL_TO_WSTRING
%token	<token>		SINT_TO_BOOL
%token	<token>		SINT_TO_INT
%token	<token>		SINT_TO_DINT
%token	<token>		SINT_TO_LINT
%token	<token>		SINT_TO_USINT
%token	<token>		SINT_TO_UINT
%token	<token>		SINT_TO_UDINT
%token	<token>		SINT_TO_ULINT
%token	<token>		SINT_TO_REAL
%token	<token>		SINT_TO_LREAL
%token	<token>		SINT_TO_TIME
%token  <token>     SINT_TO_DATE
%token	<token>		SINT_TO_TOD
%token	<token>		SINT_TO_DT
%token	<token>		SINT_TO_STRING
%token	<token>		SINT_TO_BYTE
%token	<token>		SINT_TO_WORD
%token	<token>		SINT_TO_DWORD
%token	<token>		SINT_TO_LWORD
%token	<token>		SINT_TO_WSTRING
%token	<token>		INT_TO_BOOL
%token	<token>		INT_TO_SINT
%token	<token>		INT_TO_DINT
%token	<token>		INT_TO_LINT
%token	<token>		INT_TO_USINT
%token	<token>		INT_TO_UINT
%token	<token>		INT_TO_UDINT
%token	<token>		INT_TO_ULINT
%token	<token>		INT_TO_REAL
%token	<token>		INT_TO_LREAL
%token	<token>		INT_TO_TIME
%token  <token>     INT_TO_DATE
%token	<token>		INT_TO_TOD
%token	<token>		INT_TO_DT
%token	<token>		INT_TO_STRING
%token	<token>		INT_TO_BYTE
%token	<token>		INT_TO_WORD
%token	<token>		INT_TO_DWORD
%token	<token>		INT_TO_LWORD
%token	<token>		INT_TO_WSTRING
%token	<token>		DINT_TO_BOOL
%token	<token>		DINT_TO_SINT
%token	<token>		DINT_TO_INT
%token	<token>		DINT_TO_LINT
%token	<token>		DINT_TO_USINT
%token	<token>		DINT_TO_UINT
%token	<token>		DINT_TO_UDINT
%token	<token>		DINT_TO_ULINT
%token	<token>		DINT_TO_REAL
%token	<token>		DINT_TO_LREAL
%token	<token>		DINT_TO_TIME
%token  <token>     DINT_TO_DATE
%token	<token>		DINT_TO_TOD
%token	<token>		DINT_TO_DT
%token	<token>		DINT_TO_STRING
%token	<token>		DINT_TO_BYTE
%token	<token>		DINT_TO_WORD
%token	<token>		DINT_TO_DWORD
%token	<token>		DINT_TO_LWORD
%token	<token>		DINT_TO_WSTRING
%token	<token>		LINT_TO_BOOL
%token	<token>		LINT_TO_SINT
%token	<token>		LINT_TO_INT
%token	<token>		LINT_TO_DINT
%token	<token>		LINT_TO_USINT
%token	<token>		LINT_TO_UINT
%token	<token>		LINT_TO_UDINT
%token	<token>		LINT_TO_ULINT
%token	<token>		LINT_TO_REAL
%token	<token>		LINT_TO_LREAL
%token	<token>		LINT_TO_TIME
%token  <token>     LINT_TO_DATE
%token	<token>		LINT_TO_TOD
%token	<token>		LINT_TO_DT
%token	<token>		LINT_TO_STRING
%token	<token>		LINT_TO_BYTE
%token	<token>		LINT_TO_WORD
%token	<token>		LINT_TO_DWORD
%token	<token>		LINT_TO_LWORD
%token	<token>		LINT_TO_WSTRING
%token	<token>		USINT_TO_BOOL
%token	<token>		USINT_TO_SINT
%token	<token>		USINT_TO_INT
%token	<token>		USINT_TO_DINT
%token	<token>		USINT_TO_LINT
%token	<token>		USINT_TO_UINT
%token	<token>		USINT_TO_UDINT
%token	<token>		USINT_TO_ULINT
%token	<token>		USINT_TO_REAL
%token	<token>		USINT_TO_LREAL
%token	<token>		USINT_TO_TIME
%token  <token>     USINT_TO_DATE
%token	<token>		USINT_TO_TOD
%token	<token>		USINT_TO_DT
%token	<token>		USINT_TO_STRING
%token	<token>		USINT_TO_BYTE
%token	<token>		USINT_TO_WORD
%token	<token>		USINT_TO_DWORD
%token	<token>		USINT_TO_LWORD
%token	<token>		USINT_TO_WSTRING
%token	<token>		UINT_TO_BOOL
%token	<token>		UINT_TO_SINT
%token	<token>		UINT_TO_INT
%token	<token>		UINT_TO_DINT
%token	<token>		UINT_TO_LINT
%token	<token>		UINT_TO_USINT
%token	<token>		UINT_TO_UDINT
%token	<token>		UINT_TO_ULINT
%token	<token>		UINT_TO_REAL
%token	<token>		UINT_TO_LREAL
%token	<token>		UINT_TO_TIME
%token  <token>     UINT_TO_DATE
%token	<token>		UINT_TO_TOD
%token	<token>		UINT_TO_DT
%token	<token>		UINT_TO_STRING
%token	<token>		UINT_TO_BYTE
%token	<token>		UINT_TO_WORD
%token	<token>		UINT_TO_DWORD
%token	<token>		UINT_TO_LWORD
%token	<token>		UINT_TO_WSTRING
%token	<token>		UDINT_TO_BOOL
%token	<token>		UDINT_TO_SINT
%token	<token>		UDINT_TO_INT
%token	<token>		UDINT_TO_DINT
%token	<token>		UDINT_TO_LINT
%token	<token>		UDINT_TO_USINT
%token	<token>		UDINT_TO_UINT
%token	<token>		UDINT_TO_ULINT
%token	<token>		UDINT_TO_REAL
%token	<token>		UDINT_TO_LREAL
%token	<token>		UDINT_TO_TIME
%token  <token>     UDINT_TO_DATE
%token	<token>		UDINT_TO_TOD
%token	<token>		UDINT_TO_DT
%token	<token>		UDINT_TO_STRING
%token	<token>		UDINT_TO_BYTE
%token	<token>		UDINT_TO_WORD
%token	<token>		UDINT_TO_DWORD
%token	<token>		UDINT_TO_LWORD
%token	<token>		UDINT_TO_WSTRING
%token	<token>		ULINT_TO_BOOL
%token	<token>		ULINT_TO_SINT
%token	<token>		ULINT_TO_INT
%token	<token>		ULINT_TO_DINT
%token	<token>		ULINT_TO_LINT
%token	<token>		ULINT_TO_USINT
%token	<token>		ULINT_TO_UINT
%token	<token>		ULINT_TO_UDINT
%token	<token>		ULINT_TO_REAL
%token	<token>		ULINT_TO_LREAL
%token	<token>		ULINT_TO_TIME
%token  <token>     ULINT_TO_DATE
%token	<token>		ULINT_TO_TOD
%token	<token>		ULINT_TO_DT
%token	<token>		ULINT_TO_STRING
%token	<token>		ULINT_TO_BYTE
%token	<token>		ULINT_TO_WORD
%token	<token>		ULINT_TO_DWORD
%token	<token>		ULINT_TO_LWORD
%token	<token>		ULINT_TO_WSTRING
%token	<token>		REAL_TO_BOOL
%token	<token>		REAL_TO_SINT
%token	<token>		REAL_TO_INT
%token	<token>		REAL_TO_DINT
%token	<token>		REAL_TO_LINT
%token	<token>		REAL_TO_USINT
%token	<token>		REAL_TO_UINT
%token	<token>		REAL_TO_UDINT
%token	<token>		REAL_TO_ULINT
%token	<token>		REAL_TO_LREAL
%token	<token>		REAL_TO_TIME
%token  <token>     REAL_TO_DATE
%token	<token>		REAL_TO_TOD
%token	<token>		REAL_TO_DT
%token	<token>		REAL_TO_STRING
%token	<token>		REAL_TO_BYTE
%token	<token>		REAL_TO_WORD
%token	<token>		REAL_TO_DWORD
%token	<token>		REAL_TO_LWORD
%token	<token>		REAL_TO_WSTRING
%token	<token>		LREAL_TO_BOOL
%token	<token>		LREAL_TO_SINT
%token	<token>		LREAL_TO_INT
%token	<token>		LREAL_TO_DINT
%token	<token>		LREAL_TO_LINT
%token	<token>		LREAL_TO_USINT
%token	<token>		LREAL_TO_UINT
%token	<token>		LREAL_TO_UDINT
%token	<token>		LREAL_TO_ULINT
%token	<token>		LREAL_TO_REAL
%token	<token>		LREAL_TO_TIME
%token  <token>     LREAL_TO_DATE
%token	<token>		LREAL_TO_TOD
%token	<token>		LREAL_TO_DT
%token	<token>		LREAL_TO_STRING
%token	<token>		LREAL_TO_BYTE
%token	<token>		LREAL_TO_WORD
%token	<token>		LREAL_TO_DWORD
%token	<token>		LREAL_TO_LWORD
%token	<token>		LREAL_TO_WSTRING
%token	<token>		TIME_TO_BOOL
%token	<token>		TIME_TO_SINT
%token	<token>		TIME_TO_INT
%token	<token>		TIME_TO_DINT
%token	<token>		TIME_TO_LINT
%token	<token>		TIME_TO_USINT
%token	<token>		TIME_TO_UINT
%token	<token>		TIME_TO_UDINT
%token	<token>		TIME_TO_ULINT
%token	<token>		TIME_TO_REAL
%token	<token>		TIME_TO_LREAL
%token  <token>     TIME_TO_DATE
%token	<token>		TIME_TO_TOD
%token	<token>		TIME_TO_DT
%token	<token>		TIME_TO_STRING
%token	<token>		TIME_TO_BYTE
%token	<token>		TIME_TO_WORD
%token	<token>		TIME_TO_DWORD
%token	<token>		TIME_TO_LWORD
%token	<token>		TIME_TO_WSTRING
%token	<token>		TOD_TO_BOOL
%token	<token>		TOD_TO_SINT
%token	<token>		TOD_TO_INT
%token	<token>		TOD_TO_DINT
%token	<token>		TOD_TO_LINT
%token	<token>		TOD_TO_USINT
%token	<token>		TOD_TO_UINT
%token	<token>		TOD_TO_UDINT
%token	<token>		TOD_TO_ULINT
%token	<token>		TOD_TO_REAL
%token	<token>		TOD_TO_LREAL
%token	<token>		TOD_TO_TIME
%token  <token>     TOD_TO_DATE
%token	<token>		TOD_TO_DT
%token	<token>		TOD_TO_STRING
%token	<token>		TOD_TO_BYTE
%token	<token>		TOD_TO_WORD
%token	<token>		TOD_TO_DWORD
%token	<token>		TOD_TO_LWORD
%token	<token>		TOD_TO_WSTRING
%token	<token>		DT_TO_BOOL
%token	<token>		DT_TO_SINT
%token	<token>		DT_TO_INT
%token	<token>		DT_TO_DINT
%token	<token>		DT_TO_LINT
%token	<token>		DT_TO_USINT
%token	<token>		DT_TO_UINT
%token	<token>		DT_TO_UDINT
%token	<token>		DT_TO_ULINT
%token	<token>		DT_TO_REAL
%token	<token>		DT_TO_LREAL
%token	<token>		DT_TO_TIME
%token  <token>     DT_TO_DATE
%token	<token>		DT_TO_TOD
%token	<token>		DT_TO_STRING
%token	<token>		DT_TO_BYTE
%token	<token>		DT_TO_WORD
%token	<token>		DT_TO_DWORD
%token	<token>		DT_TO_LWORD
%token	<token>		DT_TO_WSTRING
%token	<token>		STRING_TO_BOOL
%token	<token>		STRING_TO_SINT
%token	<token>		STRING_TO_INT
%token	<token>		STRING_TO_DINT
%token	<token>		STRING_TO_LINT
%token	<token>		STRING_TO_USINT
%token	<token>		STRING_TO_UINT
%token	<token>		STRING_TO_UDINT
%token	<token>		STRING_TO_ULINT
%token	<token>		STRING_TO_REAL
%token	<token>		STRING_TO_LREAL
%token	<token>		STRING_TO_TIME
%token  <token>     STRING_TO_DATE
%token	<token>		STRING_TO_TOD
%token	<token>		STRING_TO_DT
%token	<token>		STRING_TO_BYTE
%token	<token>		STRING_TO_WORD
%token	<token>		STRING_TO_DWORD
%token	<token>		STRING_TO_LWORD
%token	<token>		STRING_TO_WSTRING
%token	<token>		BYTE_TO_BOOL
%token	<token>		BYTE_TO_SINT
%token	<token>		BYTE_TO_INT
%token	<token>		BYTE_TO_DINT
%token	<token>		BYTE_TO_LINT
%token	<token>		BYTE_TO_USINT
%token	<token>		BYTE_TO_UINT
%token	<token>		BYTE_TO_UDINT
%token	<token>		BYTE_TO_ULINT
%token	<token>		BYTE_TO_REAL
%token	<token>		BYTE_TO_LREAL
%token	<token>		BYTE_TO_TIME
%token  <token>     BYTE_TO_DATE
%token	<token>		BYTE_TO_TOD
%token	<token>		BYTE_TO_DT
%token	<token>		BYTE_TO_STRING
%token	<token>		BYTE_TO_WORD
%token	<token>		BYTE_TO_DWORD
%token	<token>		BYTE_TO_LWORD
%token	<token>		BYTE_TO_WSTRING
%token	<token>		WORD_TO_BOOL
%token	<token>		WORD_TO_SINT
%token	<token>		WORD_TO_INT
%token	<token>		WORD_TO_DINT
%token	<token>		WORD_TO_LINT
%token	<token>		WORD_TO_USINT
%token	<token>		WORD_TO_UINT
%token	<token>		WORD_TO_UDINT
%token	<token>		WORD_TO_ULINT
%token	<token>		WORD_TO_REAL
%token	<token>		WORD_TO_LREAL
%token	<token>		WORD_TO_TIME
%token  <token>     WORD_TO_DATE
%token	<token>		WORD_TO_TOD
%token	<token>		WORD_TO_DT
%token	<token>		WORD_TO_STRING
%token	<token>		WORD_TO_BYTE
%token	<token>		WORD_TO_DWORD
%token	<token>		WORD_TO_LWORD
%token	<token>		WORD_TO_WSTRING
%token	<token>		DWORD_TO_BOOL
%token	<token>		DWORD_TO_SINT
%token	<token>		DWORD_TO_INT
%token	<token>		DWORD_TO_DINT
%token	<token>		DWORD_TO_LINT
%token	<token>		DWORD_TO_USINT
%token	<token>		DWORD_TO_UINT
%token	<token>		DWORD_TO_UDINT
%token	<token>		DWORD_TO_ULINT
%token	<token>		DWORD_TO_REAL
%token	<token>		DWORD_TO_LREAL
%token	<token>		DWORD_TO_TIME
%token  <token>     DWORD_TO_DATE
%token	<token>		DWORD_TO_TOD
%token	<token>		DWORD_TO_DT
%token	<token>		DWORD_TO_STRING
%token	<token>		DWORD_TO_BYTE
%token	<token>		DWORD_TO_WORD
%token	<token>		DWORD_TO_LWORD
%token	<token>		DWORD_TO_WSTRING
%token	<token>		LWORD_TO_BOOL
%token	<token>		LWORD_TO_SINT
%token	<token>		LWORD_TO_INT
%token	<token>		LWORD_TO_DINT
%token	<token>		LWORD_TO_LINT
%token	<token>		LWORD_TO_USINT
%token	<token>		LWORD_TO_UINT
%token	<token>		LWORD_TO_UDINT
%token	<token>		LWORD_TO_ULINT
%token	<token>		LWORD_TO_REAL
%token	<token>		LWORD_TO_LREAL
%token	<token>		LWORD_TO_TIME
%token  <token>     LWORD_TO_DATE
%token	<token>		LWORD_TO_TOD
%token	<token>		LWORD_TO_DT
%token	<token>		LWORD_TO_STRING
%token	<token>		LWORD_TO_BYTE
%token	<token>		LWORD_TO_WORD
%token	<token>		LWORD_TO_DWORD
%token	<token>		LWORD_TO_WSTRING
%token	<token>		WSTRING_TO_BOOL
%token	<token>		WSTRING_TO_SINT
%token	<token>		WSTRING_TO_INT
%token	<token>		WSTRING_TO_DINT
%token	<token>		WSTRING_TO_LINT
%token	<token>		WSTRING_TO_USINT
%token	<token>		WSTRING_TO_UINT
%token	<token>		WSTRING_TO_UDINT
%token	<token>		WSTRING_TO_ULINT
%token	<token>		WSTRING_TO_REAL
%token	<token>		WSTRING_TO_LREAL
%token	<token>		WSTRING_TO_TIME
%token  <token>     WSTRING_TO_DATE
%token	<token>		WSTRING_TO_TOD
%token	<token>		WSTRING_TO_DT
%token	<token>		WSTRING_TO_STRING
%token	<token>		WSTRING_TO_BYTE
%token	<token>		WSTRING_TO_WORD
%token	<token>		WSTRING_TO_DWORD
%token	<token>		WSTRING_TO_LWORD
%token	<token>		TRUNC
%token	<token>		BYTE_BCD_TO_USINT
%token	<token>		BYTE_BCD_TO_UINT
%token	<token>		BYTE_BCD_TO_UDINT
%token	<token>		BYTE_BCD_TO_ULINT
%token	<token>		WORD_BCD_TO_USINT
%token	<token>		WORD_BCD_TO_UINT
%token	<token>		WORD_BCD_TO_UDINT
%token	<token>		WORD_BCD_TO_ULINT
%token	<token>		DWORD_BCD_TO_USINT
%token	<token>		DWORD_BCD_TO_UINT
%token	<token>		DWORD_BCD_TO_UDINT
%token	<token>		DWORD_BCD_TO_ULINT
%token	<token>		LWORD_BCD_TO_USINT
%token	<token>		LWORD_BCD_TO_UINT
%token	<token>		LWORD_BCD_TO_UDINT
%token	<token>		LWORD_BCD_TO_ULINT
%token	<token>		USINT_TO_BCD_BYTE
%token	<token>		USINT_TO_BCD_WORD
%token	<token>		USINT_TO_BCD_DWORD
%token	<token>		USINT_TO_BCD_LWORD
%token	<token>		UINT_TO_BCD_BYTE
%token	<token>		UINT_TO_BCD_WORD
%token	<token>		UINT_TO_BCD_DWORD
%token	<token>		UINT_TO_BCD_LWORD
%token	<token>		UDINT_TO_BCD_BYTE
%token	<token>		UDINT_TO_BCD_WORD
%token	<token>		UDINT_TO_BCD_DWORD
%token	<token>		UDINT_TO_BCD_LWORD
%token	<token>		ULINT_TO_BCD_BYTE
%token	<token>		ULINT_TO_BCD_WORD
%token	<token>		ULINT_TO_BCD_DWORD
%token	<token>		ULINT_TO_BCD_LWORD

/* B.2.5.1.5.2 Numerical Functions */

%token	<token>		ABS
%token	<token>		SQRT
%token	<token>		LN
%token	<token>		LOG
%token	<token>		EXP
%token	<token>		SIN
%token	<token>		COS
%token	<token>		TAN
%token	<token>		ASIN
%token	<token>		ACOS
%token	<token>		ATAN
// %token	<token>		ADD
// %token	<token>		MUL
// %token	<token>		SUB
// %token	<token>		DIV
// %token	<token>		MOD
%token	<token>		EXPT
%token	<token>		MOVE

/* B.2.5.1.5.3 Bit String Functions */

%token	<token>		SHL
%token	<token>		SHR
%token	<token>		ROR
%token	<token>		ROL
// %token	<token>		AND
// %token	<token>		OR
// %token	<token>		XOR
// %token	<token>		NOT

/* B.2.5.1.5.4 Selection and Comparison Functions */

%token	<token>		SEL
%token	<token>		MAX
%token	<token>		MIN
%token	<token>		LIMIT
%token	<token>		MUX
// %token	<token>		GT
// %token	<token>		GE
// %token	<token>		EQ
// %token	<token>		LE
// %token	<token>		LT
// %token	<token>		NE

/* B.2.5.1.5.5 Character String Functions */

%token	<token>		LEN
%token	<token>		LEFT
%token	<token>		RIGHT
%token	<token>		MID
%token	<token>		CONCAT
%token	<token>		INSERT
%token	<token>		DELETE
%token	<token>		REPLACE
%token	<token>		FIND

/* B.2.5.2.3 Standard Function Blocks */

/* B.2.5.2.3.1 Bistable Elements */

%token	<token>		SR
%token	<token>		RS

/* B.2.5.2.3.2 Edge Detection */

%token	<token>		R_TRIG
%token	<token>		F_TRIG

/* B.2.5.2.3.3 Counters */

%token	<token>		CTU
%token	<token>		CTU_DINT
%token	<token>		CTU_LINT
%token	<token>		CTU_UDINT
%token	<token>		CTU_ULINT
%token	<token>		CTD
%token	<token>		CTD_DINT
%token	<token>		CTD_LINT
%token	<token>		CTD_UDINT
%token	<token>		CTD_ULINT
%token	<token>		CTUD
%token	<token>		CTUD_DINT
%token	<token>		CTUD_LINT
%token	<token>		CTUD_UDINT
%token	<token>		CTUD_ULINT

/* B.2.5.2.3.4 Timers */

%token	<token>		TP
%token	<token>		TON
%token	<token>		TOF

%token	RESOURCE
%token	END_RESOURCE
%token	CONFIGURATION
%token	END_CONFIGURATION
%token	STRUCT
%token	END_STRUCT
%token	TYPE
%token	END_TYPE
%token	VAR
%token	END_VAR
%token	VAR_INPUT
%token	VAR_OUTPUT
%token	VAR_IN_OUT
%token	VAR_EXTERNAL
%token	VAR_TEMP
%token	VAR_ACCESS
%token	VAR_GLOBAL
%token	VAR_CONFIG
%token	FUNCTION
%token	END_FUNCTION
%token	FUNCTION_BLOCK
%token	END_FUNCTION_BLOCK
%token	PROGRAM
%token	END_PROGRAM

%token	ARRAY
%token	<token>		ASSIGN
%token	AT
%token	CONSTANT
%token	EOL
%token	INTERVAL
%token	NON_RETAIN
%token	OF
%token	ON
%token	PRIORITY
%token	RANGE
%token	READ_ONLY
%token	READ_WRITE
%token	RETAIN
%token	<token>		SENDTO
%token	SINGLE
%token	TASK
%token	WITH

%type	<token>		access_name
%type	<token>     bit_string_literal
%type	<token>     bit_string_type_name
%type	<token>     boolean_literal
%type	<token>     character_string
%type	<token>     constant
%type	<token>		data_type_declaration
%type	<token>     date_type_name
%type	<token>		derived_function_block_name
%type	<token>		derived_function_name
%type	<token>     derived_type_name
%type	<token>     double_byte_character_string
%type	<list>		edge_declaration
%type	<token>     elementary_type_name
%type	<token>		external_declaration
%type	<list>		external_declaration_list
%type	<list>		external_var_declarations
%type	<token>		fb_name
%type   <token>     field_selector
%type	<list>		function_block_body
%type	<token>		function_block_declaration
%type	<list>		function_block_var_declarations
%type	<list>		function_body
%type	<token>		function_declaration
//%type	<token>		function_name
%type	<list>		function_var_declarations
%type	<list>		function_var_decls
%type	<token>		global_var_name
%type	<token>		il_assign_operator
%type	<token>		il_assign_out_operator
%type	<token>		il_call_operator
%type	<token>		il_expr_operator
%type	<token>		il_expression
%type	<token>		il_fb_call
%type	<token>		il_formal_funct_call
%type	<token>		il_instruction
%type	<token>		il_instruction_list
%type	<token>		il_jump_operation
%type	<token>		il_jump_operator
%type	<token>		il_operand
%type	<list>		il_operand_list
%type	<list>		il_operand_list2
%type	<token>		il_param_assignment
%type	<token>		il_param_instruction
%type	<list>		il_param_instruction_list
%type	<token>		il_param_last_instruction
%type	<list>		il_param_list
%type	<token>		il_param_out_assignment
%type	<token>		il_return_operator
%type	<token>		il_simple_instruction
%type	<token>		il_simple_operation
%type	<token>		il_simple_operator
%type	<list>		input_declaration
%type	<list>		input_declarations
%type	<list>		input_declaration_list
%type	<list>		input_output_declarations
%type	<list>		instruction_list
%type	<token>     integer_literal
%type	<token>     integer_type_name
%type	<list>		io_var_declarations
%type	<token>		label
%type	<list>		language_list
%type	<list>		library
%type	<token>		library_element_declaration
%type	<token>		located_var_decl
%type	<list>		located_var_decl_list
%type	<list>		located_var_declarations
%type   <token>     located_var_spec_init
%type	<token>		location
%type   <token>     multi_element_variable
%type	<list>		non_retentive_var_declarations
%type	<token>     numeric_literal
%type	<token>     numeric_type_name
%type	<list>		other_var_declarations
%type	<list>		output_declarations
%type	<list>		program_access_decl_list
%type	<token>		program_access_decl
%type	<list>		program_access_decls
%type	<token>		program_declaration
%type	<token>		program_type_name
%type	<list>		program_var_declarations
%type	<token>     real_literal
%type	<token>     real_type_name
%type   <token>     record_variable
%type	<list>		retentive_var_declarations
%type	<token>     signed_integer
%type	<token>     signed_integer_type_name
%type	<token>     single_element_type_name
%type	<list>		simple_instr_list
%type	<token>		simple_spec_init
%type	<token>     simple_specification
%type	<token>		simple_type_declaration
%type	<token>     simple_type_name
%type	<token>     single_byte_character_string
%type	<token>		single_element_type_declaration
%type	<list>		start
%type   <token>     structured_variable
%type	<token>		symbolic_variable
%type	<list>		temp_var_decl
%type	<list>		temp_var_decl_list
%type	<list>		temp_var_decls
%type	<token>     time_literal
%type	<token>		type_declaration
%type	<list>		type_declaration_list
%type	<token>     unsigned_integer_type_name
%type	<list>		var_declaration
%type	<list>		var_declaration_list
%type	<list>		var_declarations
%type	<list>		var_init_decl
%type	<list>		var_init_decl_list
%type	<list>		var1_declaration
%type	<list>		var1_init_decl
%type	<list>		var1_list
%type	<list>		var2_init_decl
%type	<list>		var2_init_decl_list
%type	<token>		variable
%type	<token>		variable_name

%token	<token>		binary_integer
%token	<token>     bit_string
%token	<token>     boolean
%token	<token>     date
%token	<token>     date_and_time
%token	<token>		direct_variable
%token	<token>		double_byte_character_representation
%token	<token>     duration
%token	<token>     hex_integer
%token	<token>		identifier
%token	<token>     integer
%token	<token>     octal_integer
%token	<token>     real
%token	<token>		single_byte_character_representation
%token	<token>     time_of_day


%%

start:
	library
;

library:
		{ $$ = &Context->Library; }
	| library library_element_declaration
		{ 
		    $$ = $1; 
		    IEC61131_LIST_INSERT_TAIL( $$, $2 ); 
		}
	| library error END_OF_INPUT
		{
            $$ = $1;
		    YYERROR;
		}
;

eol_list:
	EOL
	| eol_list EOL
;

language_list:
	instruction_list
;


/* B.0 Programming model */

/* library_element_name ::= data_type_name | function_name
        | function_block_type_name | program_type_name
        | resource_type_name | configuration_name */

/* library_element_declaration ::= data_type_declaration
        | function_declaration | function_block_declaration
        | program_declaration | configuration_declaration */

library_element_declaration:
	data_type_declaration
	| function_declaration
	| function_block_declaration
	| program_declaration
;


/* B.1 Common elements */


/* B.1.1 Letters, digits and identifiers */

/* letter  ::=  'A' | 'B' | <...> | 'Z' | 'a' | 'b' | <...> | 'z' */

/* digit ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' */

/* octal_digit ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' */

/* hex_digit ::= digit | 'A'|'B'|'C'|'D'|'E'|'F' */

/* identifier ::= (letter | ('_' (letter | digit))) {['_'] (letter | digit)} */


/* B.1.2 Constants */

/* constant ::= numeric_literal | character_string | time_literal
        | bit_string_literal | boolean_literal */

constant:
	numeric_literal
	| character_string
	| time_literal
	| bit_string_literal
	| boolean_literal
;


/* B.1.2.1 Numeric literals */

/* numeric_literal ::= integer_literal | real_literal */

numeric_literal:
    real_literal
	| integer_literal
;

/* integer_literal ::= [ integer_type_name '#' ]
        ( signed_integer | binary_integer | octal_integer | hex_integer) */

integer_literal:
	integer_type_name '#' signed_integer
		{
			iec61131_castInteger( $3, _iec61131_tokenToType( $1->Id ) );
			
			$$ = $3;
		}
	| integer_type_name '#' binary_integer
		{
			IEC61131_TOKEN *pToken;

			pToken = iec61131_parseInteger( $3, @3.first_line, @3.first_column, 2 );
			iec61131_castInteger( pToken, _iec61131_tokenToType( $1->Id ) );
			
			$$ = pToken; 
		}
	| integer_type_name '#' octal_integer
		{
			IEC61131_TOKEN *pToken;

			pToken = iec61131_parseInteger( $3, @3.first_line, @3.first_column, 8 );
			iec61131_castInteger( pToken, _iec61131_tokenToType( $1->Id ) );
			
			$$ = pToken; 
		}
	| integer_type_name '#' hex_integer
		{
			IEC61131_TOKEN *pToken;

			pToken = iec61131_parseInteger( $3, @3.first_line, @3.first_column, 16 );
			iec61131_castInteger( pToken, _iec61131_tokenToType( $1->Id ) );
			
			$$ = pToken; 
		}
	| signed_integer
	| binary_integer
		{ $$ = iec61131_parseInteger( $1, @1.first_line, @1.first_column, 2 ); }
	| octal_integer
		{ $$ = iec61131_parseInteger( $1, @1.first_line, @1.first_column, 8 ); }
	| hex_integer
		{ $$ = iec61131_parseInteger( $1, @1.first_line, @1.first_column, 16 ); }

    | integer_type_name signed_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between integer type name and value" );
        }
    | integer_type_name binary_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between integer type name and value" );
        }
    | integer_type_name octal_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between integer type name and value" );
        }
    | integer_type_name hex_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between integer type name and value" );
        }
    | integer_type_name '#' error
        {
            IEC61131_ERROR( @3.first_line, @3.first_column, "missing integer value" );
        }
;

/* signed_integer ::= ['+' |'-'] integer */

signed_integer:
	'+' integer
		{ $$ = iec61131_parseInteger( $2, @2.first_line, @2.first_column, 10 ); }
	| '-' integer
		{
			IEC61131_TOKEN_LITERAL *pLiteral;
						
			pLiteral = ( IEC61131_TOKEN_LITERAL * ) iec61131_parseInteger( $2, @2.first_line, @2.first_column, 10 );
			pLiteral->Value.Value.S64 = -( pLiteral->Value.Value.S64 );
			
			$$ = ( IEC61131_TOKEN * ) pLiteral; 
		}
	| integer
		{ $$ = iec61131_parseInteger( $1, ( @1.first_line ), ( @1.first_column ), 10 ); }
;

/* integer ::= digit {['_'] digit} */

/* binary_integer ::= '2#' bit {['_'] bit} */

/* bit ::= '1' | '0' */

/* octal_integer ::= '8#' octal_digit {['_'] octal_digit} */

/* hex_integer ::= '16#' hex_digit {['_'] hex_digit} */

/* real_literal ::= [ real_type_name '#' ]
        signed_integer  '.' integer [exponent] */

real_literal:
    real_type_name '#' real
        {
			IEC61131_TOKEN *pToken;

			pToken = iec61131_parseReal( $3, @3.first_line, @3.first_column );
			iec61131_castReal( pToken, _iec61131_tokenToType( $1->Id ) );
			
			$$ = pToken; 
        }
	| real
        { $$ = iec61131_parseReal( $1, @1.first_line, @1.first_column ); }

    | real_type_name real
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between real type name and value" );
        }
    | real_type_name '#' error
        {
            IEC61131_ERROR( @3.first_line, @3.first_column, "missing real value" );
        }
;

/* exponent ::= ('E' | 'e') ['+'|'-'] integer */

/* bit_string_literal ::=
        [ ('BYTE' | 'WORD' | 'DWORD' | 'LWORD') '#' ]
        ( unsigned_integer | binary_integer | octal_integer | hex_integer) */

bit_string_literal:
    bit_string_type_name '#' integer
        {
            IEC61131_TOKEN *pToken;

            pToken = iec61131_parseBitstring( $3, @3.first_line, @3.first_column, 10 );
            iec61131_castBitstring( pToken, _iec61131_tokenToType( $1->Id ) );

            $$ = pToken;
        }
    | bit_string_type_name '#' binary_integer
        {
            IEC61131_TOKEN *pToken;

            pToken = iec61131_parseBitstring( $3, @3.first_line, @3.first_column, 2 );
            iec61131_castBitstring( pToken, _iec61131_tokenToType( $1->Id ) );

            $$ = pToken;
        }
    | bit_string_type_name '#' octal_integer
        {
            IEC61131_TOKEN *pToken;

            pToken = iec61131_parseBitstring( $3, @3.first_line, @3.first_column, 8 );
            iec61131_castBitstring( pToken, _iec61131_tokenToType( $1->Id ) );

            $$ = pToken;
        }
    | bit_string_type_name '#' hex_integer
        {
            IEC61131_TOKEN *pToken;

            pToken = iec61131_parseBitstring( $3, @3.first_line, @3.first_column, 16 );
            iec61131_castBitstring( pToken, _iec61131_tokenToType( $1->Id ) );

            $$ = pToken;
        }

    | bit_string_type_name integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between bit string type name and value" );
        }
    | bit_string_type_name binary_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between bit string type name and value" );
        }
    | bit_string_type_name octal_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between bit string type name and value" );
        }
    | bit_string_type_name hex_integer
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing '#' between bit string type name and value" );
        }
    | bit_string_type_name '#' error
        {
            IEC61131_ERROR( @3.first_line, @3.first_column, "missing bit string value" );
        }
;

/* boolean_literal ::=
        ( [ 'BOOL#' ] ( '1' | '0' )  )| 'TRUE' | 'FALSE' */

boolean_literal:
	boolean
        { $$ = iec61131_parseBoolean( $1, ( @1.first_line ), ( @1.first_column ) ); }
;


/* B.1.2.2 Character strings */

/* character_string ::=
        single_byte_character_string | double_byte_character_string */

character_string:
	single_byte_character_string
	| double_byte_character_string
;

/* single_byte_character_string ::=
        "'" {single_byte_character_representation} "'" */

single_byte_character_string:
	"'" single_byte_character_representation "'"
		{ $$ = $2; }
;

/* double_byte_character_string ::=
        '"' {double_byte_character_representation} '"' */

double_byte_character_string:
	"'" double_byte_character_representation "'"
		{ $$ = $2; }
;

/* single_byte_character_representation ::= common_character_representation
        | "$'" | '"' | '$' hex_digit hex_digit */

/* double_byte_character_representation ::= common_character_representation
        | '$"' | "'"| '$' hex_digit hex_digit hex_digit hex_digit */

/* common_character_representation ::=
        <any printable character except '$', '"' or "'">
        | '$$' | '$L' | '$N' | '$P' | '$R' | '$T'
        | '$l' | '$n' | '$p' | '$r' | '$t' */


/* B.1.2.3 Time literals */

/* time_literal ::= duration | time_of_day | date | date_and_time */

time_literal:
	duration
        {
            $$ = iec61131_parseDuration( $1, @1.first_line, @1.first_column );
        }
	| time_of_day
	| date
	| date_and_time
;


/* B.1.2.3.1 Duration */

/* duration ::= ('T' | 'TIME') '#' ['-'] interval */

/* interval ::= days | hours | minutes | seconds | milliseconds */

/* days ::= fixed_point ('d') | integer ('d') ['_'] hours */

/* fixed_point ::= integer [ '.' integer] */

/* hours ::= fixed_point ('h') | integer ('h') ['_'] minutes */

/* minutes ::= fixed_point ('m')  | integer ('m') ['_'] seconds */

/* seconds ::= fixed_point ('s') | integer ('s') ['_'] milliseconds */

/* milliseconds ::= fixed_point ('ms') */


/* B.1.2.3.2 Time of day and date */

/* time_of_day ::= ('TIME_OF_DAY' | 'TOD')  '#' daytime */

/* daytime ::= day_hour ':' day_minute ':' day_second */

/* day_hour ::= integer */

/* day_minute ::= integer */

/* day_second ::= fixed_point */

/* date ::= ('DATE' | 'D') '#' date_literal */

/* date_literal ::= year '-' month '-' day */

/* year ::= integer */

/* month ::= integer */

/* day ::= integer */

/* date_and_time ::= ('DATE_AND_TIME' | 'DT') '#' date_literal '-' daytime */


/* B.1.3 Data types */

/* data_type_name ::= non_generic_type_name | generic_type_name */

/* non_generic_type_name ::=  elementary_type_name | derived_type_name */

non_generic_type_name:
	elementary_type_name
	| derived_type_name
;


/* B.1.3.1 Elementary data types */

/* elementary_type_name ::= numeric_type_name | date_type_name
        | bit_string_type_name | 'STRING' | 'WSTRING' | 'TIME' */

elementary_type_name:
	numeric_type_name
	| date_type_name
	| bit_string_type_name
	| STRING
	| WSTRING
	| TIME
;

/* numeric_type_name ::= integer_type_name | real_type_name */

numeric_type_name:
	integer_type_name
	| real_type_name
;

/* integer_type_name ::= signed_integer_type_name | unsigned_integer_type_name */

integer_type_name:
	signed_integer_type_name
	| unsigned_integer_type_name
;

/* signed_integer_type_name ::= 'SINT' | 'INT' | 'DINT' | 'LINT' */

signed_integer_type_name:
	SINT
	| INT
	| DINT
	| LINT
;

/* unsigned_integer_type_name ::= 'USINT' | 'UINT' | 'UDINT'  | 'ULINT' */

unsigned_integer_type_name:
	USINT
	| UINT
	| UDINT
	| ULINT
;

/* real_type_name ::= 'REAL' | 'LREAL' */

real_type_name:
	REAL
	| LREAL
;

/* date_type_name ::= 'DATE' | 'TIME_OF_DAY' | 'TOD'  | 'DATE_AND_TIME' | 'DT' */

date_type_name:
	DATE
	| TIME_OF_DAY
	| TOD
	| DATE_AND_TIME
	| DT
;

/* bit_string_type_name ::= 'BOOL' | 'BYTE' | 'WORD' | 'DWORD' | 'LWORD' */

bit_string_type_name:
	BOOL
	| BYTE
	| WORD
	| DWORD
	| LWORD
;


/* B.1.3.2 Generic data types */

/* generic_type_name ::= 'ANY' | 'ANY_DERIVED' | 'ANY_ELEMENTARY'
        | 'ANY_MAGNITUDE' | 'ANY_NUM' | 'ANY_REAL' | 'ANY_INT' | 'ANY_BIT'
        | 'ANY_STRING' | 'ANY_DATE' */


/* B.1.3.3 Derived data types */

/* derived_type_name ::= single_element_type_name | array_type_name
        | structure_type_name | string_type_name */

derived_type_name:
	single_element_type_name
//	| array_type_name
//	| structure_type_name
//	| string_type_name
;

/* single_element_type_name ::= simple_type_name | subrange_type_name
        | enumerated_type_name */

single_element_type_name:
	simple_type_name
//	| subrange_type_name
//	| enumerated_type_name
;

/* simple_type_name ::= identifier */

simple_type_name:
	identifier
;

/* subrange_type_name ::= identifier */

//subrange_type_name:
//	identifier
//;

/* enumerated_type_name ::= identifier */

//enumerated_type_name:
//	identifier
//;

/* array_type_name ::= identifier */

//array_type_name:
//	identifier
//;

/* structure_type_name ::= identifier */

//structure_type_name:
//	identifier
//;

/* data_type_declaration ::=
        'TYPE' type_declaration ';'
          {type_declaration ';'}
        'END_TYPE' */

data_type_declaration:
	TYPE type_declaration_list END_TYPE
		{
			IEC61131_TOKEN_COLLECTION *pDeclaration;


			pDeclaration = IEC61131_NEW_COLLECTION( TYPE );
			pDeclaration->Members = $2;
			
			$$ = ( IEC61131_TOKEN * ) pDeclaration; 
		}
;

type_declaration_list:
	type_declaration ';'
		{
			$$ = IEC61131_NEW_LIST();
			IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| type_declaration ';' type_declaration_list
		{
			IEC61131_LIST_INSERT_TAIL( $3, $1 );
			$$ = $3;
		}
;

/* type_declaration ::= single_element_type_declaration | array_type_declaration
        | structure_type_declaration | string_type_declaration */

type_declaration:
	single_element_type_declaration
		{ $$ = $1; }
//	| array_type_declaration
//	| structure_type_declaration
//	| string_type_declaration
;

/* single_element_type_declaration ::= simple_type_declaration
        | subrange_type_declaration | enumerated_type_declaration */

single_element_type_declaration:
	simple_type_declaration
		{ $$ = $1; }
//	| subrange_type_declaration
//	| enumerated_type_declaration
;

/* simple_type_declaration ::= simple_type_name ':' simple_spec_init */

simple_type_declaration:
	simple_type_name ':' simple_spec_init
		{
			IEC61131_TOKEN_RAW *pRaw;
			IEC61131_TOKEN_TYPE *pType;


			pRaw = ( IEC61131_TOKEN_RAW * ) $1;
			//	Replace TYPE with unique identifier for each type declaration?
			pType = IEC61131_NEW_TYPE( TYPE, pRaw->Data );
			BASE_DESTROY( pRaw );

			$$ = ( IEC61131_TOKEN * ) pType;
		}
;

/* simple_spec_init := simple_specification [':=' constant] */

simple_spec_init:
	simple_specification
        {
            IEC61131_TOKEN *pType;
            IEC61131_TOKEN_RAW *pRaw;
            IEC61131_TOKEN_LITERAL *pToken;


            pType = ( IEC61131_TOKEN * ) $1;
            pRaw = ( IEC61131_TOKEN_RAW * ) pType;
            pToken = IEC61131_NEW_LITERAL( pType->Id, pRaw->Data );

            $$ = ( IEC61131_TOKEN * ) pToken;
        }
	| simple_specification ASSIGN constant
        {
            IEC61131_TOKEN *pType, *pValue;
            IEC61131_TOKEN_LITERAL *pLiteral;


            /*
                This code will create a new IEC61131_TOKEN_LITERAL object for the storage 
                of constant value and specification type.  This action is not as simple or 
                direct as this would otherwise seem as there is additionally a requirement 
                to properly cast the constant literal value (constant) to the specified 
                type (simple_specification).

                TODO: Only works for integers!
            */

            pType = ( IEC61131_TOKEN * ) $1;

            pValue = ( IEC61131_TOKEN * ) $3;
            pValue->Id = pType->Id;
            iec61131_castInteger( pValue, _iec61131_tokenToType( pType->Id ) );

            pLiteral = ( IEC61131_TOKEN_LITERAL * ) pValue;
            pLiteral->Assigned = BOOL_TRUE;

            $$ = ( IEC61131_TOKEN * ) pLiteral;
        }
;

/* simple_specification ::= elementary_type_name | simple_type_name */

simple_specification:
	elementary_type_name
	| simple_type_name
;

/* subrange_type_declaration ::= subrange_type_name ':' subrange_spec_init */

//subrange_type_declaration:
//	subrange_type_name ':' subrange_spec_init
//;

/* subrange_spec_init ::= subrange_specification [':=' signed_integer] */

//subrange_spec_init:
//	subrange_specification
//	| subrange_specification ASSIGN signed_integer
//;

/* subrange_specification ::= integer_type_name '(' subrange')' | subrange_type_name */

//subrange_specification:
//	integer_type_name '(' subrange ')'
//	| subrange_type_name
//;

/* subrange ::= signed_integer '..' signed_integer */

//subrange:
//	signed_integer RANGE signed_integer
//;

/* enumerated_type_declaration ::= enumerated_type_name ':' enumerated_spec_init */

//enumerated_type_declaration:
//	enumerated_type_name ':' enumerated_spec_init
//;

/* enumerated_spec_init ::= enumerated_specification [':=' enumerated_value] */

//enumerated_spec_init:
//	enumerated_specification
//	| enumerated_specification ASSIGN enumerated_value
//;

/* enumerated_specification ::=
        ( '(' enumerated_value {',' enumerated_value} ')' ) | enumerated_type_name */

//enumerated_specification:
//	'(' enumerated_value_list ')'
//	| enumerated_type_name
//;

//enumerated_value_list:
//	enumerated_value
//	| enumerated_value ',' enumerated_value
//	| enumerated_value_list ',' enumerated_value
//;

/* enumerated_value ::= [enumerated_type_name '#'] identifier */

//enumerated_value:
//	enumerated_type_name '#' identifier
//	| identifier
//;

/* array_type_declaration ::= array_type_name ':' array_spec_init */

//array_type_declaration:
//	array_type_name ':' array_spec_init
//;

/* array_spec_init ::= array_specification [':=' array_initialization] */

//array_spec_init:
//	array_specification
//	| array_specification ASSIGN array_initialization
//;

/* array_specification ::= array_type_name
        | 'ARRAY' '[' subrange {',' subrange} ']' 'OF' non_generic_type_name */

//array_specification:
//	array_type_name
//	| ARRAY '[' subrange_list ']' OF non_generic_type_name
//;

//subrange_list:
//	subrange
//	| subrange_list ',' subrange
//;

/* array_initialization ::=
        '[' array_initial_elements {',' array_initial_elements} ']' */

//array_initialization:
//	'[' array_initial_elements_list ']'
//;

//array_initial_elements_list:
//	array_initial_elements
//	| array_initial_elements ',' array_initial_elements
//	| array_initial_elements_list ',' array_initial_elements
//;

/* array_initial_elements ::=
        array_initial_element | integer '(' [array_initial_element] ')' */

//array_initial_elements:
//	array_initial_element
//	| integer '(' array_initial_element ')'
//	| integer '(' ')'
//;

/* array_initial_element ::= constant | enumerated_value
        | structure_initialization | array_initialization */

//array_initial_element:
//	constant
//	| enumerated_value
//	| structure_initialization
//	| array_initialization
//;

/* structure_type_declaration ::=
        structure_type_name ':' structure_specification */

//structure_type_declaration:
//	structure_type_name ':' structure_specification
//;

/* structure_specification ::= structure_declaration | initialized_structure */

//structure_specification:
//	structure_declaration
//	| initialized_structure
//;

/* initialized_structure ::= structure_type_name [':=' structure_initialization] */

//initialized_structure:
//	structure_type_name
//	| structure_type_name ASSIGN structure_initialization
//;

/* structure_declaration ::=
        'STRUCT' structure_element_declaration ';'
          {structure_element_declaration ';'}
        'END_STRUCT' */

//structure_declaration:
//	STRUCT structure_element_declaration_list END_STRUCT
//;

//structure_element_declaration_list:
//	structure_element_declaration ';'
//	| structure_element_declaration_list structure_element_declaration ';'
//;

/* structure_element_declaration ::= structure_element_name ':'
        (simple_spec_init | subrange_spec_init  | enumerated_spec_init
        | array_spec_init | initialized_structure) */

//structure_element_declaration:
//	structure_element_name ':' simple_spec_init
//	| structure_element_name ':' subrange_spec_init
//	| structure_element_name ':' enumerated_spec_init
//	| structure_element_name ':' array_spec_init
//	| structure_element_name ':' initialized_structure
//;

/* structure_element_name ::= identifier */

//structure_element_name:
//	identifier
//;

/* structure_initialization ::=
        '(' structure_element_initialization
        {','  structure_element_initialization} ')' */

//structure_initialization:
//	'(' structure_element_initialization_list ')'
//;

//structure_element_initialization_list:
//	structure_element_initialization
//	| structure_element_initialization_list ',' structure_element_initialization
//;

/* structure_element_initialization ::=
        structure_element_name ':=' (constant | enumerated_value
        | array_initialization | structure_initialization) */

//structure_element_initialization:
//	structure_element_name ASSIGN constant
//	| structure_element_name ASSIGN enumerated_value
//	| structure_element_name ASSIGN array_initialization
//	| structure_element_name ASSIGN structure_initialization
//;

/* string_type_name ::= identifier */

//string_type_name:
//	identifier
//;

/* string_type_declaration ::= string_type_name ':'
        ('STRING'|'WSTRING') ['[' integer ']'] [':=' character_string] */

//string_type_declaration:
//	string_type_name ':' elementary_string_type_name
//	| string_type_name ':' elementary_string_type_name '[' integer ']'
//	| string_type_name ':' elementary_string_type_name '[' integer ']' ASSIGN character_string
//	| string_type_name ':' elementary_string_type_name ASSIGN character_string
//;

//elementary_string_type_name:
//	STRING
//	| WSTRING
//;


/* B.1.4 Variables */

/* variable ::= direct_variable | symbolic_variable */

variable:
	direct_variable
	| symbolic_variable
;

/* symbolic_variable ::= variable_name | multi_element_variable */

symbolic_variable:
	variable_name
	| multi_element_variable
;

/* variable_name ::= identifier */

variable_name:
	identifier
		{
		    IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column );
		}
;


/* B.1.4.1 Directly represented variables */

/* direct_variable ::= '%' location_prefix size_prefix integer {'.' integer} */

/* location_prefix ::= 'I' | 'Q' | 'M' */

/* size_prefix ::= NIL | 'X' | 'B' | 'W' | 'D' | 'L' */


/* B.1.4.2 Multi-element variables */

/* multi_element_variable ::= array_variable | structured_variable */

multi_element_variable:
//	array_variable
	structured_variable
;

/* array_variable ::= subscripted_variable subscript_list */

//array_variable:
//	subscripted_variable subscript_list
//;

/* subscripted_variable ::= symbolic_variable */

//subscripted_variable:
//	symbolic_variable
//;

/* subscript_list ::= '[' subscript {',' subscript} ']' */

//subscript_list:
//	'[' subscript_comma_list ']'
//;

//subscript_comma_list:
//	subscript
//	| subscript_comma_list ',' subscript
//;

/* subscript ::= expression */

//subscript:
//	expression
//;

/* structured_variable ::= record_variable '.' field_selector */

structured_variable:
	record_variable '.' field_selector
        {
		    IEC61131_TOKEN_RAW *pField, *pRecord, *pVariable;
            char szName[LINE_MAX];

		    pRecord = ( IEC61131_TOKEN_RAW * ) $1;
            pField = ( IEC61131_TOKEN_RAW * ) $3;
            ( void ) snprintf( szName, sizeof( szName ), "%s.%s", pRecord->Data, pField->Data );
		    pVariable = IEC61131_NEW_RAW( identifier, szName, strlen( szName ) );
		    IEC61131_TOKEN_LOCATION( pVariable, @1.first_line, @1.first_column );
		    BASE_DESTROY( pRecord );
		    BASE_DESTROY( pField );

            $$ = ( IEC61131_TOKEN * ) pVariable;
        }
;

/* record_variable ::= symbolic_variable */

record_variable:
	symbolic_variable
        { $$ = $1; }
;

/* field_selector ::= identifier */

field_selector:
	identifier
        { $$ = $1; }
;


/* B.1.4.3 Declaration and initialization */

/* input_declarations ::=
        'VAR_INPUT' ['RETAIN' | 'NON_RETAIN']
          input_declaration ';'
          {input_declaration ';'}
        'END_VAR' */

input_declarations:
    VAR_INPUT input_declaration_list END_VAR
        { 
            IEC61131_LIST_FOREACH( $2, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_INPUT );
            $$ = $2; 
        }
	| VAR_INPUT RETAIN input_declaration_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= ( FLAGS_INPUT | FLAGS_RETAIN ) );
            $$ = $3;
		}
	| VAR_INPUT NON_RETAIN input_declaration_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= ( FLAGS_INPUT | FLAGS_NON_RETAIN ) );
            $$ = $3;
		}
;

input_declaration_list:
	input_declaration ';'
		{ $$ = $1; }
	| input_declaration_list input_declaration ';'
		{
			IEC61131_LIST_INSERT_TAIL( $1, $2 );
			$$ = $1;
		}
;

/* input_declaration ::= var_init_decl | edge_declaration */

input_declaration:
	var_init_decl
		{ $$ = $1; }
	| edge_declaration
		{ $$ = $1; }
;

/* edge_declaration ::= var1_list ':' 'BOOL' ('R_EDGE' | 'F_EDGE') */

edge_declaration:
	var1_list ':' BOOL F_EDGE
		{ $$ = $1; }
	| var1_list ':' BOOL R_EDGE
		{ $$ = $1; }
;

/* var_init_decl ::= var1_init_decl | array_var_init_decl
        | structured_var_init_decl | fb_name_decl | string_var_declaration */

var_init_decl:
	var1_init_decl
//	| array_var_init_decl
//	| structured_var_init_decl
//	| fb_name_decl
//	| string_var_declaration
;

/* var1_init_decl ::=  var1_list ':'
        (simple_spec_init | subrange_spec_init | enumerated_spec_init) */

var1_init_decl:
	var1_list ':' simple_spec_init
		{ 
            IEC61131_TOKEN_LITERAL *pType;

            pType = ( IEC61131_TOKEN_LITERAL * ) $3;
		    IEC61131_LIST_FOREACH( $1, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Type = pType );
		}
//	| var1_list ':' subrange_spec_init
//	| var1_list ':' enumerated_spec_init
;

/* var1_list ::= variable_name {',' variable_name} */

var1_list:
	variable_name
		{
		    IEC61131_TOKEN_RAW *pRaw;
		    IEC61131_TOKEN_VARIABLE *pVar;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $1;
		    pVar = IEC61131_NEW_VAR( identifier, pRaw->Data );
		    IEC61131_TOKEN_LOCATION( pVar, @1.first_line, @1.first_column );
		    BASE_DESTROY( pRaw );

		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, pVar );
		}
	| var1_list ',' variable_name
		{ 
		    IEC61131_TOKEN_RAW *pRaw;
		    IEC61131_TOKEN_VARIABLE *pVar;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $3;
		    pVar = IEC61131_NEW_VAR( identifier, pRaw->Data );
		    IEC61131_TOKEN_LOCATION( pVar, @3.first_line, @3.first_column );
		    BASE_DESTROY( pRaw );

		    IEC61131_LIST_INSERT_TAIL( $1, pVar ); 
		}
;

/* array_var_init_decl ::= var1_list ':' array_spec_init */

//array_var_init_decl:
//	var1_list ':' array_spec_init
//;

/* structured_var_init_decl ::= var1_list ':' initialized_structure */

//structured_var_init_decl:
//	var1_list ':' initialized_structure
//;

/* fb_name_decl ::= fb_name_list ':' function_block_type_name
        [ ':=' structure_initialization ] */

//fb_name_decl:
//	fb_name_list ':' function_block_type_name
//    | fb_name_list ':' function_block_type_name ASSIGN structure_initialization
//;

/* fb_name_list ::= fb_name {',' fb_name} */

//fb_name_list:
//	fb_name_list ',' fb_name
//	| fb_name ',' fb_name
//	| fb_name
//;

/* fb_name ::= identifier */

fb_name:
	identifier
;

/* output_declarations ::=
        'VAR_OUTPUT' ['RETAIN' | 'NON_RETAIN']
          var_init_decl ';'
          {var_init_decl ';'}
        'END_VAR' */

output_declarations:
    VAR_OUTPUT var_init_decl_list END_VAR
        {
            IEC61131_LIST_FOREACH( $2, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_OUTPUT );
            $$ = $2;
        }
	| VAR_OUTPUT RETAIN var_init_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= ( FLAGS_OUTPUT | FLAGS_RETAIN ) );
		    $$ = $3;
		}
	| VAR_OUTPUT NON_RETAIN var_init_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= ( FLAGS_OUTPUT | FLAGS_NON_RETAIN ) );
		    $$ = $3;
		}
;

var_init_decl_list:
	var_init_decl ';'
		{
		    $$ = $1;
		}
	| var_init_decl_list var_init_decl ';'
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
;

/* input_output_declarations ::=
        'VAR_IN_OUT'
          var_declaration ';'
          {var_declaration ';'}
        'END_VAR' */

input_output_declarations:
	VAR_IN_OUT var_declaration_list END_VAR
		{ 
            IEC61131_LIST_FOREACH( $2, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= ( FLAGS_INPUT | FLAGS_OUTPUT ) );
            $$ = $2; 
        }
;

var_declaration_list:
	var_declaration ';'
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| var_declaration_list var_declaration ';'
		{ IEC61131_LIST_INSERT_TAIL( $1, $2 ); }
;

/* var_declaration ::=  temp_var_decl | fb_name_decl */

var_declaration:
	temp_var_decl
//	| fb_name_decl
;

/* temp_var_decl ::= var1_declaration | array_var_declaration
        | structured_var_declaration | string_var_declaration */

temp_var_decl:
	var1_declaration
//	| array_var_declaration
//	| structured_var_declaration
//	| string_var_declaration
;

/* var1_declaration ::=  var1_list ':' (simple_specification
        | subrange_specification | enumerated_specification) */

var1_declaration:
	var1_list ':' simple_specification
		{
            IEC61131_TOKEN_LITERAL *pType;

            pType = ( IEC61131_TOKEN_LITERAL * ) $3;
		    IEC61131_LIST_FOREACH( $1, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Type = pType );
		}
//	| var1_list ':' subrange_specification
//	| var1_list ':' enumerated_specification
//;

/* array_var_declaration ::= var1_list ':' array_specification */

//array_var_declaration:
//	var1_list ':' array_specification
//;

/* structured_var_declaration ::= var1_list ':' structure_type_name */

//structured_var_declaration:
//	var1_list ':' structure_type_name
//;

/* var_declarations ::=
        'VAR' ['CONSTANT']
          var_init_decl ';'
          {var_init_decl ';'}
        'END_VAR' */

var_declarations:
	VAR CONSTANT var_init_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_LITERAL );
		    $$ = $3;
		}
	| VAR var_init_decl_list END_VAR
		{ $$ = $2; }
;

/* retentive_var_declarations ::=
        'VAR' 'RETAIN'
          var_init_decl ';'
          {var_init_decl ';'}
        'END_VAR' */

retentive_var_declarations:
	VAR RETAIN var_init_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_RETAIN );
		    $$ = $3;
		}
;

/* located_var_declarations ::=
        'VAR' ['CONSTANT' | 'RETAIN' | 'NON_RETAIN']
          located_var_decl ';'
          {located_var_decl ';'}
        'END_VAR' */

located_var_declarations:
    VAR located_var_decl_list END_VAR
        { $$ = $2; }
	| VAR CONSTANT located_var_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_LITERAL );  
		    $$ = $3;
		}
	| VAR NON_RETAIN located_var_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_NON_RETAIN ); 
		    $$ = $3;
		}
	| VAR RETAIN located_var_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_RETAIN ); 
		    $$ = $3;
		}
;

located_var_decl_list:
	located_var_decl ';'
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| located_var_decl_list located_var_decl ';'
		{ IEC61131_LIST_INSERT_TAIL( $1, $2 ); }
;

/* located_var_decl ::= [variable_name] location ':' located_var_spec_init */

located_var_decl:
	variable_name location ':' located_var_spec_init
		{
            IEC61131_TOKEN_LITERAL *pLocation, *pType;
		    IEC61131_TOKEN_RAW *pRaw;
		    IEC61131_TOKEN_VARIABLE *pVar;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $1;
            pLocation = ( IEC61131_TOKEN_LITERAL * ) $2;
            pType = ( IEC61131_TOKEN_LITERAL * ) $4;
		    pVar = IEC61131_NEW_VAR( identifier, pRaw->Data );
            pVar->Location = pLocation;
            pVar->Type = pType;
		    BASE_DESTROY( pRaw );

		    $$ = ( IEC61131_TOKEN * ) pVar; 
		}
//	| location ':' located_var_spec_init
;

/* external_var_declarations :=
        'VAR_EXTERNAL' ['CONSTANT']
          external_declaration ';'
          {external_declaration ';'}
        'END_VAR' */

external_var_declarations:
	VAR_EXTERNAL CONSTANT external_declaration_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_LITERAL ); 
		    $$ = $3;
		}
	| VAR_EXTERNAL external_declaration_list END_VAR
		{ $$ = $2; }
;

external_declaration_list:
	external_declaration ';'
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| external_declaration_list external_declaration ';'
		{ IEC61131_LIST_INSERT_TAIL( $1, $2 ); }
;

/* external_declaration ::= global_var_name ':'
        (simple_specification | subrange_specification
          | enumerated_specification | array_specification | structure_type_name
          | function_block_type_name) */

external_declaration:
	global_var_name ':' simple_specification
		{
            IEC61131_TOKEN_LITERAL *pType;
		    IEC61131_TOKEN_VARIABLE *pVariable;

		    pVariable = ( IEC61131_TOKEN_VARIABLE * ) $1;
            pType = ( IEC61131_TOKEN_LITERAL * ) $3;
		    pVariable->Type = pType;
		}
//	| global_var_name ':' subrange_specification
//	| global_var_name ':' enumerated_specification
//	| global_var_name ':' array_specification
//	| global_var_name ':' structure_type_name
//	| global_var_name ':' function_block_type_name
;

/* global_var_name ::= identifier */

global_var_name:
	identifier
;

/* global_var_declarations :=
        'VAR_GLOBAL' ['CONSTANT' | 'RETAIN']
          global_var_decl ';'
          {global_var_decl ';'}
        'END_VAR' */

//global_var_declarations:
//	VAR_GLOBAL CONSTANT global_var_decl_list END_VAR
//	| VAR_GLOBAL RETAIN global_var_decl_list END_VAR
//;

//global_var_decl_list:
//	global_var_decl ';'
//	| global_var_decl_list global_var_decl ';'
//;

/* global_var_decl ::= global_var_spec ':'
        [ located_var_spec_init | function_block_type_name ] */

//global_var_decl:
//	global_var_spec ':'
//	| global_var_spec ':' located_var_spec_init
//	| global_var_spec ':' function_block_type_name
//;

/* global_var_spec ::= global_var_list | [global_var_name] location */

//global_var_spec:
//	global_var_list
//	| global_var_name location
//	| location
//;

/* located_var_spec_init ::= simple_spec_init | subrange_spec_init
        | enumerated_spec_init | array_spec_init | initialized_structure
        | single_byte_string_spec | double_byte_string_spec */

located_var_spec_init:
	simple_spec_init
//	| subrange_spec_init
//	| enumerated_spec_init
//	| array_spec_init
//	| initialized_structure
//	| single_byte_string_spec
//	| double_byte_string_spec
;

/* location ::= 'AT' direct_variable */

location:
	AT direct_variable
		{ $$ = $2; }
;

/* global_var_list ::= global_var_name {',' global_var_name} */

//global_var_list:
//	global_var_name
//	| global_var_list ',' global_var_name
//;

/* string_var_declaration ::= single_byte_string_var_declaration
        | double_byte_string_var_declaration */

//string_var_declaration:
//	single_byte_string_var_declaration
//	| double_byte_string_var_declaration
//;

/* single_byte_string_var_declaration ::=
        var1_list ':' single_byte_string_spec */

//single_byte_string_var_declaration:
//	var1_list ':' single_byte_string_spec
//;

/* single_byte_string_spec ::=
        'STRING' ['[' integer ']'] [':=' single_byte_character_string] */

//single_byte_string_spec:
//	STRING
//	| STRING '[' integer ']'
//	| STRING '[' integer ']' ASSIGN single_byte_character_string
//	| STRING ASSIGN single_byte_character_string
//;

/* double_byte_string_var_declaration ::=
        var1_list ':' double_byte_string_spec */

//double_byte_string_var_declaration:
//	var1_list ':' double_byte_string_spec
//;

/* double_byte_string_spec ::=
        'WSTRING' ['[' integer ']'] [':=' double_byte_character_string] */

//double_byte_string_spec:
//	WSTRING
//	| WSTRING '[' integer ']'
//	| WSTRING '[' integer ']' ASSIGN double_byte_character_string
//	| WSTRING ASSIGN double_byte_character_string
//;

/* incompl_located_var_declarations ::=
        'VAR' ['RETAIN'|'NON_RETAIN']
          incompl_located_var_decl ';'
          {incompl_located_var_decl ';'}
        'END_VAR' */

//incompl_located_var_declarations:
//	VAR NON_RETAIN incompl_located_var_decl_list END_VAR
//	| VAR RETAIN incompl_located_var_decl_list END_VAR
//;

//incompl_located_var_decl_list:
//	incompl_located_var_decl ';'
//	| incompl_located_var_decl_list incompl_located_var_decl ';'
//;

/* incompl_located_var_decl ::= variable_name incompl_location ':' var_spec */

//incompl_located_var_decl:
//	variable_name incompl_location ':' var_spec
//;

/* incompl_location ::= 'AT' '%' ('I' | 'Q' | 'M') '*' */

//incompl_location:
//	AT '%' 'I' '*'
//	| AT '%' 'Q' '*'
//	| AT '%' 'M' '*'
//;

/* var_spec ::= simple_specification
        | subrange_specification | enumerated_specification
        | array_specification | structure_type_name | 'STRING' ['[' integer ']']
        | 'WSTRING' ['[' integer ']'] */

//var_spec:
//	simple_specification
//	| subrange_specification
//	| enumerated_specification
//	| array_specification
//	| structure_type_name
//	| STRING
//	| STRING '[' integer ']'
//	| WSTRING
//	| WSTRING '[' integer ']'
//;


/* B.1.5 Program organization units */


/* B.1.5.1 Functions */

/* function_name ::= standard_function_name | derived_function_name */

/* standard_function_name ::= <as defined in 2.5.1.5> */

/* derived_function_name ::= identifier */

derived_function_name:
	identifier
;

/* function_declaration ::=
        'FUNCTION' derived_function_name ':'
            (elementary_type_name | derived_type_name)
          { io_var_declarations | function_var_decls }
          function_body
        'END_FUNCTION' */

function_declaration:
	FUNCTION derived_function_name ':' elementary_type_name function_var_declarations function_body END_FUNCTION
		{
            IEC61131_LIST_ELEMENT *pElement;
            IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_POU *pPOU;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $2;
		    pPOU = IEC61131_NEW_POU( FUNCTION, pRaw->Data );
            pPOU->Type = $4;
		    pPOU->Body = $6;
		    pPOU->Variables = $5;
		    BASE_DESTROY( pRaw );

            pElement = IEC61131_LIST_TAIL( pPOU->Body );
            for( ;; )
            {
                if( pElement != NULL )
                {
                    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
                    if( ( pInstr != NULL ) &&
                            ( ( ( IEC61131_TOKEN * ) pInstr )->Id == RET ) )
                    {
                        break;
                    }
                }

                IEC61131_LIST_INSERT_TAIL( pPOU->Body, IEC61131_NEW_INSTRUCTION( RET ) );
                break;
            }
            _iec61131_setTokenScope( pPOU->Body, pPOU );

		    IEC61131_LIST_FOREACH( pPOU->Variables, ( ( IEC61131_TOKEN * ) Element->Data )->Scope = pPOU );

		    $$ = ( IEC61131_TOKEN * ) pPOU;
		}
	| FUNCTION derived_function_name ':' derived_type_name function_var_declarations function_body END_FUNCTION
		{
            IEC61131_LIST_ELEMENT *pElement;
            IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_POU *pPOU;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $2;
		    pPOU = IEC61131_NEW_POU( FUNCTION, pRaw->Data );
            pPOU->Type = $4;
		    pPOU->Body = $6;
		    pPOU->Variables = $5;
		    BASE_DESTROY( pRaw );

            pElement = IEC61131_LIST_TAIL( pPOU->Body );
            for( ;; )
            {
                if( pElement != NULL )
                {
                    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
                    if( ( pInstr != NULL ) &&
                            ( ( ( IEC61131_TOKEN * ) pInstr )->Id == RET ) )
                    {
                        break;
                    }
                }

                IEC61131_LIST_INSERT_TAIL( pPOU->Body, IEC61131_NEW_INSTRUCTION( RET ) );
                break;
            }
            _iec61131_setTokenScope( pPOU->Body, pPOU );

		    IEC61131_LIST_FOREACH( pPOU->Variables, ( ( IEC61131_TOKEN * ) Element->Data )->Scope = pPOU );

		    $$ = ( IEC61131_TOKEN * ) pPOU;
		}
;

function_var_declarations:
	io_var_declarations
		{ $$ = $1; }
	| function_var_decls
		{ $$ = $1; }
	| function_var_declarations io_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
	| function_var_declarations function_var_decls
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
;

/* io_var_declarations ::= input_declarations | output_declarations
        | input_output_declarations */

io_var_declarations:
	input_declarations
        { $$ = $1; }
	| output_declarations
        { $$ = $1; }
	| input_output_declarations
        { $$ = $1; }
;

/* function_var_decls ::= 'VAR' ['CONSTANT']
        var2_init_decl ';' {var2_init_decl ';'} 'END_VAR' */

function_var_decls:
	VAR var2_init_decl_list END_VAR
		{ $$ = $2; }
	| VAR CONSTANT var2_init_decl_list END_VAR
		{ $$ = $3; }
;

var2_init_decl_list:
	var2_init_decl ';'
		{
		    $$ = IEC61131_NEW_LIST();
			IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| var2_init_decl_list var2_init_decl ';'
		{ 
			IEC61131_LIST_INSERT_TAIL( $1, $2 ); 
			$$ = $1;
		}
;

/* function_body ::= ladder_diagram | function_block_diagram | instruction_list
        | statement_list | <other languages> */

function_body:
	language_list
;

/* var2_init_decl ::= var1_init_decl | array_var_init_decl
        | structured_var_init_decl | string_var_declaration */

var2_init_decl:
	var1_init_decl
		{ $$ = $1; }
//	| array_var_init_decl
//	| structured_var_init_decl
//	| string_var_declaration
;


/* B.1.5.2 Function blocks */

/* function_block_type_name ::= standard_function_block_name
        | derived_function_block_name */

/* standard_function_block_name ::= <as defined in 2.5.2.3> */

/* derived_function_block_name ::= identifier  */

derived_function_block_name:
	identifier
;

/* function_block_declaration ::=
        'FUNCTION_BLOCK' derived_function_block_name
          { io_var_declarations | other_var_declarations }
          function_block_body
        'END_FUNCTION_BLOCK' */

function_block_declaration:
	FUNCTION_BLOCK derived_function_block_name function_block_var_declarations function_block_body END_FUNCTION_BLOCK
		{
            IEC61131_LIST_ELEMENT *pElement;
            IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_POU *pPOU;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $2;
		    pPOU = IEC61131_NEW_POU( FUNCTION_BLOCK, pRaw->Data );
		    pPOU->Body = $4;
		    pPOU->Variables = $3;
		    BASE_DESTROY( pRaw );

            pElement = IEC61131_LIST_TAIL( pPOU->Body );
            for( ;; )
            {
                if( pElement != NULL )
                {
                    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
                    if( ( pInstr != NULL ) &&
                            ( ( ( IEC61131_TOKEN * ) pInstr )->Id == RET ) )
                    {
                        break;
                    }
                }

                IEC61131_LIST_INSERT_TAIL( pPOU->Body, IEC61131_NEW_INSTRUCTION( RET ) );
                break;
            }
            _iec61131_setTokenScope( pPOU->Body, pPOU );

		    IEC61131_LIST_FOREACH( pPOU->Variables, ( ( IEC61131_TOKEN * ) Element->Data )->Scope = pPOU );

		    $$ = ( IEC61131_TOKEN * ) pPOU;
		}
;

function_block_var_declarations:
	io_var_declarations
		{ $$ = $1; }
	| other_var_declarations
		{ $$ = $1; }
	| function_block_var_declarations io_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
	| function_block_var_declarations other_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
;

/* other_var_declarations ::= external_var_declarations | var_declarations
        | retentive_var_declarations | non_retentive_var_declarations
        | temp_var_decls | incompl_located_var_declarations */

other_var_declarations:
	external_var_declarations
	| var_declarations
	| retentive_var_declarations
	| non_retentive_var_declarations
	| temp_var_decls
//	| incompl_located_var_declarations
;

/* temp_var_decls ::=
        'VAR_TEMP'
          temp_var_decl ';'
          {temp_var_decl ';'}
        'END_VAR' */

temp_var_decls:
	VAR_TEMP temp_var_decl_list END_VAR
		{ $$ = $2; }
;

temp_var_decl_list:
	temp_var_decl ';'
	| temp_var_decl_list temp_var_decl ';'
;

/* non_retentive_var_declarations ::=
        'VAR' 'NON_RETAIN'
          var_init_decl ';'
          {var_init_decl ';'}
        'END_VAR' */

non_retentive_var_declarations:
	VAR NON_RETAIN var_init_decl_list END_VAR
		{
		    IEC61131_LIST_FOREACH( $3, ( ( IEC61131_TOKEN_VARIABLE * ) Element->Data )->Flags |= FLAGS_NON_RETAIN ); 
		    $$ = $3;
		}
;

/* function_block_body ::= sequential_function_chart | ladder_diagram
        | function_block_diagram | instruction_list | statement_list
        | <other languages> */

function_block_body:
	language_list
;


/* B.1.5.3 Programs */

/* program_type_name ::= identifier */

program_type_name:
	identifier
;

/* program_declaration ::=
        'PROGRAM' program_type_name
          { io_var_declarations | other_var_declarations
            | located_var_declarations | program_access_decls }
          function_block_body
        'END_PROGRAM' */

program_declaration:
	PROGRAM program_type_name program_var_declarations function_block_body END_PROGRAM
		{
            IEC61131_LIST_ELEMENT *pElement;
            IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_POU *pPOU;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $2;
		    pPOU = IEC61131_NEW_POU( PROGRAM, pRaw->Data );
		    pPOU->Body = $4;
		    pPOU->Variables = $3;
		    BASE_DESTROY( pRaw );

            pElement = IEC61131_LIST_TAIL( pPOU->Body );
            for( ;; )
            {
                if( pElement != NULL )
                {
                    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
                    if( ( pInstr != NULL ) &&
                            ( ( ( IEC61131_TOKEN * ) pInstr )->Id == RET ) )
                    {
                        break;
                    }
                }

                IEC61131_LIST_INSERT_TAIL( pPOU->Body, IEC61131_NEW_INSTRUCTION( RET ) );
                break;
            }
            _iec61131_setTokenScope( pPOU->Body, pPOU );

            IEC61131_LIST_FOREACH( pPOU->Variables, ( ( IEC61131_TOKEN * ) Element->Data )->Scope = pPOU ); 

		    if( iec61131_validatePOU( Context, pPOU ) != 0 )
		    {
		        YYERROR;
		    }

		    $$ = ( IEC61131_TOKEN * ) pPOU;
		}
;

program_var_declarations:
	io_var_declarations
		{ $$ = $1; }
	| other_var_declarations
		{ $$ = $1; }
	| located_var_declarations
		{ $$ = $1; }
	| program_access_decls
		{ $$ = $1; }
	| program_var_declarations io_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
	| program_var_declarations other_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
	| program_var_declarations located_var_declarations
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
	| program_var_declarations program_access_decls
		{ IEC61131_LIST_CONSOLIDATE( $1, $2 ); }
;


/* program_access_decls ::=
        'VAR_ACCESS' program_access_decl ';'
          { program_access_decl ';' }
        'END_VAR' */

program_access_decls:
	VAR_ACCESS program_access_decl_list END_VAR
		{ $$ = $2; }
;

program_access_decl_list:
	program_access_decl ';'
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| program_access_decl_list program_access_decl ';'
		{ IEC61131_LIST_INSERT_TAIL( $1, $2 ); }
;

/* program_access_decl ::= access_name ':' symbolic_variable ':'
        non_generic_type_name [direction] */

program_access_decl:
	access_name ':' symbolic_variable ':' non_generic_type_name
	| access_name ':' symbolic_variable ':' non_generic_type_name direction
;


/* B.1.6 Sequential function chart elements */


/* B.1.7 Configuration elements */

/* configuration_name ::= identifier */

//configuration_name:
//	identifier
//;

/* resource_type_name ::= identifier */

//resource_type_name:
//	identifier
//;

/* configuration_declaration ::=
        'CONFIGURATION' configuration_name
          [global_var_declarations]
          (single_resource_declaration
            | (resource_declaration {resource_declaration}))
          [access_declarations]
          [instance_specific_initializations]
        'END_CONFIGURATION' */

//configuration_declaration:
//	CONFIGURATION configuration_name
//		global_var_declarations_list
//		single_resource_declaration
//		access_declarations_list
//		instance_specific_initializations_list
//		END_CONFIGURATION
//	| CONFIGURATION configuration_name
//		global_var_declarations_list
//		resource_declaration_list
//		access_declarations_list
//		instance_specific_initializations_list
//		END_CONFIGURATION
//;
//
//global_var_declarations_list:
//	| global_var_declarations
//;
//
//resource_declaration_list:
//	resource_declaration
//	| resource_declaration_list resource_declaration
//;
//
//access_declarations_list:
//	| access_declarations
//;
//
//instance_specific_initializations_list:
//	| instance_specific_initializations
//;

/* resource_declaration ::=
        'RESOURCE' resource_name 'ON' resource_type_name
          [global_var_declarations]
          single_resource_declaration
        'END_RESOURCE' */

//resource_declaration:
//	RESOURCE resource_name ON resource_type_name
//		global_var_declarations_list
//		single_resource_declaration
//		END_RESOURCE
//;

/* single_resource_declaration ::=
        {task_configuration ';'}
        program_configuration ';'
        {program_configuration ';'} */

//single_resource_declaration:
//	task_configuration_list program_configuration_list
//;

//task_configuration_list:
//	| task_configuration ';'
//	| task_configuration_list task_configuration ';'
//;

//program_configuration_list:
//	program_configuration ';'
//	| program_configuration_list program_configuration ';'
//;

/* resource_name ::= identifier */

//resource_name:
//	identifier
//;

/* access_declarations ::=
        'VAR_ACCESS'
          access_declaration ';'
          {access_declaration ';'}
        'END_VAR' */

//access_declarations:
//	VAR_ACCESS access_declaration_list END_VAR
//;

//access_declaration_list:
//	access_declaration ';'
//	| access_declaration_list access_declaration ';'
//;

/* access_declaration ::= access_name ':' access_path ':' non_generic_type_name
        [direction] */

//access_declaration:
//	access_name ':' access_path ':' non_generic_type_name
//	| access_name ':' access_path ':' non_generic_type_name direction
//;

/* access_path ::= [resource_name '.'] direct_variable
        | [resource_name '.'] [program_name '.']
          {fb_name'.'} symbolic_variable */

//access_path:
//	resource_name '.' direct_variable
//	| direct_variable
//	| resource_name '.' program_name '.' symbolic_variable
//	| program_name '.' symbolic_variable
//;

/* global_var_reference ::=
        [resource_name '.'] global_var_name ['.' structure_element_name] */

//global_var_reference:
//	global_var_name
//	| global_var_name '.' structure_element_name
//	| resource_name '.' global_var_name
//	| resource_name '.' global_var_name '.' structure_element_name
//;

/* access_name ::= identifier */

access_name:
	identifier
;

/* program_output_reference ::= program_name '.' symbolic_variable */

//program_output_reference:
//	program_name '.' symbolic_variable
//;

/* program_name ::= identifier */

//program_name:
//	identifier
//;

/* direction ::= 'READ_WRITE' | 'READ_ONLY' */

direction:
	READ_WRITE
	| READ_ONLY
;

/* task_configuration ::= 'TASK' task_name task_initialization */

//task_configuration:
//	TASK task_name task_initialization
//;

/* task_name := identifier */

//task_name:
//	identifier
//;

/* task_initialization ::=
        '(' ['SINGLE' ':=' data_source ',']
          ['INTERVAL' ':=' data_source ',']
          'PRIORITY' ':=' integer ')' */

//task_initialization:
//	'(' optional_SINGLE
//		optional_INTERVAL
//		PRIORITY ASSIGN integer ')'
//;

//optional_SINGLE:
//	| SINGLE ASSIGN data_source ','
//;

//optional_INTERVAL:
//	| INTERVAL ASSIGN data_source ','
//;

/* data_source ::= constant | global_var_reference | program_output_reference
        | direct_variable */

//data_source:
//	constant
//	| global_var_reference
//	| program_output_reference
//	| direct_variable
//;

/* program_configuration ::=
        'PROGRAM' [RETAIN | NON_RETAIN]
          program_name ['WITH' task_name] ':' program_type_name
          ['(' prog_conf_elements ')'] */

//program_configuration:
//	PROGRAM RETAIN
//		program_name optional_task_name ':' program_type_name
//		optional_prog_conf_elements
//	| PROGRAM NON_RETAIN
//		program_name optional_task_name ':' program_type_name
//		optional_prog_conf_elements
//;

//optional_task_name:
//	| WITH task_name
//;

//optional_prog_conf_elements:
//	| '(' prog_conf_elements ')'
//;


/* prog_conf_elements ::= prog_conf_element {',' prog_conf_element} */

//prog_conf_elements:
//	prog_conf_element
//	| prog_conf_elements ',' prog_conf_element
//;

/* prog_conf_element ::= fb_task | prog_cnxn */

//prog_conf_element:
//	fb_task
//	| prog_cnxn
//;

/* fb_task ::= fb_name 'WITH' task_name */

//fb_task:
//	fb_name WITH task_name
//;

/* prog_cnxn ::= symbolic_variable ':=' prog_data_source
        | symbolic_variable '=>' data_sink */

//prog_cnxn:
//	symbolic_variable ASSIGN prog_data_source
//	| symbolic_variable SENDTO data_sink
//;

/* prog_data_source ::=
        constant | enumerated_value | global_var_reference | direct_variable */

//prog_data_source:
//	constant
//	| enumerated_value
//	| global_var_reference
//	| direct_variable
//;

/* data_sink ::= global_var_reference | direct_variable */

//data_sink:
//	global_var_reference
//	| direct_variable
//;

/* instance_specific_initializations ::=
        'VAR_CONFIG'
          instance_specific_init ';'
          {instance_specific_init ';'}
        'END_VAR' */

//instance_specific_initializations:
//	VAR_CONFIG instance_specific_init_list END_VAR
//;

//instance_specific_init_list:
//	instance_specific_init ';'
//	| instance_specific_init_list instance_specific_init ';'
//;

/* instance_specific_init ::=
        resource_name '.' program_name '.' {fb_name '.'}
        ((variable_name [location] ':' located_var_spec_init) |
          (fb_name ':' function_block_type_name ':=' structure_initialization)) */

//instance_specific_init:
//	resource_name '.' program_name '.' variable_name ':' located_var_spec_init
//	| resource_name '.' program_name '.' variable_name location ':' located_var_spec_init
//	| resource_name '.' program_name '.' fb_name ':' function_block_type_name ASSIGN structure_initialization
//;


/* B.2 Language IL (Instruction List) */


/* B.2.1 Instructions and operands */

/* instruction_list ::= il_instruction {il_instruction} */

instruction_list:
	il_instruction
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| instruction_list il_instruction
		{
		    $$ = $1; 
		    IEC61131_LIST_INSERT_TAIL( $$, $2 ); 
		}
;

/* il_instruction ::= [label':'] [  il_simple_operation
        | il_expression
        | il_jump_operation
        | il_fb_call
        | il_formal_funct_call
        | il_return_operator      ]     EOL {EOL} */

il_instruction:
	il_instruction_list eol_list
	| label ':' il_instruction_list eol_list
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $1;
		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $3;
		    IEC61131_INSTRUCTION_LABEL( pInstr, pRaw->Data );
		    BASE_DESTROY( pRaw );

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
	| label ':' eol_list
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;
		    IEC61131_TOKEN_RAW *pRaw;

		    pRaw = ( IEC61131_TOKEN_RAW * ) $1;
		    pInstr = IEC61131_NEW_INSTRUCTION( NOP );
		    IEC61131_INSTRUCTION_LABEL( pInstr, pRaw->Data );
		    BASE_DESTROY( pRaw );

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}

    | label il_instruction_list eol_list
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing ':' between label and instruction" );
        }
;

il_instruction_list:
	il_simple_operation
	| il_expression
	| il_jump_operation
	| il_fb_call
	| il_formal_funct_call
	| il_return_operator
;

/* label ::= identifier */

label:
	identifier
;

/* il_simple_operation ::= ( il_simple_operator [il_operand] )
        | ( function_name [il_operand_list] ) */

il_simple_operation:
	il_simple_operator
	| il_simple_operator il_operand
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Operand = $2;

		    $$ = ( IEC61131_TOKEN * ) pInstr; 
		}
//	| identifier
//		{
//		    IEC61131_TOKEN_INSTRUCTION *pInstr;
//
//		    pInstr = IEC61131_NEW_INSTRUCTION( CAL );
//		    pInstr->Branch = $1;
//
//		    $$ = ( IEC61131_TOKEN * ) pInstr;
//		}
//	| identifier il_operand_list
//		{
//		    IEC61131_TOKEN_INSTRUCTION *pInstr;
//
//		    pInstr = IEC61131_NEW_INSTRUCTION( CAL );
//		    pInstr->Branch = $1;
//		    pInstr->List = $2;
//
//		    $$ = ( IEC61131_TOKEN * ) pInstr;
//		}
;

/* il_expression ::= il_expr_operator '(' [il_operand] EOL {EOL}
        [simple_instr_list] ')' */

il_expression:
	il_expr_operator '(' il_operand eol_list simple_instr_list ')'
		{ 
		    IEC61131_TOKEN_INSTRUCTION *pInstr, *pLoad;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
            pLoad = IEC61131_NEW_INSTRUCTION( LD );
            pLoad->Operand = $3;
            IEC61131_LIST_INSERT_HEAD( $5, pLoad );
            pInstr->Operand = NULL;
		    pInstr->List = $5;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
	| il_expr_operator '(' il_operand eol_list ')'
		{ 
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Operand = $3;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
	| il_expr_operator '(' eol_list simple_instr_list ')'
		{ 
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
            pInstr->Operand = NULL;
		    pInstr->List = $4;

		    $$ = ( IEC61131_TOKEN * ) pInstr; 
		}
	| il_expr_operator '(' eol_list ')'
;

/* il_jump_operation ::= il_jump_operator label */

il_jump_operation:
	il_jump_operator label
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}

    | il_jump_operator error
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid label for jump instruction" );
        }
;

/* il_fb_call ::= il_call_operator fb_name ['('
        (EOL {EOL} [ il_param_list ]) | [ il_operand_list ] ')'] */

il_fb_call:
	il_call_operator fb_name
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;

		    $$ = ( IEC61131_TOKEN * ) $1;
		}
	| il_call_operator fb_name '(' ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;

		    $$ = ( IEC61131_TOKEN * ) $1;
		}
	| il_call_operator fb_name '(' eol_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;

		    $$ = ( IEC61131_TOKEN * ) $1;
		}
	| il_call_operator fb_name '(' eol_list il_param_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;
		    pInstr->List = $5;

		    $$ = ( IEC61131_TOKEN * ) $1;
		}
	| il_call_operator fb_name '(' il_operand_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;
		    pInstr->List = $4;

		    $$ = ( IEC61131_TOKEN * ) $1;
		}

    | il_call_operator error
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid function block name in function block call" );
        }
    | il_call_operator error '(' ')'
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid function block name in function block call" );
        }
    | il_call_operator error '(' eol_list ')'
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid function block name in function block call" );
        }
    | il_call_operator error '(' eol_list il_param_list ')'
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid function block name in function block call" );
        }
    | il_call_operator error '(' il_operand_list ')'
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "invalid function block name in function block call" );
        }
    | il_call_operator '(' ')'
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing function block name in function block call" );
        }
    | il_call_operator '(' eol_list ')'
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing function block name in function block call" );
        }
    | il_call_operator '(' eol_list il_param_list ')'
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing function block name in function block call" );
        }
    | il_call_operator '(' il_operand_list ')'
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing function block name in function block call" );
        }
    | il_call_operator fb_name ')'
        {
            IEC61131_ERROR( @3.first_line, @3.first_column, "missing '(' between function block name and operand list" );
        }
    | il_call_operator fb_name il_operand_list ')'
        {
            IEC61131_ERROR( @3.first_line, @3.first_column, "missing '(' between function block name and operand list" );
        }
    | il_call_operator fb_name '(' eol_list il_param_list
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing ')' after parameter list in function block call" );
        }
    | il_call_operator fb_name '(' il_operand_list
        {
            IEC61131_ERROR( @1.first_line, @1.first_column, "missing ')' after operand list in function block call" );
        }
;

/* il_formal_funct_call ::= function_name '(' EOL {EOL} [il_param_list] ')' */

il_formal_funct_call:
	identifier '(' eol_list il_param_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = IEC61131_NEW_INSTRUCTION( CAL );
		    pInstr->Branch = $1;
		    pInstr->List = $4;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
	| identifier '(' eol_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = IEC61131_NEW_INSTRUCTION( CAL );
		    pInstr->Branch = $1;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
;

/* il_operand ::= constant | variable | enumerated_value */

il_operand:
	constant
	| variable
//	| enumerated_value
;

/* il_operand_list ::= il_operand {',' il_operand} */

il_operand_list:
	il_operand
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| il_operand_list2
;

il_operand_list2:
	il_operand ',' il_operand
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		    IEC61131_LIST_INSERT_TAIL( $$, $3 );
		}
	| il_operand_list2 ',' il_operand
		{ IEC61131_LIST_INSERT_TAIL( $1, $3 ); }
;


/* simple_instr_list ::= il_simple_instruction {il_simple_instruction} */

simple_instr_list:
	il_simple_instruction
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| simple_instr_list il_simple_instruction
		{
		    IEC61131_LIST_INSERT_TAIL( $1, $2 );
		}
;

/* il_simple_instruction ::=
        (il_simple_operation | il_expression | il_formal_funct_call)
        EOL {EOL} */

il_simple_instruction:
	il_simple_operation eol_list
	| il_expression eol_list
	| il_formal_funct_call eol_list

    | il_simple_operation error
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing EOL after IL instruction" );
        }
    | il_formal_funct_call error
        {
            IEC61131_ERROR( @2.first_line, @2.first_column, "missing EOL after formal function call IL instruction" );
        }
;

/* il_param_list ::= {il_param_instruction} il_param_last_instruction */

il_param_list:
    il_param_last_instruction
        {
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
        }
	| il_param_instruction_list il_param_last_instruction
		{
		    $$ = $1;
		    IEC61131_LIST_INSERT_TAIL( $$, $2 );
		}
;

il_param_instruction_list:
	il_param_instruction
		{
		    $$ = IEC61131_NEW_LIST();
		    IEC61131_LIST_INSERT_TAIL( $$, $1 );
		}
	| il_param_instruction_list il_param_instruction
		{ 
            IEC61131_LIST_INSERT_TAIL( $1, $2 ); 
        }
;

/* il_param_instruction ::= (il_param_assignment | il_param_out_assignment) ','
        EOL {EOL} */

il_param_instruction:
	il_param_assignment ',' eol_list
		{ $$ = $1; }
	| il_param_out_assignment ',' eol_list
		{ $$ = $1; }
;

/* il_param_last_instruction ::=
        ( il_param_assignment | il_param_out_assignment ) EOL {EOL} */

il_param_last_instruction:
	il_param_assignment
	| il_param_out_assignment
;

/* il_param_assignment ::= il_assign_operator ( il_operand | ( '(' EOL {EOL}
        simple_instr_list ')' ) ) */

il_param_assignment:
	il_assign_operator il_operand
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Operand = $2;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
	| il_assign_operator '(' eol_list simple_instr_list ')'
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->List = $4;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
;

/* il_param_out_assignment ::= il_assign_out_operator variable */

il_param_out_assignment:
	il_assign_out_operator variable
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $1;
		    pInstr->Branch = $2;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
;


/* B.2.2 Operators */

/* il_simple_operator ::=   'NOP' | 'LD' | 'LDN' | 'ST' | 'STN' | 'NOT' | 'S'
        | 'R' | 'S1' | 'R1' | 'CLK' | 'CU' | 'CD' | 'PV'
        | 'IN' | 'PT' | il_expr_operator */

il_simple_operator:
    NOP
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| LD
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| LDN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| ST
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| STN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| NOT
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| S
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| R
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| S1
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| R1
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| CLK
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| CU
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| CD
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| PV
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| IN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| PT
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| il_expr_operator
;

/* il_expr_operator ::=  'AND' | '&' | 'OR' | 'XOR' | 'ANDN' | '&N' | 'ORN'
        | 'XORN' | 'ADD' | 'SUB' | 'MUL' | 'DIV' | 'MOD' | 'GT' | 'GE' | 'EQ '
        | 'LT' | 'LE' | 'NE' */

il_expr_operator:
	AND
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| AND2
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| OR
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| XOR
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| ANDN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| ANDN2
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| ORN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| XORN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| ADD
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| SUB
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| MUL
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| DIV
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| MOD
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| GT
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| GE
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| EQ
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| LT
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| LE
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| NE
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
;

/* il_assign_operator ::= variable_name ':=' */

il_assign_operator:
	variable_name ASSIGN
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $2;
		    pInstr->Branch = $1;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
;

/* il_assign_out_operator ::= ['NOT'] variable_name '=>' */

il_assign_out_operator:
	variable_name SENDTO
		{
		    IEC61131_TOKEN_INSTRUCTION *pInstr;

		    pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) $2;
		    pInstr->Branch = $1;

		    $$ = ( IEC61131_TOKEN * ) pInstr;
		}
//	| NOT variable_name SENDTO
;

/* il_call_operator ::= 'CAL' | 'CALC' | 'CALCN' */

il_call_operator:
	CAL
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| CALC
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| CALCN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
;

/* il_return_operator ::= 'RET' | 'RETC' | 'RETCN' */

il_return_operator:
	RET
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| RETC
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| RETCN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
;

/* il_jump_operator ::= 'JMP' | 'JMPC' | 'JMPCN' */

il_jump_operator:
	JMP
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| JMPC
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
	| JMPCN
		{ IEC61131_TOKEN_LOCATION( $1, @1.first_line, @1.first_column ); }
;


%%


void 
_iec61131_setTokenScope( IEC61131_LIST *List, void *Scope )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_INSTRUCTION *pInstr;
    IEC61131_TOKEN *pToken;


    for( pElement = IEC61131_LIST_HEAD( List );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;

        /*
            As the List member of the IEC61131_TOKEN_INSTRUCTION data structure may be 
            used to store varying information depending upon the instruction, an 
            additional check must be performed on the instruction type before iterating 
            through List members, attempting to set token scope.
        */

        if( pInstr->List != NULL )
        {
            switch( ( ( IEC61131_TOKEN * ) pInstr )->Id )
            {
                case AND:
                case AND2:
                case OR:
                case XOR:
                case ANDN:
                case ANDN2:
                case ORN:
                case XORN:
                case ADD:
                case SUB:
                case MUL:
                case DIV:
                case MOD:
                case GT:
                case GE:
                case EQ:
                case LT:
                case LE:
                case NE:

                    if( IEC61131_LIST_SIZE( pInstr->List ) > 0 )
                    {
                        _iec61131_setTokenScope( pInstr->List, Scope );
                    }
                    break;

                case CAL:
                case CALC:
                case CALCN:
                default:

                    break;
            }
        }


        /*
            The following condition is included in order to maintain a very tight 
            definition for the scope of a token - For example to ensure that the 
            application of program scope does not override that which may have already 
            been declared at a function or function block level.
        */

        pToken = ( IEC61131_TOKEN * ) pInstr;
        if( pToken->Scope == NULL )
        {
            pToken->Scope = Scope;
        }
    }
}


IEC61131_TYPE 
_iec61131_tokenToType( int Id )
{
    switch( Id )
    {
        case BOOL:      return TYPE_BOOL;
        case SINT:      return TYPE_SINT;
        case INT:       return TYPE_INT;
        case DINT:      return TYPE_DINT;
        case LINT:      return TYPE_LINT;
        case USINT:     return TYPE_USINT;
        case UINT:      return TYPE_UINT;
        case UDINT:     return TYPE_UDINT;
        case ULINT:     return TYPE_ULINT;
        case REAL:      return TYPE_REAL;
        case LREAL:     return TYPE_LREAL;
        case TIME:      return TYPE_TIME;
        case DATE:      return TYPE_DATE;
        case TOD:       return TYPE_TOD;
        case DT:        return TYPE_DT;
        case STRING:    return TYPE_STRING;
        case BYTE:      return TYPE_BYTE;
        case WORD:      return TYPE_WORD;
        case DWORD:     return TYPE_DWORD;
        case LWORD:     return TYPE_LWORD;
        case WSTRING:   return TYPE_WSTRING;
        default:

            return TYPE_NONE;
    }
}


void
yyerror( IEC61131_CC *Context, const char *Msg )
{
    fprintf( stderr, "%s: error: %s\n", Context->Source, Msg );
}


