#ifndef __IEC61131_TYPES_H
#define __IEC61131_TYPES_H


#include <stddef.h>
#include <stdint.h>

#include "config.h"


#define BOOL_FALSE      ( ( uint8_t ) 0 )
#define BOOL_TRUE       ( ( uint8_t ) 1 )


/*!
    \param IEC61131_TYPE

    This enumeration specifies elementary data types defined within the IEC
    61131-3 standard.  These elementary data types are described in Table 10.
*/

typedef enum _IEC61131_TYPE
{
    TYPE_NONE = 0,

    TYPE_BOOL = 1,
    TYPE_SINT = 2,
    TYPE_INT = 3,
    TYPE_DINT = 4,
    TYPE_LINT = 5,
    TYPE_USINT = 6,
    TYPE_UINT = 7,
    TYPE_UDINT = 8,
    TYPE_ULINT = 9,
    TYPE_REAL = 10,
    TYPE_LREAL = 11,
    TYPE_TIME = 12,
    TYPE_DATE = 13,
    TYPE_TOD = 14,
    TYPE_DT = 15,
    TYPE_STRING = 16,
    TYPE_BYTE = 17,
    TYPE_WORD = 18,
    TYPE_DWORD = 19,
    TYPE_LWORD = 20,
    TYPE_WSTRING = 21,

    TYPE_FUNCTIONBLOCK = 31
}
IEC61131_TYPE;


typedef struct _IEC61131_DATA
{
    IEC61131_TYPE Type;

    union
    {
        uint8_t         B1;
        int8_t          S8;
        uint8_t         U8;
        int16_t         S16;
        uint16_t        U16;
        int32_t         S32;
        uint32_t        U32;
        int64_t         S64;
        uint64_t        U64;
        float           Float;
        double          Double;
        char            *Pointer;

        uint8_t         B8;
        uint16_t        B16;
        uint32_t        B32;
        uint64_t        B64;
#ifdef CONFIG_SUPPORT_DATETIME

        int32_t         Time;
#endif
    }
    Value;

    size_t Length;
}
IEC61131_DATA;


#endif /* __IEC61131_TYPES_H */
