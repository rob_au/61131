#ifndef __IEC61131_BYTECODE_H
#define __IEC61131_BYTECODE_H


#include "iec61131_types.h"


/*!
    This header file is intended to describe the byte code format employed by
    the IEC 61131-3 compiler and associated virtual machine provided by this
    project.  While the nomenclature employed within the IEC 61131-3 standard
    has been maintained within this header file, the following additional
    definitions and nomenclature have been employed:

    - The combination of IEC 61131-3 operator, modifier and operand identifier
        are deemed to comprise a single virtual machine _byte code_;

    - The combination of IEC 61131-3 operator and modifier within a single
        virtual machine byte code is referred to as an _instruction_;
*/


/*!
    \enum IEC61131_BYTECODE_OP

    This enumeration specifies Instruction List (IL) opcodes defined within the
    IEC 61131-3 standard.  These opcodes and permitted modifiers are described
    in Table 52.  Additionally, while not specified within the IEC 61131-3
    standard, support has also been implemented for a no-operation (NOP) 
    operator for development purposes and a paired set of push (PUSH) and pop 
    (POP) operators for manipulation of the data (parameter) stack in 
    association with call operations (CAL).

    The operator and modifier enumeration values are combined to form a single
    8-bit instruction.
*/

typedef enum _IEC61131_BYTECODE_OP
{
    OP_NONE = 0,
    OP_NOP = 1,
    OP_PUSH = 2,
    OP_POP = 3,
    OP_LD = 4,
    OP_ST = 5,
    OP_S = 6,
    OP_R = 7,
    OP_AND = 8,
    OP_OR = 9,
    OP_XOR = 10,
    OP_NOT = 11,
    OP_ADD = 12,
    OP_SUB = 13,
    OP_MUL = 14,
    OP_DIV = 15,
    OP_MOD = 16,
    OP_GT = 17,
    OP_GE = 18,
    OP_EQ = 19,
    OP_NE = 20,
    OP_LE = 21,
    OP_LT = 22,
    OP_JMP = 23,
    OP_CAL = 24,
    OP_RET = 25,
    OP_PARENTHESIS_RIGHT = 26,

    /* OP_LAST = 31 */
}
IEC61131_BYTECODE_OP;


/*!
    \enum IEC61131_BYTECODE_OPERAND

    This enumeration specifies operand types defined within the IEC 61131-3
    standard that may be employed in both literal and symbolic means in
    association with Instruction List (IL) opcodes.  These elementary data
    types are described in Table 10.

    It should be noted that there is an intended equivalence between the
    values defined below and those defined in association with the
    IEC61131_TYPE enumeration.
*/

typedef enum _IEC61131_BYTECODE_OPERAND
{
    OPERAND_NONE = 0,

    OPERAND_BOOL = 1,
    OPERAND_SINT = 2,
    OPERAND_INT = 3,
    OPERAND_DINT = 4,
    OPERAND_LINT = 5,
    OPERAND_USINT = 6,
    OPERAND_UINT = 7,
    OPERAND_UDINT = 8,
    OPERAND_ULINT = 9,
    OPERAND_REAL = 10,
    OPERAND_LREAL = 11,
    OPERAND_TIME = 12,
    OPERAND_DATE = 13,
    OPERAND_TOD = 14,
    OPERAND_DT = 15,
    OPERAND_STRING = 16,
    OPERAND_BYTE = 17,
    OPERAND_WORD = 18,
    OPERAND_DWORD = 19,
    OPERAND_LWORD = 20,
    OPERAND_WSTRING = 21,

    OPERAND_FUNCTIONBLOCK = 31,

    OPERAND_DIRECT_NONE = 0,
    OPERAND_DIRECT_INPUT = 32,
    OPERAND_DIRECT_OUTPUT = 64,
    OPERAND_DIRECT_MEMORY = 96,
    OPERAND_DIRECT_MASK = 96,

    OPERAND_SYMBOL = 128,

    OPERAND_MASK = 224,
}
IEC61131_BYTECODE_OPERAND;


/*!
    \enum IEC61131_BYTECODE_MODIFIER

    This enumeration specifies modifiers which may be defined in association
    with selected standard operators as defined within the IEC 61131-3
    standard.  As multiple modifiers can be applied simultaneously - namely, C
    and N modifiers in association with the left parenthesis modifier - in
    association with certain operators, these modifiers are defined with bit
    values.

    The operator and modifier enumeration values are combined to form a single
    8-bit operation identifier.
*/

typedef enum _IEC61131_BYTECODE_MODIFIER
{
    MODIFIER_NONE = 0,

    /*!
        The modifier "C" indicates that the associated instruction shall be
        performed only if the value of the currently evaluated result is a Boolean
        true value.
    */

    MODIFIER_C = 1,

    /*!
        The modifier "N" indicates that the associated instruction shall be
        performed only if the value of the currently evaluated result is a Boolean
        false value.
    */

    MODIFIER_N = 2,

    /*!
        The left parenthesis modifier "(" indicates that evaluation of the operator
        shall be deferred until a right parenthesis operator ")" is encountered.
    */

    MODIFIER_PARENTHESIS_LEFT = 4
}
IEC61131_BYTECODE_MODIFIER;


/*!
    The following definitions have been specified to simplify the accessing of
    IEC 61131-3 byte code operand, operator and modifier where bit-packed
    within a single 16-bit value.
*/

struct _IEC61131_BYTECODE
{
    unsigned char Op:5;

    unsigned char Modifier:3;

    unsigned char Operand:8;
}
__attribute__((__packed__));

typedef struct _IEC61131_BYTECODE IEC61131_BYTECODE;

/* assert( sizeof( IEC61131_BYTECODE ) == sizeof( uint16_t ) ); */

#define IEC61131_BYTECODE_OP_MASK               (0xf800)

#define IEC61131_BYTECODE_OP_SHIFT              (11)

#define IEC61131_BYTECODE_MODIFIER_MASK         (0x0700)

#define IEC61131_BYTECODE_MODIFIER_SHIFT        (8)

#define IEC61131_BYTECODE_OPERAND_MASK          (0x00ff)

#define IEC61131_BYTECODE_OPERAND_SHIFT         (0)

#define G_OP( v )                               \
        ( ( ( v ) & IEC61131_BYTECODE_OP_MASK ) >> IEC61131_BYTECODE_OP_SHIFT )

#define G_OPERAND( v )                          \
        ( ( ( v ) & IEC61131_BYTECODE_OPERAND_MASK ) >> IEC61131_BYTECODE_OPERAND_SHIFT )

#define G_MODIFIER( v )                         \
        ( ( ( v ) & IEC61131_BYTECODE_MODIFIER_MASK ) >> IEC61131_BYTECODE_MODIFIER_SHIFT )

#define S_INSTRUCTION( op, modifier )           \
        ( ( ( ( op ) << IEC61131_BYTECODE_OP_SHIFT ) & IEC61131_BYTECODE_OP_MASK ) | \
        ( ( ( modifier ) << IEC61131_BYTECODE_MODIFIER_SHIFT ) & IEC61131_BYTECODE_MODIFIER_MASK ) )

#define S_BYTECODE( op, modifier, operand )     \
        ( ( ( ( op ) << IEC61131_BYTECODE_OP_SHIFT ) & IEC61131_BYTECODE_OP_MASK ) | \
        ( ( ( modifier ) << IEC61131_BYTECODE_MODIFIER_SHIFT ) & IEC61131_BYTECODE_MODIFIER_MASK ) | \
        ( ( ( operand ) << IEC61131_BYTECODE_OPERAND_SHIFT ) & IEC61131_BYTECODE_OPERAND_MASK ) )


/*!
    The following definitions are employed to distinguish between calls to
    functions defined in the IEC 61131-3 program environment and those 
    implemented in supporting C language libraries.
*/

#define IEC61131_BYTECODE_FUNCTION              (0x80000000)
#define IEC61131_BYTECODE_FUNCTION_BLOCK        (0x40000000)


struct _IEC61131_FORMAT
{
    uint32_t Header;

    uint32_t Symbols;

    /* uint32_t Retain; */

    uint32_t ByteCode;


    /*!
        \param Functions

        This member of this data structure is intended to hold a digest hash of the 
        C-based functions defined and included within the library shared between 
        the IEC 61131-3 compiler and virtual machine run-time.  While it is 
        desirable that this digest were shorter, this hash serves the important 
        purpose of ensuring that applications compiled with the IEC 61131-3 
        compiler that utilise C-based functions can be correctly run on the 
        corresponding IEC 61131-3 virtual machine run-time.
    */

    uint8_t Functions[32];
}
__attribute__((__packed__));

typedef struct _IEC61131_FORMAT IEC61131_FORMAT;

#define IEC61131_FORMAT_MAGIC   (0x11310001)


#endif /* __IEC61131_BYTECODE_H */
