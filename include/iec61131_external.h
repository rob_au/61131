#ifndef __IEC61131_EXTERNAL_H
#define __IEC61131_EXTERNAL_H


#include <stdint.h>

#include "iec61131_types.h"


/*
    This header file is intended to specify the type definitions to be employed 
    in defining and implementing C-based functions and function blocks intended 
    to be used with the IEC 61131-3 compiler and virtual machine run-time.  
    
    It should be noted that these definition defers the validation of parameters
    and return value type information until run time - This is a deliberate 
    decision intended to provide maximum flexibility for these functions, 
    particularly for those standard mathematic functions which may support 
    ANY_NUM or ANY_REAL data types.
*/


/*
    The following definitions are intended to be employed in association with 
    the data structures defining IEC 61131-3 function block members, 
    simplifying the process of correctly accessing, in a memory aligned 
    fashion, function block instance members.
*/

#define EXTERNAL_BOOL(x)        uint8_t x __attribute__((aligned(4)))
#define EXTERNAL_SINT(x)        int8_t x __attribute__((aligned(4)))
#define EXTERNAL_INT(x)         int16_t x __attribute__((aligned(4)))
#define EXTERNAL_DINT(x)        int32_t x __attribute__((aligned(4)))
#define EXTERNAL_LINT(x)        int64_t x __attribute__((aligned(4)))
#define EXTERNAL_USINT(x)       uint8_t x __attribute__((aligned(4)))
#define EXTERNAL_UINT(x)        uint16_t x __attribute__((aligned(4)))
#define EXTERNAL_UDINT(x)       uint32_t x __attribute__((aligned(4)))
#define EXTERNAL_ULINT(x)       uint64_t x __attribute__((aligned(4)))
#define EXTERNAL_REAL(x)        float x __attribute__((aligned(4)))
#define EXTERNAL_LREAL(x)       double x __attribute__((aligned(4)))
#define EXTERNAL_BYTE(x)        uint8_t x __attribute__((aligned(4)))
#define EXTERNAL_WORD(x)        uint16_t x __attribute__((aligned(4)))
#define EXTERNAL_DWORD(x)       uint32_t x __attribute__((aligned(4)))
#define EXTERNAL_LWORD(x)       uint64_t x __attribute__((aligned(4)))
#define EXTERNAL_TIME(x)        int32_t x __attribute__((aligned(4)))


extern uint32_t _CYCLETIME;


/*!
    \enum EXTERNAL_PARAMETER

    This enumeration is employed in association with the validation of function 
    block parameters, permitting the specification of parameter type when the 
    parameter is passed to the external library.
*/

typedef enum _EXTERNAL_PARAMETER
{
    PARAMETER_NONE = 0,
    PARAMETER_INPUT,
    PARAMETER_OUTPUT,
    PARAMETER_LOCAL
}
EXTERNAL_PARAMETER;


/*!
    \typedef EXTERNAL_FUNCTION

    This type definition declares the prototype for functions which implement 
    C-based functions made available for execution within the IEC 61131-3 
    virtual machine.
*/

typedef int ( *EXTERNAL_FUNCTION )( IEC61131_DATA * Parameters, unsigned int Count );


/*!
    \typedef EXTERNAL_FUNCTION_BLOCK

    This type definition declares the prototype for functions which implement
    C-based function blocks made available for execution within the IEC 61131-3
    virtual machine.  It should be noted that the functions defined in this 
    fashion are intended to implement concrete actions associated with function
    block execution.
*/

typedef int ( *EXTERNAL_FUNCTION_BLOCK )( char * Memory );


/*!
    \typedef EXTERNAL_FUNCTION_BLOCK_PARAMETER

    This type definition declares the prototype for functions which implement
    parameter validation for C-based function blocks made available for 
    execution within the IEC 61131-3 virtual machine.  Functions defined in
    association with this prototype are intended to return the byte offset
    within the function block memory associated with the given parameter.
*/

typedef int ( *EXTERNAL_FUNCTION_BLOCK_PARAMETER )( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );


typedef struct _EXTERNAL_FUNCTION_BLOCK_DECLARATION
{
    const char * Name;

    size_t Size;

    EXTERNAL_FUNCTION_BLOCK_PARAMETER Parameter;

    EXTERNAL_FUNCTION_BLOCK Pointer;
}
EXTERNAL_FUNCTION_BLOCK_DECLARATION;

typedef struct _EXTERNAL_FUNCTION_DECLARATION
{
    const char * Name;

    EXTERNAL_FUNCTION Pointer;
}
EXTERNAL_FUNCTION_DECLARATION;


extern EXTERNAL_FUNCTION_BLOCK_DECLARATION IEC61131_FUNCTION_BLOCK[];

extern EXTERNAL_FUNCTION_DECLARATION IEC61131_FUNCTION[];

extern size_t IEC61131_FUNCTION_BLOCK_COUNT;

extern size_t IEC61131_FUNCTION_COUNT;

extern void IEC61131_DIGEST( char *pDigest );


#endif
