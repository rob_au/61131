#ifndef __TARGET_H
#define __TARGET_H


#include <stdint.h>


uint8_t target_getB8( char *pPointer );

uint16_t target_getB16( char *pPointer );

uint32_t target_getB32( char *pPointer );

uint64_t target_getB64( char *pPointer );

uint32_t target_getMillisecondCounter( void );

void target_setB8( char *pPointer, uint8_t uValue );

void target_setB16( char *pPointer, uint16_t uValue );

void target_setB32( char *pPointer, uint32_t uValue );

void target_setB64( char *pPointer, uint64_t uValue );


#endif
