#ifndef __DEBUG_H
#define __DEBUG_H


/*!
    \def DEBUG_OPERAND

    This definition specifies whether additional source code is included to
    validate instruction operands within the IEC 61131-3 virtual machine.
    Under normal circumstances, where it is assumed that the IEC 61131-3
    compiler has correctly compiled, linked and assembled the virtual machine
    byte code, such validation is deemed redundant, unnecessarily slowing
    virtual machine operations.  However during development such additional
    operations may be useful to validate compiler operations.
*/

#define DEBUG_OPERAND


#endif /* __DEBUG_H */
