#ifndef __CONFIG_H
#define __CONFIG_H


/*!
    \def CONFIG_RETAIN

    This definition specifies whether the virtual machine will attempt to 
    retain and restore retained symbol table information from a non-volatile 
    store on the target device.  Where defined, this configuration causes the 
    symbol table to be written to the non-volatile store following modification 
    upon the completion of the virtual machine cycle.
*/

#undef CONFIG_RETAIN


/*!
    \def CONFIG_SUPPORT_DATETIME

    This definition specifies whether compilation of the virtual machine will
    include support for date and time data types.  This is manifest in the
    virtual machine environment as implementation of the TIME, DATE, TOD and
    DT IEC 61131-3 data types.
*/

#undef CONFIG_SUPPORT_DATETIME


/*!
    \def CONFIG_SUPPORT_LREAL

    This definition specifies whether compilation of the virtual machine will 
    include support for double number types.  This is manifest in the virtual
    machine environment as implementation of the REAL IEC 61131-3 data type.
*/

#undef CONFIG_SUPPORT_LREAL


/*!
    \def CONFIG_SUPPORT_REAL

    This definition specifies whether compilation of the virtual machine will 
    include support for float number types.  This is manifest in the virtual
    machine environment as implementation of the LREAL IEC 61131-3 data type.
*/

#define CONFIG_SUPPORT_REAL


/*!
    \def CONFIG_SUPPORT_LONGLONG

    This definition specifies whether compilation of the virtual machine will
    include support for 64-bit wide integer (long long) number types.  This is
    manifest in the virtual machine environment as implementation of the 
    LINT, ULINT and LWORD IEC 61131-3 data types.
*/

#undef CONFIG_SUPPORT_LONGLONG


/*!
    \def CONFIG_SUPPORT_STRING
    \def CONFIG_SUPPORT_WSTRING

    This definition specifies whether compilation of the virtual machine will
    include support for string data types.  This is manifest in the virtual
    machine environment as implementation of the STRING and WSTRING IEC
    61131-3 data types.
*/

#undef CONFIG_SUPPORT_STRING
#undef CONFIG_SUPPORT_WSTRING


#endif
