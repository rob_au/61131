#ifndef __VM_H
#define __VM_H


#include <stdint.h>
#include <limits.h>

#include "iec61131_types.h"

#include "handle.h"
#include "io.h"


#define VM_APP_MAX              (1024)

#define VM_CYCLE_TIME_MS        (100)

#define VM_DRIVER_MAX           (2)

#define VM_PARAMETER_MAX        (32)

#define VM_STACK_MAX            (64)

#define VM_SYMBOL_MAX           (128)


extern uint32_t _CYCLETIME;


typedef enum _VM_STATE
{
    VM_STATE_UNKNOWN = 0,
    VM_STATE_DEBUG,
    VM_STATE_RUN,
    VM_STATE_SHUTDOWN
}
VM_STATE;


/*!
    \struct VM_REGISTER
    \brief Virtual machine registers data structure

    This data structure is intended to hold internal registers associated with
    IEC 61131-3 virtual machine operations.  While it has not yet been
    prototyped, it is intended that this data structure will serve as the stack
    frame where deferred execution is encountered.
*/

typedef struct _VM_REGISTER
{
    /*!
        \param PC

        This data structure member is intended to hold the Program Counter (PC)
        register associated with IEC 61131-3 virtual machine operations.  In
        implementation, this program counter is interpreted as an offset into the
        memory allocated for the storage of compiled virtual machine byte code.
    */

    uint32_t PC;

    /*!
        \param BC

        This data structure member is intended to hold the current byte code (BC)
        under execution by the IEC 61131-3 virtual machine.
    */

    uint32_t BC;

    /*!
        \param CR

        This data structure member is intended to hold the Current Register (CR)
        associated with the current frame of IEC 61131-3 virtual machine operations.
    */

    IEC61131_DATA CR;

    /*
        \param ER

        This data structure member is intended to hold the Exception Register (ER)
        associated with the current frame of IEC 61131-3 virtual machine operations.
        It should be noted however that exception codes have not yet been defined
        in association with this virtual machine register.
    */

    uint32_t ER;

    /*
        \param OF

        This data structure member is intended to hold the symbol table offset
        (OF) from which operand pointer operations should be referenced.  This
        is primarily employed with respect to operations involving function
        block instances.
    */

    uint32_t OF;
}
VM_REGISTER;


/*!
    \struct VM

    This data structure serves as the primary contextual data structure for IEC
    61131-3 virtual machine operations.
*/

typedef struct _VM
{
    const char *Name;

    char Directory[ PATH_MAX ];

    unsigned int Align;

    unsigned char Debug;

    unsigned char Finish;

    unsigned char GDB;

    unsigned char Step;

    VM_STATE State;

    struct
    {
        IEC61131_DATA Value[ VM_PARAMETER_MAX ];

        unsigned int Count;
    }
    Parameters;

    /*!
        \param Stack

        This data structure member is intended to hold copies of virtual machine 
        control registers when performing nested operations such as deferred 
        execution evaluation and program organisation unit (POU) calls.
    */

    struct
    {
        VM_REGISTER Frame[ VM_STACK_MAX ];

        unsigned int Count;
    }
    Stack;

    VM_REGISTER Register;

    unsigned int Cycle;

    unsigned int CycleCount;

    unsigned int CycleTime;

    unsigned int LastCycleTime;

    unsigned int Overflow;

    unsigned int Start;

    char Symbol[ VM_SYMBOL_MAX ];

    /*!
        \param App

        This data structure member is intended to hold the compiled byte code for 
        execution within the IEC 61131-3 virtual machine.  Given the early stage of 
        current development, the nature of this data structure member is likely to 
        change as development progresses.
    */

    char App[ VM_APP_MAX ];

    unsigned int Length;

    HANDLE Handles;

    IO_CONTEXT IO;

    void * Drivers[ VM_DRIVER_MAX ];
}
VM;


void vm_executeCycle( VM *pVm );

unsigned int vm_getCycleTime( VM *pVm );

VM_STATE vm_getKernelState( VM *pVm );

void vm_finish( VM *pVm );

void vm_initialise( VM *pVm );

int vm_parseArguments( VM *pVm, int nArgc, char **pArgv );

void vm_start( VM *pVm );


#endif
