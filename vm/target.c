#include <stdint.h>
#include <time.h>
#include <sys/time.h>

#include "target.h"


/*!
    \fn uint8_t target_getB8( char *pPointer )
    \brief Retrieve 8-bit unsigned integer value from buffer
    \param pPointer Pointer to buffer location
    \return Returns 8-bit unsigned integer value
*/

uint8_t
target_getB8( char *pPointer )
{
    return ( uint8_t ) *pPointer;
}


/*!
    \fn uint16_t target_getB16( char *pPointer )
    \brief Retrieve 16-bit unsigned integer value from buffer
    \param pPointer Pointer to buffer location
    \return Returns 16-bit unsigned integer value
*/

uint16_t
target_getB16( char *pPointer )
{
    uint16_t uValue;


    uValue = ( ( ( ( uint16_t ) *( pPointer + 0 ) ) << 8 ) & 0xff00 );
    uValue |= ( ( ( uint16_t ) *( pPointer + 1 ) ) & 0x00ff );
    return uValue;
}


/*!
    \fn uint32_t target_getB32( char *pPointer )
    \brief Retrieve 32-bit unsigned integer value from buffer
    \param pPointer Pointer to buffer location
    \return Returns 32-bit unsigned integer value
*/

uint32_t
target_getB32( char *pPointer )
{
    uint32_t uValue;


    uValue = ( ( ( ( uint32_t ) *( pPointer + 0 ) << 24 ) ) & 0xff000000 );
    uValue |= ( ( ( ( uint32_t ) *( pPointer + 1 ) << 16 ) ) & 0x00ff0000 );
    uValue |= ( ( ( ( uint32_t ) *( pPointer + 2 ) << 8 ) ) & 0x0000ff00 );
    uValue |= ( ( ( uint32_t ) *( pPointer + 3 ) ) & 0x000000ff );
    return uValue;
}



uint32_t
target_getMillisecondCounter( void )
{
    struct timespec stTp;


    ( void ) clock_gettime( CLOCK_MONOTONIC, &stTp );
    return ( uint32_t ) ( ( stTp.tv_sec * 1000 ) + ( stTp.tv_nsec / 1000000 ) );
}
