#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "iec61131_bytecode.h"
#include "iec61131_external.h"

#include "base.h"
#include "drivers.h"
#include "gdb.h"
#include "hex.h"
#include "io.h"
#include "log.h"
#include "op.h"
#include "target.h"
#include "vm.h"


#define MIN( a, b )             ( ( (a) < (b) ) ? (a) : (b) )


/*
    The following macro definition is employed to identify directory separator
    characters within path strings in a system-independent manner.  This
    functionality is used in association with the parsing of command line
    arguments passed to the vm_parseArguments function.
*/

#define IS_DIR_SEPARATOR( c )   ( ( (c) == '/' ) || ( (c) == '\\' ) )


uint32_t _CYCLETIME = 0;


static int
_vm_validateHeader( VM *pVM )
{
    IEC61131_FORMAT *pFormat;
    unsigned char uDigest[16];


    if( pVM->Length < sizeof( IEC61131_FORMAT ) )
    {
        return -EINVAL;
    }

    pFormat = ( IEC61131_FORMAT * ) pVM->App;
    if( pFormat->Header != IEC61131_FORMAT_MAGIC )
    {
        return -EINVAL;
    }

    IEC61131_DIGEST( ( char * ) &uDigest );
    if( memcmp( pFormat->Functions, uDigest, 16 ) != 0 )
    {
        LOG_ERR( "Mismatch between compiler and virtual machine run-time function hashes" );

        return -EINVAL;
    }

    return 0;
}


void
vm_executeCycle( VM *pVM )
{
    IEC61131_FORMAT *pFormat;


    if( pVM->Finish != 0 )
    {
        pFormat = ( IEC61131_FORMAT * ) pVM->App;
        pVM->Register.PC = pFormat->ByteCode;

        ( void ) memset( &pVM->Register.CR, 0, sizeof( IEC61131_DATA ) );
        pVM->Register.CR.Type = TYPE_NONE;
        pVM->Parameters.Count = 0;
        pVM->Stack.Count = 0;
        ++pVM->Cycle;

        pVM->Start = target_getMillisecondCounter();
        pVM->Finish = 0;

        _CYCLETIME = pVM->Start;

        io_readInputs( &pVM->IO );
    }

    op_executeByteCode( pVM );

    if( pVM->Finish != 0 )
    {
        io_writeOutputs( &pVM->IO );

        pVM->LastCycleTime = target_getMillisecondCounter() - pVM->Start;
        if( pVM->LastCycleTime > pVM->CycleTime )
        {
            ++pVM->Overflow;
        }

        if( pVM->Debug != 0 )
        {
            LOG_DEBUG( "Timing results for cycle %u (%u.%03u%s)",
                    pVM->Cycle,
                    pVM->LastCycleTime / 1000,
                    pVM->LastCycleTime % 1000,
                    ( pVM->LastCycleTime > pVM->CycleTime ) ? "!" : "" );
        }
    }

    if( ( pVM->CycleCount > 0 ) &&
            ( pVM->Cycle >= pVM->CycleCount ) )
    {
        LOG_INFO( "IEC 61131-3 VM Shutdown after %u cycles", pVM->Cycle );
        pVM->State = VM_STATE_SHUTDOWN;
    }
}


unsigned int
vm_getCycleTime( VM *pVM )
{
    return pVM->CycleTime;
}


VM_STATE
vm_getKernelState( VM *pVM )
{
    return pVM->State;
}


void
vm_finish( VM *pVM )
{
    BASE_UNUSED( pVM );
}


void
vm_initialise( VM *pVM )
{
    DRIVER_DECLARATION *pDriver;
    unsigned int uIndex;


    log_initialise();

    ( void ) memset( &pVM->Stack, 0, sizeof( pVM->Stack ) );
    ( void ) memset( &pVM->Drivers, 0, sizeof( pVM->Drivers ) );

    pVM->Align = 4;
    pVM->Debug = 0;
    pVM->GDB = 0;
    pVM->Cycle = 0;
    pVM->CycleCount = 0;
    pVM->CycleTime = VM_CYCLE_TIME_MS;
    pVM->Overflow = 0;

    handle_initialise( &pVM->Handles );
    io_initialise( &pVM->IO );


    /*
        The following code will iterate through all drivers defined in the 
        DRIVER_TABLE table, calling the initialisation function of each driver in 
        turn.
    */

    for( uIndex = 0; uIndex < DRIVER_TABLE_COUNT; ++uIndex )
    {
        if( uIndex >= VM_DRIVER_MAX )
        {
            break;
        }

        pDriver = &DRIVER_TABLE[ uIndex ];
        if( pDriver->Pointer != NULL )
        {
            LOG_INFO( "Loading IEC 61131-3 VM driver \"%s\"", pDriver->Name );
            pVM->Drivers[ uIndex ] = pDriver->Pointer( pVM );

            if( pVM->Drivers[ uIndex ] == NULL )
            {
                LOG_ERR( "Error loading IEC 61131-3 VM driver \"%s\"", pDriver->Name );
            }
        }
    }
}


int
vm_parseArguments( VM *pVM, int nArgc, char **pArgv )
{
    struct stat stStat;
    int nLength, nOption;
    char *pArg, *pChar;
    char szFilename[ PATH_MAX ];


    pVM->Name = pArgv[0] + strlen( pArgv[0] );
    while( ( pVM->Name != pArgv[0] ) &&
            ( ! IS_DIR_SEPARATOR( pVM->Name[-1] ) ) )
    {
        --pVM->Name;
    }

    ( void ) snprintf( pVM->Directory, sizeof( pVM->Directory ), "." );

    opterr = 0;
    while( ( nOption = getopt( nArgc, pArgv, "c:d:gt:v" ) ) != -1 )
    {
        switch( nOption )
        {
            case 'c':

                pVM->CycleCount = strtoul( optarg, ( char ** ) NULL, 10 );
                break;

            case 'd':

                ( void ) strncpy( pVM->Directory, optarg, MIN( sizeof( pVM->Directory ), strlen( optarg ) ) );

                pChar = pVM->Directory + strlen( pVM->Directory );
                while( ( pChar != pVM->Directory ) &&
                        ( IS_DIR_SEPARATOR( pChar[-1] ) ) )
                {
                    *pChar = '\0';
                    --pChar;
                }
                break;

            case 'g':

                pVM->GDB = 1;
                break;

            case 't':

                pVM->CycleTime = strtoul( optarg, ( char ** ) NULL, 10 );
                break;

            case 'v':

                pVM->Debug = 1;
                break;

            default:

                fprintf( stderr, "%s: error: bad command line option '-%c'\n",
                        pVM->Name,
                        optopt );
                return -EINVAL;
        }
    }
    if( optind >= nArgc )
    {
        return /* -ENOENT */ 0;
    }


    /*
        The first argument passed to the virtual machine run-time executable may be 
        interpreted as either a direct path to an object file (supporting an 
        execution convention and invocation argument structure employed during 
        development) or a project name (for which there may be multiple files in 
        place specifying different elements of the IEC 61131-3 project such as 
        object file, symbol definition file, etc).
    */

    pArg = pArgv[ optind ];
    if( stat( pArg, &stStat ) < 0 )
    {
        ( void ) snprintf( szFilename, sizeof( szFilename ), "%s/%s.hex",
                pVM->Directory,
                pArgv[ optind ] );
    }
    else
    {
        ( void ) snprintf( szFilename, sizeof( szFilename ), "%s", pArg );
    }

    if( ( nLength = hex_openFile( szFilename, pVM->App ) ) <= 0 )
    {
        fprintf( stderr, "%s: error: invalid object file - '%s'\n",
                pVM->Name,
                szFilename );
        return -EINVAL;
    }
    pVM->Length = nLength;

    if( _vm_validateHeader( pVM ) != 0 )
    {
        fprintf( stderr, "%s: error: invalid object file - '%s'\n",
                pVM->Name,
                szFilename );
        return -EINVAL;
    }

    LOG_DEBUG( "Successfully read object file '%s', Length %u",
            szFilename,
            nLength );

    return 0;
}


void
vm_start( VM *pVM )
{
    IEC61131_FORMAT *pFormat;


    /*
        The following code will copy initial values for symbol table entries from 
        the application space into symbol space.
    */

    pFormat = ( IEC61131_FORMAT * ) pVM->App;
    if( ( pFormat->Symbols > 0 ) &&
            ( pFormat->ByteCode > pFormat->Symbols ) )
    {
        /* assert( pFormat->Symbols < sizeof( pVM->App ) ); */
        /* assert( ( pFormat->ByteCode - pFormat->Symbols ) < ( sizeof( pVM->App ) - pFormat->Symbols ) ); */
        /* assert( ( pFormat->ByteCode - pFormat->Symbols ) < sizeof( pVM->Source ) ); */
        ( void ) memcpy( pVM->Symbol, &pVM->App[ pFormat->Symbols ], pFormat->ByteCode - pFormat->Symbols );
    }

    pVM->Register.PC = pFormat->ByteCode;
    pVM->Finish = 1;

    if( pVM->GDB != 0 )
    {
        gdb_initialise( pVM );
    }

    LOG_INFO( "IEC 61131-3 VM Started" );
}

