#include "drivers.h"

#include "example.h"
#include "modbus.h"
#include "piface.h"


DRIVER_DECLARATION DRIVER_TABLE[] = 
{
//    {   "example",              example_initialise      },
    {   "modbus",               modbus_initialise       },

    /* 
        PiFace Digital - http://www.piface.org.uk/products/piface_digital/

        The pfio driver implements support for reading and writing digital I/O 
        associated with the PiFace Digital hardware module.
    */

//    {   "pfio",                 piface_initialise       },
};


size_t DRIVER_TABLE_COUNT = ( sizeof( DRIVER_TABLE ) / sizeof( DRIVER_DECLARATION ) );



