#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/mman.h>
#include <sys/stat.h>

#include "hex.h"


static inline uint8_t _hex_parseHex4( unsigned char uByte );

static uint8_t _hex_parseHex8( char *pData );

static uint16_t _hex_parseHex16( char *pData );

static int _hex_parseRecord( char *pData, char *pPointer, unsigned long *pLength );


static inline uint8_t
_hex_parseHex4( unsigned char uByte )
{
    if( ( uByte >= 0x61 ) &&            /* A .. F */
            ( uByte <= 0x66 ) )
    {
        return uByte - 0x61 + 10;
    }
    else if( ( uByte >= 0x41 ) &&       /* a .. f */
            ( uByte <= 0x46 ) )
    {
        return uByte - 0x41 + 10;
    }
    else if( ( uByte >= 0x30 ) &&       /* 0 .. 9 */
            ( uByte <= 0x39 ) )
    {
        return uByte - 0x30;
    }
    else
    {
        return 0;
    }
}


static uint8_t
_hex_parseHex8( char *pData )
{
    return ( _hex_parseHex4( pData[0] ) << 4 ) +
            _hex_parseHex4( pData[1] );
}


static uint16_t
_hex_parseHex16( char *pData )
{
    return ( _hex_parseHex4( pData[0] ) << 12 ) +
            ( _hex_parseHex4( pData[1] ) << 8 ) +
            ( _hex_parseHex4( pData[2] ) << 4 ) +
            _hex_parseHex4( pData[3] );
}

static int
_hex_parseRecord( char *pData, char *pPointer, unsigned long *pLength )
{
    unsigned int uChkSum, uIndex, uLength, uOffset, uRecLen, uRecTyp;
    unsigned char uByte, uCheck;


    if( *pData != ':' )                                         /* RECORD MARK */
    {
        errno = -EINVAL;
        return errno;
    }

    uRecLen = _hex_parseHex8( pData + 1 );                  /* RECLEN */
    uLength = ( uRecLen * 2 );
    uOffset = _hex_parseHex16( pData + 3 );                 /* LOAD OFFSET */
    uRecTyp = _hex_parseHex8( pData + 7 );                  /* RECTYP */
    uChkSum = _hex_parseHex8( pData + 9 + uLength );        /* CHKSUM */

    if( ( ( pData[ uLength + 11 ] != '\n' ) ||
            ( pData[ uLength + 12 ] != '\r' ) ) &&
            ( pData[ uLength + 11 ] != '\n' ) )
    {
        errno = -EINVAL;
        return errno;
    }

    uCheck = uRecLen +
            ( ( uOffset >> 8 ) & 0xff ) +
            ( uOffset & 0xff ) +
            uRecTyp +
            uChkSum;

    for( uIndex = 0; uIndex < uRecLen; ++uIndex )
    {
        if( ( pData[ ( uIndex * 2 ) + 9 ] == '\n' ) ||
                ( pData[ ( uIndex * 2 ) + 9 ] == '\r' ) )
        {
            errno = -EINVAL;
            return errno;
        }

        uByte = _hex_parseHex8( pData + 9 + ( uIndex * 2 ) );
        uCheck += uByte;

        if( pPointer != NULL )
        {
            *pPointer++ = uByte;
            *pLength = ( *pLength + 1 );
        }
    }

    if( ( uCheck & 0xff ) != 0 )
    {
        errno = -EINVAL;
        return errno;
    }

    return ( uLength + 10 );
}


int
hex_openFile( const char *pFilename, char *pPointer )
{
    struct stat stStat;
    unsigned long uBytes, uLength;
    char *pChar, *pData, *pStart;
    int nFd, nResult;


    if( ( nFd = open( pFilename, O_RDONLY ) ) < 0 )
    {
        return -errno;
    }

    if( fstat( nFd, &stStat ) != 0 )
    {
        close( nFd );

        return -errno;
    }
    uLength = stStat.st_size;

    if( ( pChar = ( char * ) mmap( NULL, uLength, PROT_READ, MAP_PRIVATE, nFd, 0 ) ) == ( void * ) -1 )
    {
        close( nFd );

        return -errno;
    }

    for( errno = 0,
            pData = pChar,
            pStart = pPointer,
            uBytes = 0;; )
    {
        while( ( *pData != ':' ) &&
                ( *pData != 0x00 ) )
        {
            ++pData;
        }
        if( *pData == 0x00 )
        {
            break;
        }

        pPointer = pStart + uBytes;
        if( ( nResult = _hex_parseRecord( pData, pPointer, &uBytes ) ) < 0 )
        {
            return nResult;
        }
        pData += nResult;
    }

    munmap( ( void * ) pChar, uLength );
    close( nFd );

    return uBytes;
}
