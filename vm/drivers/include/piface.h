#ifndef __PIFACE_H
#define __PIFACE_H


#include <stdint.h>

#include "io.h"


//  PiFace Digital SPI configuration
#define PIFACE_SPI_BUS                  0
#define PIFACE_SPI_CHIP_SELECT          0

#define PIFACE_SPI_BITS                 8
#define PIFACE_SPI_DELAY                0
#define PIFACE_SPI_MODE                 0
#define PIFACE_SPI_SPEED                10000000

//  PiFace Digital commands
#define PIFACE_CMD_WRITE                0
#define PIFACE_CMD_READ                 1

//  PiFace Digital registers
#define PIFACE_REG_IODIRA               0x00
#define PIFACE_REG_IODIRB               0x01
#define PIFACE_REG_IPOLA                0x02
#define PIFACE_REG_IPOLB                0x03
#define PIFACE_REG_GPINTENA             0x04
#define PIFACE_REG_GPINTENB             0x05
#define PIFACE_REG_DEFVALA              0x06
#define PIFACE_REG_DEFVALB              0x07
#define PIFACE_REG_INTCONA              0x08
#define PIFACE_REG_INTCONB              0x09
#define PIFACE_REG_IOCON                0x0a
#define PIFACE_REG_GPPUA                0x0c
#define PIFACE_REG_GPPUB                0x0d
#define PIFACE_REG_INTFA                0x0e
#define PIFACE_REG_INTFB                0x0f
#define PIFACE_REG_INTCAPA              0x10
#define PIFACE_REG_INTCAPB              0x11
#define PIFACE_REG_GPIOA                0x12
#define PIFACE_REG_OUTPUT               0x12
#define PIFACE_REG_GPIOB                0x13
#define PIFACE_REG_INPUT                0x13
#define PIFACE_REG_OLATA                0x14
#define PIFACE_REG_OLATB                0x15

//  PiFace Digital I/O configuration
#define PIFACE_IO_BANK_OFF              0x00
#define PIFACE_IO_BANK_ON               0x80
#define PIFACE_IO_INT_MIRROR_ON         0x40
#define PIFACE_IO_INT_MIRROR_OFF        0x00
#define PIFACE_IO_SEQOP_OFF             0x20
#define PIFACE_IO_SEQOP_ON              0x00
#define PIFACE_IO_DISSLW_ON             0x10
#define PIFACE_IO_DISSLW_OFF            0x00
#define PIFACE_IO_HAEN_ON               0x08
#define PIFACE_IO_HAEN_OFF              0x00
#define PIFACE_IO_ODR_ON                0x04
#define PIFACE_IO_ODR_OFF               0x00
#define PIFACE_IO_INTPOL_HIGH           0x02
#define PIFACE_IO_INTPOL_LOW            0x00


typedef struct _PIFACE_CONTEXT
{
    IO_DRIVER Driver;

    int Handle;
}
PIFACE_CONTEXT;


void * piface_initialise( void * pContext );

void piface_destroy( void *pPointer );

void piface_readInputs( char *pData, void *pContext );

void piface_writeOutputs( char *pData, void *pContext );


#endif
