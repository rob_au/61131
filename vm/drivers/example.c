#include <stdio.h>
#include <string.h>

#include "io.h"
#include "log.h"

#include "example.h"


#define __UNUSED( __Object )    ( ( void ) ( ( __Object ) == ( __Object ) ) )


static EXAMPLE_CONTEXT stExample;


/*!
    \fn void * example_initialise( void )
    \brief Register example I/O driver
    \param pContext Contextual void-pointer provided to driver initialisation function
    \return Returns pointer to contextual data structure on success, NULL on error
*/

void *
example_initialise( void *pContext )
{
    IO_DRIVER *pDriver;


    ( void ) memset( &stExample, 0, sizeof( stExample ) );
    stExample.Inputs = (0 << 0) | (1 << 1);
    stExample.Outputs = 0;

    pDriver = ( IO_DRIVER * ) &stExample;
    /* pDriver->Initialise = example_initialise; */
    pDriver->Destroy = example_destroy;
    pDriver->Read = example_readInputs;
    pDriver->Write = example_writeOutputs;

    if( ( io_registerBlock( pContext, "%IX2", &stExample, pDriver ) != 0 ) ||
            ( io_registerBlock( pContext, "%QB", &stExample, pDriver ) != 0 ) )
    {
        return NULL;
    }

    return &stExample;
}


void
example_destroy( void *pPointer )
{
    __UNUSED( pPointer );
}


void
example_readInputs( char *pData, void *pContext )
{
    EXAMPLE_CONTEXT *pExample;


    pExample = ( EXAMPLE_CONTEXT * ) pContext;
    pData[0] = pExample->Inputs;
}


void
example_writeOutputs( char *pData, void *pContext )
{
    EXAMPLE_CONTEXT *pExample;
#ifdef LOGDEBUG
    uint8_t uOutputs, uValue;
    unsigned int uIndex;
#endif


    pExample = ( EXAMPLE_CONTEXT * ) pContext;
#ifdef LOGDEBUG
    uOutputs = ( uint8_t ) pData[0];
    uValue = ( uOutputs ^ pExample->Outputs );
    if( uValue != 0 )
    {
        for( uIndex = 0; uIndex < ( sizeof( pExample->Outputs ) << 3 ); ++uIndex )
        {
            if( ( uValue & ( 1 << uIndex ) ) != 0 )
            {
                if( ( uOutputs & ( 1 << uIndex ) ) != 0 )
                {
                    LOG_DEBUG( "Digital output %u changed from OFF to ON", uIndex );
                }
                else
                {
                    LOG_DEBUG( "Digital output %u changed from ON to OFF", uIndex );
                }
            }
        }
        pExample->Outputs = uOutputs;
    }
#else
    pExample->Outputs = ( uint8_t ) pData[0];
#endif
}

