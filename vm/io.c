#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "base.h"
#include "io.h"
#include "log.h"
#include "vm.h"


static int _io_getBlockSize( const char *pDefinition );

static IO_TYPE _io_getBlockType( const char *pDefinition );


/*!
    \fn static int _io_getBlockSize( const char *pDefinition )
    \brief Validate an block definition provided by I/O driver registration
    \param pDefinition Pointer to I/O block definition
    \returns Returns block size in bits on success, negative value on error

    This function is intended to validate the direct variable representation of an 
    I/O block definition provided by a driver on registration and return the 
    corresponding block size in bits.  The value of zero will be returned for 
    undefined or interdeterminate I/O block definition.  Return values less than 
    zero are indicative of an invalid direct variable representation.

    For definition of directly represented variable formats, see Table 15 -
    Location and size prefix features for directly represented variables in
    IEC 61131-3, 2nd Ed. Programmable Controllers - Programming Languages

    Direct representation of a single-element variable shall be provided by a 
    special symbol formed by the concatenation of the percent sign "%" (character 
    code 037 decimal in Table 1 - Row 00 of ISO/IEC 10646), a location prefix and a 
    size prefix from Table 15, and one or more unsigned integers, separated by 
    periods (.).
*/

static int
_io_getBlockSize( const char *pDefinition )
{
    unsigned int uSize, uValue;
    char *pChar;
    int nIndex;


    for( pChar = ( char * ) pDefinition;; )
    {
        if( pChar == NULL )
        {
            return -EINVAL;
        }

        switch( *pChar )
        {
            case 'I':
            case 'M':
            case 'Q':

                break;

            case '%':

                if( pChar == pDefinition )
                {
                    ++pChar;
                    continue;
                }
                /* break; */

            default:

                return -EINVAL;
        }

        ++pChar;
        if( *pChar == '\0' )
        {
            return -EINVAL;
        }

        break;
    }

    uSize = 1;
    switch( *pChar )
    {
        case '*':

            return 0;

        case 'X':
        case 'B':
        case 'W':
        case 'D':
        case 'L':

            switch( *pChar )
            {
                case 'X':   uSize = 1; break;
                case 'B':   uSize = 8; break;
                case 'W':   uSize = 16; break;
                case 'D':   uSize = 32; break;
                case 'L':   uSize = 64; break;
            }

            ++pChar;
            if( *pChar == '\0' )
            {
                return uSize;
            }
            break;

        default:

            break;
    }

    for( nIndex = 0;; ++nIndex )
    {
        uValue = strtoul( pChar, &pChar, 10 );
        switch( nIndex )
        {
            case 0:

                uSize *= uValue;
                break;

            default:

                break;
        }

        switch( *pChar )
        {
            case '\0':

                return uSize;

            case '.':

                ++pChar;
                if( *pChar == '\0' )
                {
                    return -EINVAL;
                }
                break;

            default:

                return -EINVAL;
        }
    }
}


static IO_TYPE 
_io_getBlockType( const char *pDefinition )
{
    char *pChar;


    if( pDefinition == NULL )
    {
        return IO_TYPE_NONE;
    }

    for( pChar = ( char * ) pDefinition;; )
    {
        switch( *pChar )
        {
            case 'I':   return IO_TYPE_INPUT;
            case 'M':   return IO_TYPE_MEMORY;
            case 'Q':   return IO_TYPE_OUTPUT;
            case '%':

                if( pChar == pDefinition )
                {
                    ++pChar;
                    continue;
                }
                /* break; */

            default:

                return IO_TYPE_NONE;
        }
    }
}


int
io_initialise( IO_CONTEXT *pIO )
{
    ( void ) memset( pIO, 0, sizeof( *pIO ) );

    pIO->Blocks = 0;

    return 0;
}


void
io_readInputs( IO_CONTEXT *pIO )
{
    IO_BLOCK *pBlock;
    IO_DRIVER *pDriver;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pIO->Blocks; ++uIndex )
    {
        pBlock = &pIO->Block[ uIndex ];
        switch( pBlock->Type )
        {
            case IO_TYPE_INPUT:
            case IO_TYPE_MEMORY:

                break;

            default:

                continue;
        }

        pDriver = pBlock->Driver;
        pDriver->Read( pBlock->Location, ( void * ) pBlock->Context );
    }
}


/*!
    \fn int io_registerBlock( void *pPointer, const char *pDefinition, const void *pContext, IO_DRIVER *pDriver )
    \brief Register driver for physical I/O interface
    \param pPointer Pointer to IEC 61131-3 virtual machine contextual data-structure
    \param pDefinition Pointer to character string specifying directly-represented physical I/O
    \param pContext Pointer to driver-supplied contextual data structure
    \param pDriver Pointer to I/O driver data structure
    \returns Returns zero on success, non-zero on error
*/

int
io_registerBlock( void *pPointer, const char *pDefinition, const void *pContext, IO_DRIVER *pDriver )
{
    IO_BLOCK *pBlock;
    IO_CONTEXT *pIO;
    IO_MEMORY *pMemory;
    VM *pVM;
    int nBytes, nSize;
    char *pString;


    pVM = ( VM * ) pPointer;
    /* assert( pVM != NULL ); */
    pIO = ( IO_CONTEXT * ) &pVM->IO;

    if( ( pDefinition == NULL ) ||
            ( pDriver == NULL ) )
    {
        return -EINVAL;
    }

    if( pIO->Blocks >= IO_BLOCK_MAX )
    {
        return -ENOMEM;
    }
    pBlock = &pIO->Block[ pIO->Blocks ];
    pBlock->Driver = pDriver;
    pBlock->Context = pContext;

    if( ( nSize = _io_getBlockSize( pDefinition ) ) < 0 )
    {
        return -EINVAL;
    }
    nBytes = ( ( nSize / 8 ) + ( ( ( nSize % 8 ) > 0 ) ? 1 : 0 ) );
    pBlock->Width = ( unsigned int ) nSize;
    pBlock->Type = _io_getBlockType( pDefinition );
    switch( pBlock->Type )
    {
        case IO_TYPE_INPUT:

            pMemory = &pIO->Inputs;
            pString = "Input";
            break;

        case IO_TYPE_OUTPUT:

            pMemory = &pIO->Outputs;
            pString = "Output";
            break;

        case IO_TYPE_MEMORY:

            pMemory = &pIO->Memory;
            pString = "Memory";
            break;

        case IO_TYPE_NONE:
        default:

            return -EINVAL;
    }

    pBlock->Location = &pMemory->Memory[ pMemory->Length ];
    pMemory->Length += ( ( ( nBytes / pVM->Align ) * pVM->Align ) +
            ( ( ( nBytes % pVM->Align ) > 0 ) ?
                    pVM->Align :
                    0 ) );

    ++pIO->Blocks;

    LOG_DEBUG( "Registered I/O block %s (%s, %u bits)",
            pDefinition,
            pString,
            nSize );

    return 0;
}


void
io_writeOutputs( IO_CONTEXT *pIO )
{
    IO_BLOCK *pBlock;
    IO_DRIVER *pDriver;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pIO->Blocks; ++uIndex )
    {
        pBlock = &pIO->Block[ uIndex ];
        switch( pBlock->Type )
        {
            case IO_TYPE_OUTPUT:
            case IO_TYPE_MEMORY:

                break;

            default:

                continue;
        }

        pDriver = pBlock->Driver;
        pDriver->Write( pBlock->Location, ( void * ) pBlock->Context );
    }
}

