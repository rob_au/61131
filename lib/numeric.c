#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include "config.h"
#include "numeric.h"


/*!
    \def _TYPE_CHECK

    This macro declaration is intended to be employed within C functions 
    implementing operations associated with IEC 61131-3 functions and 
    conforming to the IEC61131_FUNCTION_POINTER prototype.  In usage this macro
    is intended to simplify the process of parameter type checking, allowing 
    the intended number and order of parameter types to be specified and then
    validated by the _iec61131_parameterCheck static function.
*/

#define _TYPE_CHECK( c, ... )   if( ( Count != (c) ) || ( _iec61131_parameterCheck( Parameters, __VA_ARGS__ ) != 0 ) ) return -EINVAL;

#define _TYPE_COUNT( c )        if( Count != (c) ) return -EINVAL;

#define _TYPE_COUNT_MIN( c )    if( Count < (c) ) return -EINVAL;



/*!
    \fn static int _iec61131_parameterCheck( IEC61131_DATA * Parameters, ... )
    \brief Validate the data type of parameters passed to a function
    \param Parameters Pointer to array of IEC 61131-3 data structures containing parameter information
    \returns Returns zero on success, non-zero on error

    This function can be employed to validate the order and data type of 
    parameters passed to a function.  For ease of use, this function may be 
    called through use of the _TYPE_CHECK variadic macro.  
*/

/* static */ int
_iec61131_parameterCheck( IEC61131_DATA * Parameters, ... )
{
    IEC61131_DATA *pValue;
    IEC61131_TYPE eType;
    va_list stAp;
    unsigned int uIndex;
    int nResult;


    va_start( stAp, Parameters );
    for( uIndex = 0, nResult = 0;; ++uIndex )
    {
        eType = va_arg( stAp, IEC61131_TYPE );
        if( eType == TYPE_NONE )
        {
            nResult = 0;
            break;
        }
        
        pValue = &Parameters[ uIndex ];
        if( ( pValue == NULL ) ||
                ( pValue->Type != eType ) )
        {
            nResult = -EINVAL;
            break;
        }
    }
    va_end( stAp );

    return nResult;
}


int
numeric_ABS( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
        case TYPE_SINT:

            pValue->Value.S8 = abs( pValue->Value.S8 );
            break;

        case TYPE_INT:

            pValue->Value.S16 = abs( pValue->Value.S16 );
            break;

        case TYPE_DINT:

            pValue->Value.S32 = abs( pValue->Value.S32 );
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:

            pValue->Value.S64 = abs( pValue->Value.S64 );
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = fabs( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = fabs( pValue->Value.Double );
            break;

#endif
        case TYPE_USINT:
        case TYPE_UINT:
        case TYPE_UDINT:
#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_ULINT:
#endif

            return 0;

        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_ACOS( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = acosf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = acos( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_ASIN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = asinf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = asin( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_ATAN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = atanf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = atan( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_COS( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = cosf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = cos( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_EXP( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = expf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = exp( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_LN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            if( pValue->Value.Float < 0.0 )
            {
                return -EINVAL;
            }
            pValue->Value.Float = logf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            if( pValue->Value.Double < 0.0 )
            {
                return -EINVAL;
            }
            pValue->Value.Double = log( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_LOG( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = log10f( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = log10( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_SIN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = sinf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = sin( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
numeric_SQRT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = sqrtf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = sqrt( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
    return -EINVAL;
}


int
numeric_TAN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue;


    _TYPE_COUNT( 1 );

    pValue = &Parameters[0];
    switch( pValue->Type )
    {
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            pValue->Value.Float = tanf( pValue->Value.Float );
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            pValue->Value.Double = tan( pValue->Value.Double );
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


