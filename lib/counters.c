#include <string.h>
#include <errno.h>

#include "config.h"
#include "counters.h"


int 
counters_executeCTD( char * Memory )
{
    CTD *pInstance;


    pInstance = ( CTD * ) Memory;
    if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else if( ( pInstance->CD != 0 ) &&
            ( pInstance->CV > INT16_MIN ) )
    {
        pInstance -= 1;
    }
    pInstance->Q = ( pInstance->CV <= 0 );

    return 0;
}


int 
counters_executeCTD_DINT( char * Memory )
{
    CTD_DINT *pInstance;


    pInstance = ( CTD_DINT * ) Memory;
    if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else if( ( pInstance->CD != 0 ) &&
            ( pInstance->CV > INT32_MIN ) )
    {
        pInstance -= 1;
    }
    pInstance->Q = ( pInstance->CV <= 0 );

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTD_LINT( char * Memory )
{
    CTD_LINT *pInstance;


    pInstance = ( CTD_LINT * ) Memory;
    if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else if( ( pInstance->CD != 0 ) &&
            ( pInstance->CV > INT64_MIN ) )
    {
        pInstance -= 1;
    }
    pInstance->Q = ( pInstance->CV <= 0 );

    return 0;
}

#endif

int 
counters_executeCTD_UDINT( char * Memory )
{
    CTD_UDINT *pInstance;


    pInstance = ( CTD_UDINT * ) Memory;
    if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else if( ( pInstance->CD != 0 ) &&
            ( pInstance->CV > 0 ) )
    {
        pInstance -= 1;
    }
    pInstance->Q = ( pInstance->CV <= 0 );

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTD_ULINT( char * Memory )
{
    CTD_ULINT *pInstance;


    pInstance = ( CTD_ULINT * ) Memory;
    if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else if( ( pInstance->CD != 0 ) &&
            ( pInstance->CV > 0 ) )
    {
        pInstance -= 1;
    }
    pInstance->Q = ( pInstance->CV <= 0 );

    return -EFAULT;
}

#endif

int 
counters_executeCTU( char * Memory )
{
    CTU *pInstance;


    pInstance = ( CTU * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( ( pInstance->CU != 0 ) &&
            ( pInstance->CV < INT16_MAX ) )
    {
        pInstance->CV += 1;
    }
    pInstance->Q = ( pInstance->CV >= pInstance->PV );

    return 0;
}


int 
counters_executeCTU_DINT( char * Memory )
{
    CTU_DINT *pInstance;


    pInstance = ( CTU_DINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( ( pInstance->CU != 0 ) &&
            ( pInstance->CV < INT32_MAX ) )
    {
        pInstance->CV += 1;
    }
    pInstance->Q = ( pInstance->CV >= pInstance->PV );

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTU_LINT( char * Memory )
{
    CTU_LINT *pInstance;


    pInstance = ( CTU_LINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( ( pInstance->CU != 0 ) &&
            ( pInstance->CV < INT64_MAX ) )
    {
        pInstance->CV += 1;
    }
    pInstance->Q = ( pInstance->CV >= pInstance->PV );

    return 0;
}

#endif

int 
counters_executeCTU_UDINT( char * Memory )
{
    CTU_UDINT *pInstance;


    pInstance = ( CTU_UDINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( ( pInstance->CU != 0 ) &&
            ( pInstance->CV < UINT32_MAX ) )
    {
        pInstance->CV += 1;
    }
    pInstance->Q = ( pInstance->CV >= pInstance->PV );

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTU_ULINT( char * Memory )
{
    CTU_ULINT *pInstance;


    pInstance = ( CTU_ULINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( ( pInstance->CU != 0 ) &&
            ( pInstance->CV < UINT64_MAX ) )
    {
        pInstance->CV += 1;
    }
    pInstance->Q = ( pInstance->CV >= pInstance->PV );

    return 0;
}

#endif

int 
counters_executeCTUD( char * Memory )
{
    CTUD *pInstance;


    pInstance = ( CTUD * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else
    {
        if( ! ( ( pInstance->CU != 0 ) &&
                ( pInstance->CD != 0 ) ) )
        {
            if( ( pInstance->CU != 0 ) &&
                    ( pInstance->CV < INT16_MAX ) )
            {
                pInstance->CV += 1;
            }
            else if( ( pInstance->CD != 0 ) &&
                    ( pInstance->CV > INT16_MIN ) )
            {
                pInstance->CV -= 1;
            }
        }
    }
    pInstance->QU = ( pInstance->CV >= pInstance->PV );
    pInstance->QD = ( pInstance->CV <= 0 );

    return -EFAULT;
}


int 
counters_executeCTUD_DINT( char * Memory )
{
    CTUD_DINT *pInstance;


    pInstance = ( CTUD_DINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else
    {
        if( ! ( ( pInstance->CU != 0 ) &&
                ( pInstance->CD != 0 ) ) )
        {
            if( ( pInstance->CU != 0 ) &&
                    ( pInstance->CV < INT32_MAX ) )
            {
                pInstance->CV += 1;
            }
            else if( ( pInstance->CD != 0 ) &&
                    ( pInstance->CV > INT32_MIN ) )
            {
                pInstance->CV -= 1;
            }
        }
    }
    pInstance->QU = ( pInstance->CV >= pInstance->PV );
    pInstance->QD = ( pInstance->CV <= 0 );

    return -EFAULT;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTUD_LINT( char * Memory )
{
    CTUD_LINT *pInstance;


    pInstance = ( CTUD_LINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else
    {
        if( ! ( ( pInstance->CU != 0 ) &&
                ( pInstance->CD != 0 ) ) )
        {
            if( ( pInstance->CU != 0 ) &&
                    ( pInstance->CV < INT64_MAX ) )
            {
                pInstance->CV += 1;
            }
            else if( ( pInstance->CD != 0 ) &&
                    ( pInstance->CV > INT64_MIN ) )
            {
                pInstance->CV -= 1;
            }
        }
    }
    pInstance->QU = ( pInstance->CV >= pInstance->PV );
    pInstance->QD = ( pInstance->CV <= 0 );

    return -EFAULT;
}

#endif

int 
counters_executeCTUD_UDINT( char * Memory )
{
    CTUD_UDINT *pInstance;


    pInstance = ( CTUD_UDINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else
    {
        if( ! ( ( pInstance->CU != 0 ) &&
                ( pInstance->CD != 0 ) ) )
        {
            if( ( pInstance->CU != 0 ) &&
                    ( pInstance->CV < UINT32_MAX ) )
            {
                pInstance->CV += 1;
            }
            else if( ( pInstance->CD != 0 ) &&
                    ( pInstance->CV > 0 ) )
            {
                pInstance->CV -= 1;
            }
        }
    }
    pInstance->QU = ( pInstance->CV >= pInstance->PV );
    pInstance->QD = ( pInstance->CV <= 0 );

    return -EFAULT;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_executeCTUD_ULINT( char * Memory )
{
    CTUD_ULINT *pInstance;


    pInstance = ( CTUD_ULINT * ) Memory;
    if( pInstance->R != 0 )
    {
        pInstance->CV = 0;
    }
    else if( pInstance->LD != 0 )
    {
        pInstance->CV = pInstance->PV;
    }
    else
    {
        if( ! ( ( pInstance->CU != 0 ) &&
                ( pInstance->CD != 0 ) ) )
        {
            if( ( pInstance->CU != 0 ) &&
                    ( pInstance->CV < UINT64_MAX ) )
            {
                pInstance->CV += 1;
            }
            else if( ( pInstance->CD != 0 ) &&
                    ( pInstance->CV > 0 ) )
            {
                pInstance->CV -= 1;
            }
        }
    }
    pInstance->QU = ( pInstance->CV >= pInstance->PV );
    pInstance->QD = ( pInstance->CV <= 0 );

    return -EFAULT;
}

#endif

int 
counters_parameterCTD( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD, PV );
        *Type = TYPE_INT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD, CV );
        *Type = TYPE_INT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}


int 
counters_parameterCTD_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_DINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_DINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_DINT, PV );
        *Type = TYPE_DINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_DINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_DINT, CV );
        *Type = TYPE_DINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTD_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_LINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_LINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_LINT, PV );
        *Type = TYPE_LINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_LINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_LINT, CV );
        *Type = TYPE_LINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

int 
counters_parameterCTD_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_UDINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_UDINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_UDINT, PV );
        *Type = TYPE_UDINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_UDINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_UDINT, CV );
        *Type = TYPE_UDINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTD_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_ULINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_ULINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTD_ULINT, PV );
        *Type = TYPE_ULINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_ULINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTD_ULINT, CV );
        *Type = TYPE_ULINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

int 
counters_parameterCTU( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU, PV );
        *Type = TYPE_INT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU, CV );
        *Type = TYPE_INT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}


int 
counters_parameterCTU_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_DINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_DINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_DINT, PV );
        *Type = TYPE_DINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_DINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_DINT, CV );
        *Type = TYPE_DINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTU_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_LINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_LINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_LINT, PV );
        *Type = TYPE_LINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_LINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_LINT, CV );
        *Type = TYPE_LINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

int 
counters_parameterCTU_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_UDINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_UDINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_UDINT, PV );
        *Type = TYPE_UDINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_UDINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_UDINT, CV );
        *Type = TYPE_UDINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTU_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_ULINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_ULINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTU_ULINT, PV );
        *Type = TYPE_ULINT;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_ULINT, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTU_ULINT, CV );
        *Type = TYPE_ULINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

int 
counters_parameterCTUD( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD, PV );
        *Type = TYPE_INT;
    }
    else if( strcmp( Name, "QU" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD, QU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "QD" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD, QD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD, CV );
        *Type = TYPE_INT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}


int 
counters_parameterCTUD_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_DINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_DINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_DINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_DINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_DINT, PV );
        *Type = TYPE_DINT;
    }
    else if( strcmp( Name, "QU" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_DINT, QU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "QD" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_DINT, QD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_DINT, CV );
        *Type = TYPE_DINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTUD_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_LINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_LINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_LINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_LINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_LINT, PV );
        *Type = TYPE_LINT;
    }
    else if( strcmp( Name, "QU" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_LINT, QU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "QD" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_LINT, QD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_LINT, CV );
        *Type = TYPE_LINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

int 
counters_parameterCTUD_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_UDINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_UDINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_UDINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_UDINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_UDINT, PV );
        *Type = TYPE_UDINT;
    }
    else if( strcmp( Name, "QU" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_UDINT, QU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "QD" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_UDINT, QD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_UDINT, CV );
        *Type = TYPE_UDINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int 
counters_parameterCTUD_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "CU" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_ULINT, CU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_ULINT, CD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_ULINT, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "LD" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_ULINT, LD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PV" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( CTUD_ULINT, PV );
        *Type = TYPE_ULINT;
    }
    else if( strcmp( Name, "QU" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_ULINT, QU );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "QD" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_ULINT, QD );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "CV" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( CTUD_ULINT, CV );
        *Type = TYPE_ULINT;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

#endif

