#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>

#include "iec61131_external.h"

#include "bitmask.h"
#include "bitstring.h"
#include "numeric.h"
#include "selection.h"
#include "type.h"

#include "config.h"


EXTERNAL_FUNCTION_DECLARATION IEC61131_FUNCTION[] =
{
    /* 2.5.1.5 Standard Functions */

    /* 2.5.1.5.1 Type Conversion Functions */

#ifndef CONFIG_OPTIMISE_SIZE
    {   "BOOL_TO_SINT",         type_TOSINT             },
    {   "BOOL_TO_INT",          type_TOINT              },
    {   "BOOL_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BOOL_TO_LINT",         type_TOLINT             },
#endif
    {   "BOOL_TO_USINT",        type_TOUSINT            },
    {   "BOOL_TO_UINT",         type_TOUINT             },
    {   "BOOL_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BOOL_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "BOOL_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "BOOL_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "BOOL_TO_TIME",         type_TOTIME             },
    {   "BOOL_TO_DATE",         type_TODATE             },
    {   "BOOL_TO_TOD",          type_TOTOD              },
    {   "BOOL_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "BOOL_TO_STRING",       type_TOSTRING           },
#endif
    {   "BOOL_TO_BYTE",         type_TOBYTE             },
    {   "BOOL_TO_WORD",         type_TOWORD             },
    {   "BOOL_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BOOL_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "BOOL_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "SINT_TO_BOOL",         type_TOBOOL             },
    {   "SINT_TO_INT",          type_TOINT              },
    {   "SINT_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "SINT_TO_LINT",         type_TOLINT             },
#endif
    {   "SINT_TO_USINT",        type_TOUSINT            },
    {   "SINT_TO_UINT",         type_TOUINT             },
    {   "SINT_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "SINT_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "SINT_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "SINT_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "SINT_TO_TIME",         type_TOTIME             },
    {   "SINT_TO_DATE",         type_TODATE             },
    {   "SINT_TO_TOD",          type_TOTOD              },
    {   "SINT_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "SINT_TO_STRING",       type_TOSTRING           },
#endif
    {   "SINT_TO_BYTE",         type_TOBYTE             },
    {   "SINT_TO_WORD",         type_TOWORD             },
    {   "SINT_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "SINT_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "SINT_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "INT_TO_BOOL",          type_TOBOOL             },
    {   "INT_TO_SINT",          type_TOSINT             },
    {   "INT_TO_DINT",          type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "INT_TO_LINT",          type_TOLINT             },
#endif
    {   "INT_TO_USINT",         type_TOUSINT            },
    {   "INT_TO_UINT",          type_TOUINT             },
    {   "INT_TO_UDINT",         type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "INT_TO_ULINT",         type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "INT_TO_REAL",          type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "INT_TO_LREAL",         type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "INT_TO_TIME",          type_TOTIME             },
    {   "INT_TO_DATE",          type_TODATE             },
    {   "INT_TO_TOD",           type_TOTOD              },
    {   "INT_TO_DT",            type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "INT_TO_STRING",        type_TOSTRING           },
#endif
    {   "INT_TO_BYTE",          type_TOBYTE             },
    {   "INT_TO_WORD",          type_TOWORD             },
    {   "INT_TO_DWORD",         type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "INT_TO_LWORD",         type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "INT_TO_WSTRING",       type_TOWSTRING          },
#endif
    {   "DINT_TO_BOOL",         type_TOBOOL             },
    {   "DINT_TO_SINT",         type_TOSINT             },
    {   "DINT_TO_INT",          type_TOINT              },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DINT_TO_LINT",         type_TOLINT             },
#endif
    {   "DINT_TO_USINT",        type_TOUSINT            },
    {   "DINT_TO_UINT",         type_TOUINT             },
    {   "DINT_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DINT_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "DINT_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "DINT_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "DINT_TO_TIME",         type_TOTIME             },
    {   "DINT_TO_DATE",         type_TODATE             },
    {   "DINT_TO_TOD",          type_TOTOD              },
    {   "DINT_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "DINT_TO_STRING",       type_TOSTRING           },
#endif
    {   "DINT_TO_BYTE",         type_TOBYTE             },
    {   "DINT_TO_WORD",         type_TOWORD             },
    {   "DINT_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DINT_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "DINT_TO_WSTRING",      type_TOWSTRING          },
#endif
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LINT_TO_BOOL",         type_TOBOOL             },
    {   "LINT_TO_SINT",         type_TOSINT             },
    {   "LINT_TO_INT",          type_TOINT              },
    {   "LINT_TO_DINT",         type_TODINT             },
    {   "LINT_TO_USINT",        type_TOUSINT            },
    {   "LINT_TO_UINT",         type_TOUINT             },
    {   "LINT_TO_UDINT",        type_TOUDINT            },
    {   "LINT_TO_ULINT",        type_TOULINT            },
#ifdef CONFIG_SUPPORT_REAL
    {   "LINT_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "LINT_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "LINT_TO_TIME",         type_TOTIME             },
    {   "LINT_TO_DATE",         type_TODATE             },
    {   "LINT_TO_TOD",          type_TOTOD              },
    {   "LINT_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "LINT_TO_STRING",       type_TOSTRING           },
#endif
    {   "LINT_TO_BYTE",         type_TOBYTE             },
    {   "LINT_TO_WORD",         type_TOWORD             },
    {   "LINT_TO_DWORD",        type_TODWORD            },
    {   "LINT_TO_LWORD",        type_TOLWORD            },
#ifdef CONFIG_SUPPORT_WSTRING
    {   "LINT_TO_WSTRING",      type_TOWSTRING          },
#endif
#endif
    {   "USINT_TO_BOOL",        type_TOBOOL             },
    {   "USINT_TO_SINT",        type_TOSINT             },
    {   "USINT_TO_INT",         type_TOINT              },
    {   "USINT_TO_DINT",        type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "USINT_TO_LINT",        type_TOLINT             },
#endif
    {   "USINT_TO_UINT",        type_TOUINT             },
    {   "USINT_TO_UDINT",       type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "USINT_TO_ULINT",       type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "USINT_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "USINT_TO_LREAL",       type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "USINT_TO_TIME",        type_TOTIME             },
    {   "USINT_TO_DATE",        type_TODATE             },
    {   "USINT_TO_TOD",         type_TOTOD              },
    {   "USINT_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "USINT_TO_STRING",      type_TOSTRING           },
#endif
    {   "USINT_TO_BYTE",        type_TOBYTE             },
    {   "USINT_TO_WORD",        type_TOWORD             },
    {   "USINT_TO_DWORD",       type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "USINT_TO_LWORD",       type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "USINT_TO_WSTRING",     type_TOWSTRING          },
#endif
    {   "UINT_TO_BOOL",         type_TOBOOL             },
    {   "UINT_TO_SINT",         type_TOSINT             },
    {   "UINT_TO_INT",          type_TOINT              },
    {   "UINT_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UINT_TO_LINT",         type_TOLINT             },
#endif
    {   "UINT_TO_USINT",        type_TOUSINT            },
    {   "UINT_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UINT_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "UINT_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "UINT_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "UINT_TO_TIME",         type_TOTIME             },
    {   "UINT_TO_DATE",         type_TODATE             },
    {   "UINT_TO_TOD",          type_TOTOD              },
    {   "UINT_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "UINT_TO_STRING",       type_TOSTRING           },
#endif
    {   "UINT_TO_BYTE",         type_TOBYTE             },
    {   "UINT_TO_WORD",         type_TOWORD             },
    {   "UINT_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UINT_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "UINT_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "UDINT_TO_BOOL",        type_TOBOOL             },
    {   "UDINT_TO_SINT",        type_TOSINT             },
    {   "UDINT_TO_INT",         type_TOINT              },
    {   "UDINT_TO_DINT",        type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UDINT_TO_LINT",        type_TOLINT             },
#endif
    {   "UDINT_TO_USINT",       type_TOUSINT            },
    {   "UDINT_TO_UINT",        type_TOUINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UDINT_TO_ULINT",       type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "UDINT_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "UDINT_TO_LREAL",       type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "UDINT_TO_TIME",        type_TOTIME             },
    {   "UDINT_TO_DATE",        type_TODATE             },
    {   "UDINT_TO_TOD",         type_TOTOD              },
    {   "UDINT_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "UDINT_TO_STRING",      type_TOSTRING           },
#endif
    {   "UDINT_TO_BYTE",        type_TOBYTE             },
    {   "UDINT_TO_WORD",        type_TOWORD             },
    {   "UDINT_TO_DWORD",       type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UDINT_TO_LWORD",       type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "UDINT_TO_WSTRING",     type_TOWSTRING          },
#endif
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "ULINT_TO_BOOL",        type_TOBOOL             },
    {   "ULINT_TO_SINT",        type_TOSINT             },
    {   "ULINT_TO_INT",         type_TOINT              },
    {   "ULINT_TO_DINT",        type_TODINT             },
    {   "ULINT_TO_LINT",        type_TOLINT             },
    {   "ULINT_TO_USINT",       type_TOUSINT            },
    {   "ULINT_TO_UINT",        type_TOUINT             },
    {   "ULINT_TO_UDINT",       type_TOUDINT            },
#ifdef CONFIG_SUPPORT_REAL
    {   "ULINT_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "ULINT_TO_LREAL",       type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "ULINT_TO_TIME",        type_TOTIME             },
    {   "ULINT_TO_DATE",        type_TODATE             },
    {   "ULINT_TO_TOD",         type_TOTOD              },
    {   "ULINT_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "ULINT_TO_STRING",      type_TOSTRING           },
#endif
    {   "ULINT_TO_BYTE",        type_TOBYTE             },
    {   "ULINT_TO_WORD",        type_TOWORD             },
    {   "ULINT_TO_DWORD",       type_TODWORD            },
    {   "ULINT_TO_LWORD",       type_TOLWORD            },
#ifdef CONFIG_SUPPORT_WSTRING
    {   "ULINT_TO_WSTRING",     type_TOWSTRING          },
#endif
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "REAL_TO_BOOL",         type_TOBOOL             },
    {   "REAL_TO_SINT",         type_TOSINT             },
    {   "REAL_TO_INT",          type_TOINT              },
    {   "REAL_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "REAL_TO_LINT",         type_TOLINT             },
#endif
    {   "REAL_TO_USINT",        type_TOUSINT            },
    {   "REAL_TO_UINT",         type_TOUINT             },
    {   "REAL_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "REAL_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "REAL_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "REAL_TO_TIME",         type_TOTIME             },
    {   "REAL_TO_DATE",         type_TODATE             },
    {   "REAL_TO_TOD",          type_TOTOD              },
    {   "REAL_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "REAL_TO_STRING",       type_TOSTRING           },
#endif
    {   "REAL_TO_BYTE",         type_TOBYTE             },
    {   "REAL_TO_WORD",         type_TOWORD             },
    {   "REAL_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "REAL_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "REAL_TO_WSTRING",      type_TOWSTRING          },
#endif
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "LREAL_TO_BOOL",        type_TOBOOL             },
    {   "LREAL_TO_SINT",        type_TOSINT             },
    {   "LREAL_TO_INT",         type_TOINT              },
    {   "LREAL_TO_DINT",        type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LREAL_TO_LINT",        type_TOLINT             },
#endif
    {   "LREAL_TO_USINT",       type_TOUSINT            },
    {   "LREAL_TO_UINT",        type_TOUINT             },
    {   "LREAL_TO_UDINT",       type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LREAL_TO_ULINT",       type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "LREAL_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "LREAL_TO_TIME",        type_TOTIME             },
    {   "LREAL_TO_DATE",        type_TODATE             },
    {   "LREAL_TO_TOD",         type_TOTOD              },
    {   "LREAL_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "LREAL_TO_STRING",      type_TOSTRING           },
#endif
    {   "LREAL_TO_BYTE",        type_TOBYTE             },
    {   "LREAL_TO_WORD",        type_TOWORD             },
    {   "LREAL_TO_DWORD",       type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LREAL_TO_LWORD",       type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "LREAL_TO_WSTRING",     type_TOWSTRING          },
#endif
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "TIME_TO_BOOL",         type_TOBOOL             },
    {   "TIME_TO_SINT",         type_TOSINT             },
    {   "TIME_TO_INT",          type_TOINT              },
    {   "TIME_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TIME_TO_LINT",         type_TOLINT             },
#endif
    {   "TIME_TO_USINT",        type_TOUSINT            },
    {   "TIME_TO_UINT",         type_TOUINT             },
    {   "TIME_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TIME_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "TIME_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "TIME_TO_LREAL",        type_TOLREAL            },
#endif
    {   "TIME_TO_DATE",         type_TODATE             },
    {   "TIME_TO_TOD",          type_TOTOD              },
    {   "TIME_TO_DT",           type_TODT               },
#ifdef CONFIG_SUPPORT_STRING
    {   "TIME_TO_STRING",       type_TOSTRING           },
#endif
    {   "TIME_TO_BYTE",         type_TOBYTE             },
    {   "TIME_TO_WORD",         type_TOWORD             },
    {   "TIME_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TIME_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "TIME_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "TOD_TO_BOOL",          type_TOBOOL             },
    {   "TOD_TO_SINT",          type_TOSINT             },
    {   "TOD_TO_INT",           type_TOINT              },
    {   "TOD_TO_DINT",          type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TOD_TO_LINT",          type_TOLINT             },
#endif
    {   "TOD_TO_USINT",         type_TOUSINT            },
    {   "TOD_TO_UINT",          type_TOUINT             },
    {   "TOD_TO_UDINT",         type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TOD_TO_ULINT",         type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "TOD_TO_REAL",          type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "TOD_TO_LREAL",         type_TOLREAL            },
#endif
    {   "TOD_TO_TIME",          type_TOTIME             },
    {   "TOD_TO_DATE",          type_TODATE             },
    {   "TOD_TO_DT",            type_TODT               },
#ifdef CONFIG_SUPPORT_STRING
    {   "TOD_TO_STRING",        type_TOSTRING           },
#endif
    {   "TOD_TO_BYTE",          type_TOBYTE             },
    {   "TOD_TO_WORD",          type_TOWORD             },
    {   "TOD_TO_DWORD",         type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "TOD_TO_LWORD",         type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "TOD_TO_WSTRING",       type_TOWSTRING          },
#endif
    {   "DT_TO_BOOL",           type_TOBOOL             },
    {   "DT_TO_SINT",           type_TOSINT             },
    {   "DT_TO_INT",            type_TOINT              },
    {   "DT_TO_DINT",           type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DT_TO_LINT",           type_TOLINT             },
#endif
    {   "DT_TO_USINT",          type_TOUSINT            },
    {   "DT_TO_UINT",           type_TOUINT             },
    {   "DT_TO_UDINT",          type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DT_TO_ULINT",          type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "DT_TO_REAL",           type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "DT_TO_LREAL",          type_TOLREAL            },
#endif
    {   "DT_TO_TIME",           type_TOTIME             },
    {   "DT_TO_DATE",           type_TODATE             },
    {   "DT_TO_TOD",            type_TOTOD              },
#ifdef CONFIG_SUPPORT_STRING
    {   "DT_TO_STRING",         type_TOSTRING           },
#endif
    {   "DT_TO_BYTE",           type_TOBYTE             },
    {   "DT_TO_WORD",           type_TOWORD             },
    {   "DT_TO_DWORD",          type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DT_TO_LWORD",          type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "DT_TO_WSTRING",        type_TOWSTRING          },
#endif
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "STRING_TO_BOOL",       type_TOBOOL             },
    {   "STRING_TO_SINT",       type_TOSINT             },
    {   "STRING_TO_INT",        type_TOINT              },
    {   "STRING_TO_DINT",       type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "STRING_TO_LINT",       type_TOLINT             },
#endif
    {   "STRING_TO_USINT",      type_TOUSINT            },
    {   "STRING_TO_UINT",       type_TOUINT             },
    {   "STRING_TO_UDINT",      type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "STRING_TO_ULINT",      type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "STRING_TO_REAL",       type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "STRING_TO_LREAL",      type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "STRING_TO_TIME",       type_TOTIME             },
    {   "STRING_TO_DATE",       type_TODATE             },
    {   "STRING_TO_TOD",        type_TOTOD              },
    {   "STRING_TO_DT",         type_TODT               },
#endif
    {   "STRING_TO_BYTE",       type_TOBYTE             },
    {   "STRING_TO_WORD",       type_TOWORD             },
    {   "STRING_TO_DWORD",      type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "STRING_TO_LWORD",      type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "STRING_TO_WSTRING",    type_TOWSTRING          },
#endif
#endif
    {   "BYTE_TO_BOOL",         type_TOBOOL             },
    {   "BYTE_TO_SINT",         type_TOSINT             },
    {   "BYTE_TO_INT",          type_TOINT              },
    {   "BYTE_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BYTE_TO_LINT",         type_TOLINT             },
#endif
    {   "BYTE_TO_USINT",        type_TOUSINT            },
    {   "BYTE_TO_UINT",         type_TOUINT             },
    {   "BYTE_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BYTE_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "BYTE_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "BYTE_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "BYTE_TO_TIME",         type_TOTIME             },
    {   "BYTE_TO_DATE",         type_TODATE             },
    {   "BYTE_TO_TOD",          type_TOTOD              },
    {   "BYTE_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "BYTE_TO_STRING",       type_TOSTRING           },
#endif
    {   "BYTE_TO_WORD",         type_TOWORD             },
    {   "BYTE_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BYTE_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "BYTE_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "WORD_TO_BOOL",         type_TOBOOL             },
    {   "WORD_TO_SINT",         type_TOSINT             },
    {   "WORD_TO_INT",          type_TOINT              },
    {   "WORD_TO_DINT",         type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WORD_TO_LINT",         type_TOLINT             },
#endif
    {   "WORD_TO_USINT",        type_TOUSINT            },
    {   "WORD_TO_UINT",         type_TOUINT             },
    {   "WORD_TO_UDINT",        type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WORD_TO_ULINT",        type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "WORD_TO_REAL",         type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "WORD_TO_LREAL",        type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "WORD_TO_TIME",         type_TOTIME             },
    {   "WORD_TO_DATE",         type_TODATE             },
    {   "WORD_TO_TOD",          type_TOTOD              },
    {   "WORD_TO_DT",           type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "WORD_TO_STRING",       type_TOSTRING           },
#endif
    {   "WORD_TO_BYTE",         type_TOBYTE             },
    {   "WORD_TO_DWORD",        type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WORD_TO_LWORD",        type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "WORD_TO_WSTRING",      type_TOWSTRING          },
#endif
    {   "DWORD_TO_BOOL",        type_TOBOOL             },
    {   "DWORD_TO_SINT",        type_TOSINT             },
    {   "DWORD_TO_INT",         type_TOINT              },
    {   "DWORD_TO_DINT",        type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DWORD_TO_LINT",        type_TOLINT             },
#endif
    {   "DWORD_TO_USINT",       type_TOUSINT            },
    {   "DWORD_TO_UINT",        type_TOUINT             },
    {   "DWORD_TO_UDINT",       type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DWORD_TO_ULINT",       type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "DWORD_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "DWORD_TO_LREAL",       type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "DWORD_TO_TIME",        type_TOTIME             },
    {   "DWORD_TO_DATE",        type_TODATE             },
    {   "DWORD_TO_TOD",         type_TOTOD              },
    {   "DWORD_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "DWORD_TO_STRING",      type_TOSTRING           },
#endif
    {   "DWORD_TO_BYTE",        type_TOBYTE             },
    {   "DWORD_TO_WORD",        type_TOWORD             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DWORD_TO_LWORD",       type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "DWORD_TO_WSTRING",     type_TOWSTRING          },
#endif
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LWORD_TO_BOOL",        type_TOBOOL             },
    {   "LWORD_TO_SINT",        type_TOSINT             },
    {   "LWORD_TO_INT",         type_TOINT              },
    {   "LWORD_TO_DINT",        type_TODINT             },
    {   "LWORD_TO_LINT",        type_TOLINT             },
    {   "LWORD_TO_USINT",       type_TOUSINT            },
    {   "LWORD_TO_UINT",        type_TOUINT             },
    {   "LWORD_TO_UDINT",       type_TOUDINT            },
    {   "LWORD_TO_ULINT",       type_TOULINT            },
#ifdef CONFIG_SUPPORT_REAL
    {   "LWORD_TO_REAL",        type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "LWORD_TO_LREAL",       type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "LWORD_TO_TIME",        type_TOTIME             },
    {   "LWORD_TO_DATE",        type_TODATE             },
    {   "LWORD_TO_TOD",         type_TOTOD              },
    {   "LWORD_TO_DT",          type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "LWORD_TO_STRING",      type_TOSTRING           },
#endif
    {   "LWORD_TO_BYTE",        type_TOBYTE             },
    {   "LWORD_TO_WORD",        type_TOWORD             },
    {   "LWORD_TO_DWORD",       type_TODWORD            },
#ifdef CONFIG_SUPPORT_STRING
    {   "LWORD_TO_WSTRING",     type_TOWSTRING          },
#endif
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "WSTRING_TO_BOOL",      type_TOBOOL             },
    {   "WSTRING_TO_SINT",      type_TOSINT             },
    {   "WSTRING_TO_INT",       type_TOINT              },
    {   "WSTRING_TO_DINT",      type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WSTRING_TO_LINT",      type_TOLINT             },
#endif
    {   "WSTRING_TO_USINT",     type_TOUSINT            },
    {   "WSTRING_TO_UINT",      type_TOUINT             },
    {   "WSTRING_TO_UDINT",     type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WSTRING_TO_ULINT",     type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "WSTRING_TO_REAL",      type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "WSTRING_TO_LREAL",     type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "WSTRING_TO_TIME",      type_TOTIME             },
    {   "WSTRING_TO_DATE",      type_TODATE             },
    {   "WSTRING_TO_TOD",       type_TOTOD              },
    {   "WSTRING_TO_DT",        type_TODT               },
#endif
    {   "WSTRING_TO_STRING",    type_TOSTRING           },
    {   "WSTRING_TO_BYTE",      type_TOBYTE             },
    {   "WSTRING_TO_WORD",      type_TOWORD             },
    {   "WSTRING_TO_DWORD",     type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WSTRING_TO_LWORD",     type_TOLWORD            },
#endif
#endif
    {   "TRUNC",                NULL                    },
    {   "BYTE_BCD_TO_USINT",    NULL                    },
    {   "BYTE_BCD_TO_UINT",     NULL                    },
    {   "BYTE_BCD_TO_UDINT",    NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "BYTE_BCD_TO_ULINT",    NULL                    },
#endif
    {   "WORD_BCD_TO_USINT",    NULL                    },
    {   "WORD_BCD_TO_UINT",     NULL                    },
    {   "WORD_BCD_TO_UDINT",    NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "WORD_BCD_TO_ULINT",    NULL                    },
#endif
    {   "DWORD_BCD_TO_USINT",   NULL                    },
    {   "DWORD_BCD_TO_UINT",    NULL                    },
    {   "DWORD_BCD_TO_UDINT",   NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "DWORD_BCD_TO_ULINT",   NULL                    },
#endif
    {   "LWORD_BCD_TO_USINT",   NULL                    },
    {   "LWORD_BCD_TO_UINT",    NULL                    },
    {   "LWORD_BCD_TO_UDINT",   NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "LWORD_BCD_TO_ULINT",   NULL                    },
#endif
    {   "USINT_TO_BCD_BYTE",    NULL                    },
    {   "USINT_TO_BCD_WORD",    NULL                    },
    {   "USINT_TO_BCD_DWORD",   NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "USINT_TO_BCD_LWORD",   NULL                    },
#endif
    {   "UINT_TO_BCD_BYTE",     NULL                    },
    {   "UINT_TO_BCD_WORD",     NULL                    },
    {   "UINT_TO_BCD_DWORD",    NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UINT_TO_BCD_LWORD",    NULL                    },
#endif
    {   "UDINT_TO_BCD_BYTE",    NULL                    },
    {   "UDINT_TO_BCD_WORD",    NULL                    },
    {   "UDINT_TO_BCD_DWORD",   NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "UDINT_TO_BCD_LWORD",   NULL                    },
#endif
    {   "ULINT_TO_BCD_BYTE",    NULL                    },
    {   "ULINT_TO_BCD_WORD",    NULL                    },
    {   "ULINT_TO_BCD_DWORD",   NULL                    },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "ULINT_TO_BCD_LWORD",   NULL                    },
#endif
#endif

    /* 2.5.1.5.2 Numerical Functions */

#if defined( CONFIG_SUPPORT_REAL ) || defined( CONFIG_SUPPORT_LREAL )
    {   "ABS",                  numeric_ABS             },
    {   "SQRT",                 numeric_SQRT            },
    {   "LN",                   numeric_LN              },
    {   "LOG",                  numeric_LOG             },
    {   "EXP",                  numeric_EXP             },
    {   "SIN",                  numeric_SIN             },
    {   "COS",                  numeric_COS             },
    {   "TAN",                  numeric_TAN             },
    {   "ASIN",                 numeric_ASIN            },
    {   "ACOS",                 numeric_ACOS            },
    {   "ATAN",                 numeric_ATAN            },

#endif
    /* 2.5.1.5.3 Bit String Functions */

    {   "SHL",                  bitstring_SHL           },
    {   "SHR",                  bitstring_SHR           },
    {   "ROR",                  bitstring_ROR           },
    {   "ROL",                  bitstring_ROL           },

    /* 2.5.1.5.4 Selection and Comparison Functions */

    {   "SEL",                  selection_SEL           },
    {   "MAX",                  selection_MAX           },
    {   "MIN",                  selection_MIN           },
    {   "LIMIT",                selection_LIMIT         },
    {   "MUX",                  selection_MUX           },
#ifndef CONFIG_OPTIMISE_SIZE

    /*
        The MUX function can be typed as defined in 2.5.1.4 in the form MUX_*_**, 
        where * is the type of K input and ** is the type of the other inputs and 
        output.  It should be noted that at this stage the selection_MUX  function 
        does _not_ assert that all inputs are of the same type.
    */

    {   "MUX_SINT_BOOL",        selection_MUX           },
    {   "MUX_SINT_SINT",        selection_MUX           },
    {   "MUX_SINT_INT",         selection_MUX           },
    {   "MUX_SINT_DINT",        selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_SINT_LINT",        selection_MUX           },
#endif
    {   "MUX_SINT_USINT",       selection_MUX           },
    {   "MUX_SINT_UINT",        selection_MUX           },
    {   "MUX_SINT_UDINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_SINT_ULINT",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_SINT_REAL",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_SINT_LREAL",       selection_MUX           },
#endif
    {   "MUX_SINT_BYTE",        selection_MUX           },
    {   "MUX_SINT_WORD",        selection_MUX           },
    {   "MUX_SINT_DWORD",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_SINT_LWORD",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_SINT_STRING",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_SINT_TIME",        selection_MUX           },
    {   "MUX_SINT_DATE",        selection_MUX           },
    {   "MUX_SINT_TOD",         selection_MUX           },
    {   "MUX_SINT_DT",          selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_SINT_WSTRING",     selection_MUX           },
#endif
    {   "MUX_INT_BOOL",         selection_MUX           },
    {   "MUX_INT_SINT",         selection_MUX           },
    {   "MUX_INT_INT",          selection_MUX           },
    {   "MUX_INT_DINT",         selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_INT_LINT",         selection_MUX           },
#endif
    {   "MUX_INT_USINT",        selection_MUX           },
    {   "MUX_INT_UINT",         selection_MUX           },
    {   "MUX_INT_UDINT",        selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_INT_ULINT",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_INT_REAL",         selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_INT_LREAL",        selection_MUX           },
#endif
    {   "MUX_INT_BYTE",         selection_MUX           },
    {   "MUX_INT_WORD",         selection_MUX           },
    {   "MUX_INT_DWORD",        selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_INT_LWORD",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_INT_STRING",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_INT_TIME",         selection_MUX           },
    {   "MUX_INT_DATE",         selection_MUX           },
    {   "MUX_INT_TOD",          selection_MUX           },
    {   "MUX_INT_DT",           selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_INT_WSTRING",      selection_MUX           },
#endif
    {   "MUX_DINT_BOOL",        selection_MUX           },
    {   "MUX_DINT_SINT",        selection_MUX           },
    {   "MUX_DINT_INT",         selection_MUX           },
    {   "MUX_DINT_DINT",        selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_DINT_LINT",        selection_MUX           },
#endif
    {   "MUX_DINT_USINT",       selection_MUX           },
    {   "MUX_DINT_UINT",        selection_MUX           },
    {   "MUX_DINT_UDINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_DINT_ULINT",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_DINT_REAL",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_DINT_LREAL",       selection_MUX           },
#endif
    {   "MUX_DINT_BYTE",        selection_MUX           },
    {   "MUX_DINT_WORD",        selection_MUX           },
    {   "MUX_DINT_DWORD",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_DINT_LWORD",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_DINT_STRING",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_DINT_TIME",        selection_MUX           },
    {   "MUX_DINT_DATE",        selection_MUX           },
    {   "MUX_DINT_TOD",         selection_MUX           },
    {   "MUX_DINT_DT",          selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_DINT_WSTRING",     selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_LINT_BOOL",        selection_MUX           },
    {   "MUX_LINT_SINT",        selection_MUX           },
    {   "MUX_LINT_INT",         selection_MUX           },
    {   "MUX_LINT_DINT",        selection_MUX           },
    {   "MUX_LINT_LINT",        selection_MUX           },
    {   "MUX_LINT_USINT",       selection_MUX           },
    {   "MUX_LINT_UINT",        selection_MUX           },
    {   "MUX_LINT_UDINT",       selection_MUX           },
    {   "MUX_LINT_ULINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_LINT_REAL",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_LINT_LREAL",       selection_MUX           },
#endif
    {   "MUX_LINT_BYTE",        selection_MUX           },
    {   "MUX_LINT_WORD",        selection_MUX           },
    {   "MUX_LINT_DWORD",       selection_MUX           },
    {   "MUX_LINT_LWORD",       selection_MUX           },
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_LINT_STRING",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_LINT_TIME",        selection_MUX           },
    {   "MUX_LINT_DATE",        selection_MUX           },
    {   "MUX_LINT_TOD",         selection_MUX           },
    {   "MUX_LINT_DT",          selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_LINT_WSTRING",     selection_MUX           },
#endif
#endif
    {   "MUX_USINT_BOOL",       selection_MUX           },
    {   "MUX_USINT_SINT",       selection_MUX           },
    {   "MUX_USINT_INT",        selection_MUX           },
    {   "MUX_USINT_DINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_USINT_LINT",       selection_MUX           },
#endif
    {   "MUX_USINT_USINT",      selection_MUX           },
    {   "MUX_USINT_UINT",       selection_MUX           },
    {   "MUX_USINT_UDINT",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_USINT_ULINT",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_USINT_REAL",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_USINT_LREAL",      selection_MUX           },
#endif
    {   "MUX_USINT_BYTE",       selection_MUX           },
    {   "MUX_USINT_WORD",       selection_MUX           },
    {   "MUX_USINT_DWORD",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_USINT_LWORD",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_USINT_STRING",     selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_USINT_TIME",       selection_MUX           },
    {   "MUX_USINT_DATE",       selection_MUX           },
    {   "MUX_USINT_TOD",        selection_MUX           },
    {   "MUX_USINT_DT",         selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_USINT_WSTRING",    selection_MUX           },
#endif
    {   "MUX_UINT_BOOL",        selection_MUX           },
    {   "MUX_UINT_SINT",        selection_MUX           },
    {   "MUX_UINT_INT",         selection_MUX           },
    {   "MUX_UINT_DINT",        selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UINT_LINT",        selection_MUX           },
#endif
    {   "MUX_UINT_USINT",       selection_MUX           },
    {   "MUX_UINT_UINT",        selection_MUX           },
    {   "MUX_UINT_UDINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UINT_ULINT",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_UINT_REAL",        selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_UINT_LREAL",       selection_MUX           },
#endif
    {   "MUX_UINT_BYTE",        selection_MUX           },
    {   "MUX_UINT_WORD",        selection_MUX           },
    {   "MUX_UINT_DWORD",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UINT_LWORD",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_UINT_STRING",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_UINT_TIME",        selection_MUX           },
    {   "MUX_UINT_DATE",        selection_MUX           },
    {   "MUX_UINT_TOD",         selection_MUX           },
    {   "MUX_UINT_DT",          selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_UINT_WSTRING",     selection_MUX           },
#endif
    {   "MUX_UDINT_BOOL",       selection_MUX           },
    {   "MUX_UDINT_SINT",       selection_MUX           },
    {   "MUX_UDINT_INT",        selection_MUX           },
    {   "MUX_UDINT_DINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UDINT_LINT",       selection_MUX           },
#endif
    {   "MUX_UDINT_USINT",      selection_MUX           },
    {   "MUX_UDINT_UINT",       selection_MUX           },
    {   "MUX_UDINT_UDINT",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UDINT_ULINT",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_UDINT_REAL",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_UDINT_LREAL",      selection_MUX           },
#endif
    {   "MUX_UDINT_BYTE",       selection_MUX           },
    {   "MUX_UDINT_WORD",       selection_MUX           },
    {   "MUX_UDINT_DWORD",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_UDINT_LWORD",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_UDINT_STRING",     selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_UDINT_TIME",       selection_MUX           },
    {   "MUX_UDINT_DATE",       selection_MUX           },
    {   "MUX_UDINT_TOD",        selection_MUX           },
    {   "MUX_UDINT_DT",         selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_UDINT_WSTRING",    selection_MUX           },
#endif
    {   "MUX_ULINT_BOOL",       selection_MUX           },
    {   "MUX_ULINT_SINT",       selection_MUX           },
    {   "MUX_ULINT_INT",        selection_MUX           },
    {   "MUX_ULINT_DINT",       selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_ULINT_LINT",       selection_MUX           },
#endif
    {   "MUX_ULINT_USINT",      selection_MUX           },
    {   "MUX_ULINT_UINT",       selection_MUX           },
    {   "MUX_ULINT_UDINT",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_ULINT_ULINT",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "MUX_ULINT_REAL",       selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "MUX_ULINT_LREAL",      selection_MUX           },
#endif
    {   "MUX_ULINT_BYTE",       selection_MUX           },
    {   "MUX_ULINT_WORD",       selection_MUX           },
    {   "MUX_ULINT_DWORD",      selection_MUX           },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "MUX_ULINT_LWORD",      selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "MUX_ULINT_STRING",     selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "MUX_ULINT_TIME",       selection_MUX           },
    {   "MUX_ULINT_DATE",       selection_MUX           },
    {   "MUX_ULINT_TOD",        selection_MUX           },
    {   "MUX_ULINT_DT",         selection_MUX           },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "MUX_ULINT_WSTRING",    selection_MUX           },
#endif
#endif

    /* 2.5.1.5.5 Character String Functions */
#if defined( CONFIG_SUPPORT_STRING ) || defined( CONFIG_SUPPORT_WSTRING )
    {   "LEN",                  NULL                    },
    {   "LEFT",                 NULL                    },
    {   "RIGHT",                NULL                    },
    {   "MID",                  NULL                    },
    {   "CONCAT",               NULL                    },
    {   "INSERT",               NULL                    },
    {   "DELETE",               NULL                    },
    {   "REPLACE",              NULL                    },
    {   "FIND",                 NULL                    },
#endif

    /*
        The following functions have not been defined within the IEC 61131-3 
        standard, but have been implemented for utility within the IEC 61131-3 
        virtual machine environment.
    */

    {   "ANY_TO_BOOL",          type_TOBOOL             },
    {   "ANY_TO_SINT",          type_TOSINT             },
    {   "ANY_TO_INT",           type_TOINT              },
    {   "ANY_TO_DINT",          type_TODINT             },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "ANY_TO_LINT",          type_TOLINT             },
#endif
    {   "ANY_TO_USINT",         type_TOUSINT            },
    {   "ANY_TO_UINT",          type_TOUINT             },
    {   "ANY_TO_UDINT",         type_TOUDINT            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "ANY_TO_ULINT",         type_TOULINT            },
#endif
#ifdef CONFIG_SUPPORT_REAL
    {   "ANY_TO_REAL",          type_TOREAL             },
#endif
#ifdef CONFIG_SUPPORT_LREAL
    {   "ANY_TO_LREAL",         type_TOLREAL            },
#endif
#ifdef CONFIG_SUPPORT_DATETIME
    {   "ANY_TO_TIME",          type_TOTIME             },
    {   "ANY_TO_DATE",          type_TODATE             },
    {   "ANY_TO_TOD",           type_TOTOD              },
    {   "ANY_TO_DT",            type_TODT               },
#endif
#ifdef CONFIG_SUPPORT_STRING
    {   "ANY_TO_STRING",        type_TOSTRING           },
#endif
    {   "ANY_TO_BYTE",          type_TOBYTE             },
    {   "ANY_TO_WORD",          type_TOWORD             },
    {   "ANY_TO_DWORD",         type_TODWORD            },
#ifdef CONFIG_SUPPORT_LONGLONG
    {   "ANY_TO_LWORD",         type_TOLWORD            },
#endif
#ifdef CONFIG_SUPPORT_WSTRING
    {   "ANY_TO_WSTRING",       type_TOWSTRING          },
#endif

    /*
        The following functions implement standard bitwise logic functions - AND,
        OR, XOR and NOT.
    */

    {   "AND_MASK",             bitmask_AND             },
    {   "OR_MASK",              bitmask_OR              },
    {   "XOR_MASK",             bitmask_XOR             },
    {   "NOT_MASK",             bitmask_NOT             },
};


size_t IEC61131_FUNCTION_COUNT = ( sizeof( IEC61131_FUNCTION ) / sizeof( typeof( IEC61131_FUNCTION ) ) ); 


