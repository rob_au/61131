#ifndef __COUNTERS_H
#define __COUNTERS_H


#include "iec61131_external.h"
#include "iec61131_types.h"


typedef struct _CTD
{
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( LD );
    EXTERNAL_INT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_INT( CV );
}
CTD;

typedef struct _CTD_DINT
{
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( LD );
    EXTERNAL_DINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_DINT( CV );
}
CTD_DINT;

typedef struct _CTD_LINT
{
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( LD );
    EXTERNAL_LINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_LINT( CV );
}
CTD_LINT;

typedef struct _CTD_UDINT
{
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( LD );
    EXTERNAL_UDINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_UDINT( CV );
}
CTD_UDINT;

typedef struct _CTD_ULINT
{
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( LD );
    EXTERNAL_ULINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_ULINT( CV );
}
CTD_ULINT;

typedef struct _CTU
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( R );
    EXTERNAL_INT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_INT( CV );
}
CTU;

typedef struct _CTU_DINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( R );
    EXTERNAL_DINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_DINT( CV );
}
CTU_DINT;

typedef struct _CTU_LINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( R );
    EXTERNAL_LINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_LINT( CV );
}
CTU_LINT;

typedef struct _CTU_UDINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( R );
    EXTERNAL_UDINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_UDINT( CV );
}
CTU_UDINT;

typedef struct _CTU_ULINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( R );
    EXTERNAL_ULINT( PV );
    EXTERNAL_BOOL( Q );
    EXTERNAL_ULINT( CV );
}
CTU_ULINT;

typedef struct _CTUD
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( LD );
    EXTERNAL_INT( PV );
    EXTERNAL_BOOL( QU );
    EXTERNAL_BOOL( QD );
    EXTERNAL_INT( CV );
}
CTUD;

typedef struct _CTUD_DINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( LD );
    EXTERNAL_DINT( PV );
    EXTERNAL_BOOL( QU );
    EXTERNAL_BOOL( QD );
    EXTERNAL_DINT( CV );
}
CTUD_DINT;

typedef struct _CTUD_LINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( LD );
    EXTERNAL_LINT( PV );
    EXTERNAL_BOOL( QU );
    EXTERNAL_BOOL( QD );
    EXTERNAL_LINT( CV );
}
CTUD_LINT;

typedef struct _CTUD_UDINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( LD );
    EXTERNAL_UDINT( PV );
    EXTERNAL_BOOL( QU );
    EXTERNAL_BOOL( QD );
    EXTERNAL_UDINT( CV );
}
CTUD_UDINT;

typedef struct _CTUD_ULINT
{
    EXTERNAL_BOOL( CU );
    EXTERNAL_BOOL( CD );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( LD );
    EXTERNAL_ULINT( PV );
    EXTERNAL_BOOL( QU );
    EXTERNAL_BOOL( QD );
    EXTERNAL_ULINT( CV );
}
CTUD_ULINT;


int counters_executeCTD( char * Memory );

int counters_executeCTD_DINT( char * Memory );

int counters_executeCTD_LINT( char * Memory );

int counters_executeCTD_UDINT( char * Memory );

int counters_executeCTD_ULINT( char * Memory );

int counters_executeCTU( char * Memory );

int counters_executeCTU_DINT( char * Memory );

int counters_executeCTU_LINT( char * Memory );

int counters_executeCTU_UDINT( char * Memory );

int counters_executeCTU_ULINT( char * Memory );

int counters_executeCTUD( char * Memory );

int counters_executeCTUD_DINT( char * Memory );

int counters_executeCTUD_LINT( char * Memory );

int counters_executeCTUD_UDINT( char * Memory );

int counters_executeCTUD_ULINT( char * Memory );

int counters_parameterCTD( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTD_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTD_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTD_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTD_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTU( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTU_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTU_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTU_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTU_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTUD( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTUD_DINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTUD_LINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTUD_UDINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );

int counters_parameterCTUD_ULINT( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );


#endif
