#ifndef __BITMASK_H
#define __BITMASK_H


#include "iec61131_types.h"


int bitmask_AND( IEC61131_DATA * Parameters, unsigned int Count );

int bitmask_OR( IEC61131_DATA * Parameters, unsigned int Count );

int bitmask_XOR( IEC61131_DATA * Parameters, unsigned int Count );

int bitmask_NOT( IEC61131_DATA * Parameters, unsigned int Count );


#endif
