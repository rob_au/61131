#ifndef __EDGE_H
#define __EDGE_H


#include "iec61131_external.h"
#include "iec61131_types.h"


typedef struct _TRIG
{
    EXTERNAL_BOOL( Clk );
    EXTERNAL_BOOL( Edge );
    EXTERNAL_BOOL( Q );
}
TRIG;


int edge_executeFTRIG( char * Memory );

int edge_executeRTRIG( char * Memory );

int edge_parameterTRIG( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );


#endif

