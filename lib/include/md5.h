#ifndef __MD5_H
#define __MD5_H


#include <stdint.h>


/*
    The following data structure and static functions within this library are
    employed to generate a unique hash for the IEC 61131-3 C-based functions
    defined within the library.  This is in turn used for matching of byte code
    between the IEC 61131-3 compiler and virtual machine run-time, ensuring
    that function offsets specified within the byte code file by the former are
    likely to operate as intended on the latter.
*/

typedef struct _MD5
{
    uint32_t State[4];
    uint32_t Total[2];
    unsigned char Buffer[64];
}
MD5;


void md5_initialise( MD5 *pContext );

void md5_finalise( MD5 *pContext, unsigned char *pDigest );

void md5_update( MD5 *pContext, unsigned char *pBuffer, unsigned int uLength );


#endif
