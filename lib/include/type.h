#ifndef __TYPE_H
#define __TYPE_H


#include "iec61131_types.h"


int type_TOBOOL( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOBYTE( IEC61131_DATA * Parameters, unsigned int Count );

int type_TODATE( IEC61131_DATA * Parameters, unsigned int Count );

int type_TODINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TODT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TODWORD( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOLINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOLREAL( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOLWORD( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOREAL( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOSINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOSTRING( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOTIME( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOTOD( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOUDINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOUINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOULINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOUSINT( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOWORD( IEC61131_DATA * Parameters, unsigned int Count );

int type_TOWSTRING( IEC61131_DATA * Parameters, unsigned int Count );


#endif

