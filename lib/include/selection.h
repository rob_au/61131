#ifndef __SELECTION_H
#define __SELECTION_H


#include "iec61131_types.h"


int selection_LIMIT( IEC61131_DATA * Parameters, unsigned int Count );

int selection_MAX( IEC61131_DATA * Parameters, unsigned int Count );

int selection_MIN( IEC61131_DATA * Parameters, unsigned int Count );

int selection_MUX( IEC61131_DATA * Parameters, unsigned int Count );

int selection_SEL( IEC61131_DATA * Parameters, unsigned int Count );


#endif

