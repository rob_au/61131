#ifndef __TIMERS_H
#define __TIMERS_H


#include "iec61131_external.h"
#include "iec61131_types.h"


typedef struct _TIMER
{
    EXTERNAL_BOOL( EN );
    EXTERNAL_TIME( PT );
    EXTERNAL_BOOL( Q );
    EXTERNAL_TIME( ET );
    EXTERNAL_UDINT( _TIME );
    EXTERNAL_BOOL( _EDGE );
}
TIMER;


int timers_executeTOF( char * Memory );

int timers_executeTON( char * Memory );

int timers_executeTP( char * Memory );

int timers_parameterTIMER( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );


#endif
