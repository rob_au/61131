#include <string.h>
#include <endian.h>

#include "md5.h"


#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21


#define F(x, y, z)          ( ( (x) & (y) ) | ( (~x) & (z) ) )
#define G(x, y, z)          ( ( (x) & (z) ) | ( (y) & (~z) ) )
#define H(x, y, z)          ( (x) ^ (y) ^ (z) )
#define I(x, y, z)          ( (y) ^ ( (x) | (~z) ) )

#define ROTATE_LEFT(x, n)   ( ( (x) << (n) ) | ( (x) >> ( 32 - (n) ) ) )


#define FF(a, b, c, d, x, s, ac) { \
        (a) += F ((b), (c), (d)) + (x) + (uint32_t)(ac); \
        (a) = ROTATE_LEFT ((a), (s)); \
        (a) += (b); \
        }
#define GG(a, b, c, d, x, s, ac) { \
        (a) += G ((b), (c), (d)) + (x) + (uint32_t)(ac); \
        (a) = ROTATE_LEFT ((a), (s)); \
        (a) += (b); \
        }
#define HH(a, b, c, d, x, s, ac) { \
        (a) += H ((b), (c), (d)) + (x) + (uint32_t)(ac); \
        (a) = ROTATE_LEFT ((a), (s)); \
        (a) += (b); \
        }
#define II(a, b, c, d, x, s, ac) { \
        (a) += I ((b), (c), (d)) + (x) + (uint32_t)(ac); \
        (a) = ROTATE_LEFT ((a), (s)); \
        (a) += (b); \
        }
 

static unsigned char __PADDING[64] =
{
        0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


static void _md5_byteToUint32( uint32_t *pValue, unsigned char *pBuffer, size_t uLength );

static void _md5_uint32ToByte( unsigned char *pBuffer, uint32_t *pValue, size_t uLength );

static void _md5_transform( MD5 *pContext, unsigned char *pBuffer );


static void
_md5_byteToUint32( uint32_t *pValue, unsigned char *pBuffer, size_t uLength )
{
    unsigned int uIndex1, uIndex2;


    /* assert( ( uLength % 4 ) == 0 ); */
    for( uIndex1 = 0, uIndex2 = 0; uIndex2 < uLength; ++uIndex1, uIndex2 += 4 )
    {
        pValue[ uIndex1 ] = ( ( uint32_t ) pBuffer[ uIndex2 ] ) |
                ( ( ( uint32_t ) pBuffer[ uIndex2 + 1 ] ) << 8 ) |
                ( ( ( uint32_t ) pBuffer[ uIndex2 + 2 ] ) << 16 ) |
                ( ( ( uint32_t ) pBuffer[ uIndex2 + 3 ] ) << 24 );
    }
}


static void
_md5_uint32ToByte( unsigned char *pBuffer, uint32_t *pValue, size_t uLength )
{
    unsigned int uIndex1, uIndex2;


    /* assert( ( uLength % 4 ) == 0 ); */
    for( uIndex1 = 0, uIndex2 = 0; uIndex2 < uLength; ++uIndex1, uIndex2 += 4 )
    {
        pBuffer[ uIndex2 ] = ( unsigned char ) ( pValue[ uIndex1 ] & 0xff );
        pBuffer[ uIndex2 + 1 ] = ( unsigned char ) ( ( pValue[ uIndex1 ] >> 8 ) & 0xff );
        pBuffer[ uIndex2 + 2 ] = ( unsigned char ) ( ( pValue[ uIndex1 ] >> 16 ) & 0xff );
        pBuffer[ uIndex2 + 3 ] = ( unsigned char ) ( ( pValue[ uIndex1 ] >> 24 ) & 0xff );
    }
}


static void
_md5_transform( MD5 *pContext, unsigned char *pBuffer )
{
    uint32_t A, B, C, D, X[16];


    _md5_byteToUint32( X, pBuffer, 64 );

    A = pContext->State[0];
    B = pContext->State[1];
    C = pContext->State[2];
    D = pContext->State[3];

    FF( A, B, C, D, X[ 0], S11, 0xd76aa478 );  /* 1 */
    FF( D, A, B, C, X[ 1], S12, 0xe8c7b756 );  /* 2 */
    FF( C, D, A, B, X[ 2], S13, 0x242070db );  /* 3 */
    FF( B, C, D, A, X[ 3], S14, 0xc1bdceee );  /* 4 */
    FF( A, B, C, D, X[ 4], S11, 0xf57c0faf );  /* 5 */
    FF( D, A, B, C, X[ 5], S12, 0x4787c62a );  /* 6 */
    FF( C, D, A, B, X[ 6], S13, 0xa8304613 );  /* 7 */
    FF( B, C, D, A, X[ 7], S14, 0xfd469501 );  /* 8 */
    FF( A, B, C, D, X[ 8], S11, 0x698098d8 );  /* 9 */
    FF( D, A, B, C, X[ 9], S12, 0x8b44f7af );  /* 10 */
    FF( C, D, A, B, X[10], S13, 0xffff5bb1 );  /* 11 */
    FF( B, C, D, A, X[11], S14, 0x895cd7be );  /* 12 */
    FF( A, B, C, D, X[12], S11, 0x6b901122 );  /* 13 */
    FF( D, A, B, C, X[13], S12, 0xfd987193 );  /* 14 */
    FF( C, D, A, B, X[14], S13, 0xa679438e );  /* 15 */
    FF( B, C, D, A, X[15], S14, 0x49b40821 );  /* 16 */

    GG( A, B, C, D, X[ 1], S21, 0xf61e2562 );  /* 17 */
    GG( D, A, B, C, X[ 6], S22, 0xc040b340 );  /* 18 */
    GG( C, D, A, B, X[11], S23, 0x265e5a51 );  /* 19 */
    GG( B, C, D, A, X[ 0], S24, 0xe9b6c7aa );  /* 20 */
    GG( A, B, C, D, X[ 5], S21, 0xd62f105d );  /* 21 */
    GG( D, A, B, C, X[10], S22, 0x02441453 );  /* 22 */
    GG( C, D, A, B, X[15], S23, 0xd8a1e681 );  /* 23 */
    GG( B, C, D, A, X[ 4], S24, 0xe7d3fbc8 );  /* 24 */
    GG( A, B, C, D, X[ 9], S21, 0x21e1cde6 );  /* 25 */
    GG( D, A, B, C, X[14], S22, 0xc33707d6 );  /* 26 */
    GG( C, D, A, B, X[ 3], S23, 0xf4d50d87 );  /* 27 */
    GG( B, C, D, A, X[ 8], S24, 0x455a14ed );  /* 28 */
    GG( A, B, C, D, X[13], S21, 0xa9e3e905 );  /* 29 */
    GG( D, A, B, C, X[ 2], S22, 0xfcefa3f8 );  /* 30 */
    GG( C, D, A, B, X[ 7], S23, 0x676f02d9 );  /* 31 */
    GG( B, C, D, A, X[12], S24, 0x8d2a4c8a );  /* 32 */

    HH( A, B, C, D, X[ 5], S31, 0xfffa3942 );  /* 33 */
    HH( D, A, B, C, X[ 8], S32, 0x8771f681 );  /* 34 */
    HH( C, D, A, B, X[11], S33, 0x6d9d6122 );  /* 35 */
    HH( B, C, D, A, X[14], S34, 0xfde5380c );  /* 36 */
    HH( A, B, C, D, X[ 1], S31, 0xa4beea44 );  /* 37 */
    HH( D, A, B, C, X[ 4], S32, 0x4bdecfa9 );  /* 38 */
    HH( C, D, A, B, X[ 7], S33, 0xf6bb4b60 );  /* 39 */
    HH( B, C, D, A, X[10], S34, 0xbebfbc70 );  /* 40 */
    HH( A, B, C, D, X[13], S31, 0x289b7ec6 );  /* 41 */
    HH( D, A, B, C, X[ 0], S32, 0xeaa127fa );  /* 42 */
    HH( C, D, A, B, X[ 3], S33, 0xd4ef3085 );  /* 43 */
    HH( B, C, D, A, X[ 6], S34, 0x04881d05 );  /* 44 */
    HH( A, B, C, D, X[ 9], S31, 0xd9d4d039 );  /* 45 */
    HH( D, A, B, C, X[12], S32, 0xe6db99e5 );  /* 46 */
    HH( C, D, A, B, X[15], S33, 0x1fa27cf8 );  /* 47 */
    HH( B, C, D, A, X[ 2], S34, 0xc4ac5665 );  /* 48 */

    II( A, B, C, D, X[ 0], S41, 0xf4292244 );  /* 49 */
    II( D, A, B, C, X[ 7], S42, 0x432aff97 );  /* 50 */
    II( C, D, A, B, X[14], S43, 0xab9423a7 );  /* 51 */
    II( B, C, D, A, X[ 5], S44, 0xfc93a039 );  /* 52 */
    II( A, B, C, D, X[12], S41, 0x655b59c3 );  /* 53 */
    II( D, A, B, C, X[ 3], S42, 0x8f0ccc92 );  /* 54 */
    II( C, D, A, B, X[10], S43, 0xffeff47d );  /* 55 */
    II( B, C, D, A, X[ 1], S44, 0x85845dd1 );  /* 56 */
    II( A, B, C, D, X[ 8], S41, 0x6fa87e4f );  /* 57 */
    II( D, A, B, C, X[15], S42, 0xfe2ce6e0 );  /* 58 */
    II( C, D, A, B, X[ 6], S43, 0xa3014314 );  /* 59 */
    II( B, C, D, A, X[13], S44, 0x4e0811a1 );  /* 60 */
    II( A, B, C, D, X[ 4], S41, 0xf7537e82 );  /* 61 */
    II( D, A, B, C, X[11], S42, 0xbd3af235 );  /* 62 */
    II( C, D, A, B, X[ 2], S43, 0x2ad7d2bb );  /* 63 */
    II( B, C, D, A, X[ 9], S44, 0xeb86d391 );  /* 64 */

    pContext->State[0] += A;
    pContext->State[1] += B;
    pContext->State[2] += C;
    pContext->State[3] += D;

    ( void ) memset( &X, 0, sizeof( X ) );
}


void
md5_finalise( MD5 *pContext, unsigned char *pDigest )
{
    unsigned char uBytes[8];
    unsigned int uIndex, uPad;


    _md5_uint32ToByte( uBytes, pContext->Total, 8 );

    uIndex = ( unsigned int ) ( ( pContext->Total[0] >> 3 ) & 0x3f );
    uPad = ( uIndex < 56 ) ? ( 56 - uIndex ) : ( 120 - uIndex );
    md5_update( pContext, __PADDING, uPad );
    md5_update( pContext, uBytes, 8 );

    _md5_uint32ToByte( pDigest, pContext->State, 16 );

    ( void ) memset( pContext, 0, sizeof( *pContext ) );
}


void
md5_initialise( MD5 *pContext )
{
    pContext->State[0] = 0x67452301;
    pContext->State[1] = 0xefcdab89;
    pContext->State[2] = 0x98badcfe;
    pContext->State[3] = 0x10325476;
 
    pContext->Total[0] = pContext->Total[1] = 0;
}


void
md5_update( MD5 *pContext, unsigned char *pBuffer, unsigned int uLength )
{
    unsigned int uIndex1, uIndex2, uPartial;


    uIndex1 = ( unsigned int ) ( ( pContext->Total[0] >> 3 ) & 0x3f );
    if( ( pContext->Total[0] += ( ( uint32_t ) uLength << 3 ) ) < ( ( uint32_t ) uLength << 3 ) )
    {
        ++pContext->Total[1];
    }
    pContext->Total[1] += ( ( uint32_t ) uLength >> 29 );

    uPartial = 64 - uIndex1;

    if( uLength >= uPartial )
    {
        ( void ) memcpy( &pContext->Buffer[ uIndex1 ], pBuffer, uPartial );
        _md5_transform( pContext, pContext->Buffer );

        for( uIndex2 = uPartial; ( uIndex2 + 63 ) < uLength; uIndex2 += 64 )
        {
            _md5_transform( pContext, &pBuffer[ uIndex2 ] );
        }
        uIndex1 = 0;
    }
    else
    {
        uIndex2 = 0;
    }

    ( void ) memcpy( &pContext->Buffer[ uIndex1 ], &pBuffer[ uIndex2 ], uLength - uIndex2 );
}

