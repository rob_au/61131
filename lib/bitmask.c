#include <string.h>
#include <errno.h>

#include "bitmask.h"


#define _TYPE_COUNT_MIN( c )    if( Count < (c) ) return -EINVAL;


int
bitmask_AND( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 2 );

    ( void ) memcpy( &stValue, Parameters, sizeof( IEC61131_DATA ) );
    for( uIndex = 1; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( stValue.Type != pParameter->Type )
        {
            return -EINVAL;
        }

        switch( pParameter->Type )
        {
            case TYPE_BOOL:

                stValue.Value.B8 &= ( ( pParameter->Value.B8 > 0 ) ? ~0 : 0 );
                break;

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 &= pParameter->Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 &= pParameter->Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 &= pParameter->Value.B32;
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 &= pParameter->Value.B64;
                break;

#endif
            case TYPE_REAL:
            case TYPE_LREAL:
            default:

                return -EINVAL;
                /* break; */
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}


int
bitmask_OR( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 2 );

    ( void ) memcpy( &stValue, Parameters, sizeof( IEC61131_DATA ) );
    for( uIndex = 1; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( stValue.Type != pParameter->Type )
        {
            return -EINVAL;
        }

        switch( pParameter->Type )
        {
            case TYPE_BOOL:

                stValue.Value.B8 |= ( ( pParameter->Value.B8 > 0 ) ? ~0 : 0 );
                break;

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 |= pParameter->Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 |= pParameter->Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 |= pParameter->Value.B32;
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 |= pParameter->Value.B64;
                break;

#endif
            case TYPE_REAL:
            case TYPE_LREAL:
            default:

                return -EINVAL;
                /* break; */
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}


int
bitmask_XOR( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 2 );

    ( void ) memcpy( &stValue, Parameters, sizeof( IEC61131_DATA ) );
    for( uIndex = 1; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( stValue.Type != pParameter->Type )
        {
            return -EINVAL;
        }

        switch( pParameter->Type )
        {
            case TYPE_BOOL:

                stValue.Value.B8 ^= ( ( pParameter->Value.B8 > 0 ) ? ~0 : 0 );
                break;

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 ^= pParameter->Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 ^= pParameter->Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 ^= pParameter->Value.B32;
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 ^= pParameter->Value.B64;
                break;

#endif
            case TYPE_REAL:
            case TYPE_LREAL:
            default:

                return -EINVAL;
                /* break; */
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
    _TYPE_COUNT_MIN( 2 );
    return 0;
}


int
bitmask_NOT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 2 );

    ( void ) memcpy( &stValue, Parameters, sizeof( IEC61131_DATA ) );
    for( uIndex = 1; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( stValue.Type != pParameter->Type )
        {
            return -EINVAL;
        }

        switch( pParameter->Type )
        {
            case TYPE_BOOL:

                stValue.Value.B8 = ~( ( pParameter->Value.B8 > 0 ) ? ~0 : 0 );
                break;

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 = ~( pParameter->Value.B8 );
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 = ~( pParameter->Value.B16 );
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 = ~( pParameter->Value.B32 );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 = ~( pParameter->Value.B64 );
                break;

#endif
            case TYPE_REAL:
            case TYPE_LREAL:
            default:

                return -EINVAL;
                /* break; */
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}

